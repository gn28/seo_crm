<?php

return [
    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'vkontakte' => [
        'client_id' => env('VK_CLIENT'),
        'client_secret' => env('VK_SECRET'),
        'redirect' => config('app.url') . '/socialite/vkontakte/callback',
    ],

    'facebook' => [
        'client_id' => env('FB_CLIENT'),
        'client_secret' => env('FB_SECRET'),
        'redirect' => config('app.url') . '/socialite/facebook/callback',
    ],

    'twitter' => [
        'client_id' => env('TWITTER_CLIENT'),
        'client_secret' => env('TWITTER_SECRET'),
        'redirect' => config('app.url') . '/socialite/twitter/callback',
    ],

    'google' => [
        'client_id' => env('GOOGLE_CLIENT'),
        'client_secret' => env('GOOGLE_SECRET'),
        'redirect' => config('app.url') . '/socialite/google/callback',
    ],
];
