<?php namespace config;


use Ariol\Models\Page;
use Ariol\Models\Model;
use Ariol\Models\Users\Role;
use Ariol\Models\Users\User;
use Ariol\Models\Menu as SiteMenu;

/**
 * Класс меню админки.
 *
 * @package config
 */
class Menu extends Model
{
    /**
     * Левое меню админки.
     *
     * @return array
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public static function getMenu()
    {
        return [
            [
                'table' => 'settings',
                'icon' => '<i class="icon-cog position-left"></i>',
                'name' => 'Настройки страниц',
                'sub' => [
                    [
                        'icon' => '<i class="icon-cog7 position-left"></i>',
                        'name' => 'Общее',
                        'url' => 'settings/common'
                    ]
                ]
            ],

            [
                'table' => 'pages',
                'icon' => '<i class="icon-files-empty2 position-left"></i>',
                'name' => translate('system.menu.packageItems.pages'),
                'url' => 'pages',
                'count' => Page::countWithLang()
            ],
            [
                'icon' => '<i class="icon-cog4 position-left"></i>',
                'name' => translate('system.menu.packageItems.system-settings.packageSubTitle'),
                'sub' => [
                    [
                        'icon' => '<i class="icon-magic-wand position-left"></i>',
                        'name' => translate('system.menu.packageItems.system-settings.packageItems.cache-clear'),
                        'url' => 'system/cache'
                    ]
                ]
            ],
        ];
    }
}
