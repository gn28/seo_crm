<?php

Route::group(['middleware' => ['web', 'auth']], function () {

    Route::group(['prefix' => config('ariol.admin-path')], function () {
        /* Настройки страниц. */
        Helper::getSettingsRoutes([
            'Common', 'Main'
        ], 'Settings');

        /* Управление элементами сайта. */
        Helper::getBaseRoutes([
            'Menu'
        ]);
    });
});
