<?php

require_once 'admin.php';

Route::get('/logout', function () {
    Auth::logout();

    return redirect()->back();
});

Route::get('/', 'HomeController@index');