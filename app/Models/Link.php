<?php

namespace App\Models;

use Ariol\Models\Model;

/**
 * Модель пунктов меню.
 *
 * @package App\Models
 */
class Link extends Model
{
    /**
     * Используемая таблица.
     *
     * @var string
     */
    protected $table = 'links';

    /**
     * Разрешаем все поля для автозаполнения.
     *
     * @var array
     */
    protected $fillable = [
        'anchor',
        'donor_id',
        'url',
        'status'
    ];

    protected $statuses = [
       0 => 'На отправке',
       1 => 'Опубликован',
       2 => 'На удалении',
       3 => 'Удалено'
    ];

    public function donor()
    {
        return $this->belongsTo(Donor::class);
    }

}
