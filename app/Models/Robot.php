<?php

namespace App\Models;

use Ariol\Models\Model;

/**
 * Модель пунктов меню.
 *
 * @package App\Models
 */
class Robot extends Model
{
    /**
     * Используемая таблица.
     *
     * @var string
     */
    protected $table = 'robots_log';

    /**
     * Разрешаем все поля для автозаполнения.
     *
     * @var array
     */
    protected $fillable = [
        'donor_id',
        'type',
        'url'
    ];

    public function donors()
    {
        return $this->belongsTo(Donor::class);
    }
}
