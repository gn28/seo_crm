<?php

namespace App\Models\Settings;

use App\Models\Type;
use Ariol\Models\Model;

/**
 * Настройки главной страницы.
 *
 * @package App\Models\Settings
 */
class Main extends Model
{
    /**
     * Используемая таблица.
     *
     * @var string
     */
    protected $table = 'settings';

    /**
     * Разрешаем все поля для автозаполнения.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Наименование полей.
     *
     * @return array
     */
    public function labels()
    {
        return [

        ];
    }

    /**
     * Описание типов полей.
     *
     * @return array
     */
    public function form()
    {
        return [

        ];
    }

}
