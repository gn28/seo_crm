<?php

namespace App\Models\Settings;

use Ariol\Models\Model;

/**
 * Общие настройки сайта.
 *
 * @package App\Models\Settings
 */
class Common extends Model
{
    /**
     * Используемая таблица.
     *
     * @var string
     */
    protected $table = 'settings';

    /**
     * Разрешаем все поля для автозаполнения.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Наименование полей.
     *
     * @return array
     */
    public function labels()
    {
        return [

        ];
    }

    /**
     * Описание типов полей.
     *
     * @return array
     */
    public function form()
    {
        return [

        ];
    }
}
