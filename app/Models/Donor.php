<?php

namespace App\Models;

use Ariol\Models\Model;

/**
 * Модель пунктов меню.
 *
 * @package App\Models
 */
class Donor extends Model
{
    /**
     * Используемая таблица.
     *
     * @var string
     */
    protected $table = 'donors';

    /**
     * Разрешаем все поля для автозаполнения.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'domain',
        'status'
    ];

    protected  $statuses = [
        0 => 'Не размещён',
        1 => 'Ошибка при размещении кода',
        2 => 'Активен',
        3 => 'Не проиндексирован'
    ];

    public function projects()
    {
        return $this->belongsToMany(Project::class);
    }

    public function links()
    {
        return $this->hasMany(Link::class);
    }
}
