<?php

namespace App\Models;

use Ariol\Models\Model;

/**
 * Модель пунктов меню.
 *
 * @package App\Models
 */
class Project extends Model
{
    /**
     * Используемая таблица.
     *
     * @var string
     */
    protected $table = 'projects';

    /**
     * Разрешаем все поля для автозаполнения.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'url'
    ];

    public function donors()
    {
        return $this->belongsToMany(Donor::class);
    }
}
