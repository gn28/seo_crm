<?php

namespace App\Models;

use Ariol\Models\Model;

/**
 * Модель пунктов меню.
 *
 * @package App\Models
 */
class DonorLink extends Model
{
    /**
     * Используемая таблица.
     *
     * @var string
     */
    protected $table = 'donor_links';

    /**
     * Разрешаем все поля для автозаполнения.
     *
     * @var array
     */
    protected $fillable = [
        'link_id',
        'donor_id',
        'url'
    ];


    public function donors()
    {
        return $this->belongsTo(Donor::class);
    }

    public function links()
    {
        return $this->belongsTo(Link::class);
    }

}
