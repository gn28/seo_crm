<?php namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/admin/*',
        '/search',
        '/social',
        '/socialite/*',
        '/load-tags',
        '/add-event',
        '/updateCity',
        '/get-discount',
        '/refuse-event',
        '/confirm-event',
        '/upload-event-image'
    ];
}
