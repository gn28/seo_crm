<?php

namespace App\Http\Controllers\Admin;

use Ariol\Admin\Controllers\Forms\BaseController;

/**
 * Класс меню.
 *
 * @package Ariol\Admin\Controllers\Modules
 */
class MenuController extends BaseController
{
    /**
     * Используемая модель.
     *
     * @var string
     */
    protected $model = 'App\Models\Menu';
}
