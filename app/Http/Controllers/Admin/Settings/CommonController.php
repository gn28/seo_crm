<?php

namespace App\Http\Controllers\Admin\Settings;

use Ariol\Admin\Controllers\Forms\SettingsController;

/**
 * Класс общих настроек сайта.
 *
 * @package App\Http\Controllers\Admin\Settings
 */
class CommonController extends SettingsController
{
    /**
     * Используемая модель.
     *
     * @var string
     */
    protected $model = 'App\Models\Settings\Common';
}
