<?php

namespace App\Http\Controllers\Admin\Settings;

use Ariol\Admin\Controllers\Forms\SettingsController;

/**
 * Класс настроек главной странице.
 *
 * @package App\Http\Controllers\Admin\Settings
 */
class MainController extends SettingsController
{
    /**
     * Используемая модель.
     *
     * @var string
     */
    protected $model = 'App\Models\Settings\Main';
}
