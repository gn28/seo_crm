<?php

namespace App\Http\Controllers;

//use Jenssegers\Agent\Agent;
//use Validator;
//use Ariol\Models\Page;
//use Ariol\Classes\Helper;
//use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\Auth;

/**
 * Класс главной страницы сайта.
 *
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * Главная страница.
     *
     * @return \Illuminate\Contracts\View\View
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function index()
    {

        return Auth::check() ? redirect('/seo-crm') : redirect()->route('login');

    }


}
