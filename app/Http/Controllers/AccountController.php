<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use Exception;
use Ariol\Classes\Helper;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Laravel\Socialite\Facades\Socialite;

class AccountController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['socialite', 'socialiteCallback', 'logout']);
    }

    public function ulogin(Request $request)
    {
        $data = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $user = json_decode($data, TRUE);

        $userData = User::where('email', $user['email'])->first();

        if ($userData) {
            if ($user['verified_email']) {
                Auth::login($userData, true);
            } else {
                return redirect()->back()->with('notice', [
                    'type' => 'warning',
                    'message' => 'E-mail не привязан или не подтверждён.'
                ]);
            }

            return redirect()->back();
        } else {
            $newUser = User::create([
                'role_id' => 0,
                'email' => $user['email'],
                'photo' => '',
                'photo_mini' => '',
                'name' => $user['first_name'] . ' ' . $user['last_name'],
                'password' => bcrypt(Helper::generate(15))
            ]);

            Auth::login($newUser, true);

            return redirect()->back()->with('notice', [
                'type' => 'warning',
                'message' => 'Ошибка авторизации. Обратитесь к администратору сайта.'
            ]);
        }
    }

    /**
     * Перенаправление на необходимый сервис.
     *
     * @param $provider
     * @return mixed
     */
    public function socialite($provider)
    {
        return Socialite::driver($provider)->with(['email'])->scopes([
            'email'
        ])->redirect();
    }

    /**
     * Ответ после обработки запроса сервиса.
     *
     * @param $provider
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function socialiteCallback($provider)
    {
        try {
            $photo = $photoMini = '';

            $user = Socialite::driver($provider)->user();

            $email = $user->accessTokenResponseBody['email'] ?? $user->email;
            if ($provider == 'twitter' && empty($email)) {
                $email = $user->id . '@twitter.com';
            }

            $person = User::where('email', $email)->first();
            if (! $person) {
                $nickname = $user->getName();

                if ($provider == 'vkontakte') {
                    $photoMini = $user->user['photo'];
                    $photo = $user->user['photo_max_orig'];
                } elseif ($provider == 'facebook') {
                    $photo = $photoMini = $user->user['picture'];
                } elseif ($provider == 'twitter') {
                    $photo = $photoMini = $user->avatar;
                    $nickname = $user->nickname ?? ($user->getName() ?? $user->id);

                    $email = $user->id . '@twitter.com';
                }

                $createUser = User::create([
                    'role_id' => 0,
                    'email' => $email,
                    'photo' => $photo,
                    'photo_mini' => $photoMini,
                    'name' => $nickname,
                    'password' => bcrypt(Helper::generate(15))
                ]);

                Auth::login($createUser, true);
            } else {
                Auth::login($person, true);
            }

            return redirect('/');
        } catch (Exception $e) {
            return redirect('/')->with('notice', [
                'type' => 'warning',
                'message' => 'Ошибка авторизации. Обратитесь к администратору сайта.'
            ]);
        }
    }
}
