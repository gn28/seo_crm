<?php

namespace App\Console\Commands;

use File;
use Illuminate\Console\Command;

/**
 * Команда для генерации карты сайта.
 *
 * @package App\Console\Commands
 */
class SiteMap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'algerda:sitemap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Генерация карты сайта.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        if (! file_exists(public_path() . '/sitemap')) {
            File::makeDirectory(public_path() . '/sitemap');
        }

        if (file_exists(public_path() . '/sitemap/sitemap.xml')) {
            unlink(public_path() . '/sitemap/sitemap.xml');
        }

        if (file_exists(public_path() . '/sitemap/sitemap-index.xml')) {
            unlink(public_path() . '/sitemap/sitemap-index.xml');
        }

        include base_path() . '/packages/ariol/admin/src/Libs/SiteMap.php';

        $defaultDate = date('d-m-Y', strtotime(date('Y-m-d H:i:s')));

        $sitemap = new \SiteMap(config('app.url'));
        $sitemap->setFilename('sitemap');
        $sitemap->setPath(public_path() . '/sitemap/');

        $sitemap->addItem('/', 1, 'daily', date('d-m-Y'));

        foreach (\Ariol\Models\Page::get() as $page) {
            $sitemap->addItem('/pages/' . $page->url, 1, 'monthly', date('d-m-Y', strtotime($page->updated_at)));
        }

        $sitemap->addItem('/about', 1, 'monthly', $defaultDate);
        $sitemap->addItem('/reviews', 1, 'monthly', $defaultDate);
        $sitemap->addItem('/events', 1, 'monthly', $defaultDate);

        foreach (\App\Models\Events\Event::where('active', 1)->get(['updated_at', 'alias']) as $event) {
            $date = date('d-m-Y', strtotime($event->updated_at));
            $sitemap->addItem($event->getFullUrl(), 1, 'monthly', $date);
        }

        foreach (\App\Models\Reviews\Review::where('active', 1)->get(['updated_at', 'alias']) as $review) {
            $date = date('d-m-Y', strtotime($review->updated_at));
            $sitemap->addItem($review->getFullUrl(), 1, 'monthly', $date);
        }

        foreach (\App\Models\Type::select('id')->orderBy('name')->get() as $type) {
            $sitemap->addItem('/events?types[]=' . $type->id, 1, 'monthly', $defaultDate);
            $sitemap->addItem('/reviews?types[]=' . $type->id, 1, 'monthly', $defaultDate);
        }

        foreach (\App\Models\Tag::select('id')->orderBy('name')->get() as $tag) {
            $sitemap->addItem('/events?tags[]=' . $tag->id, 1, 'monthly', $defaultDate);
        }

        foreach (\App\Models\Places\Place::select('id')->orderBy('name')->get() as $place) {
            $sitemap->addItem('/events?places[]=' . $place->id, 1, 'monthly', $defaultDate);
            $sitemap->addItem('/reviews?places[]=' . $place->id, 1, 'monthly', $defaultDate);
        }

        foreach (\App\Models\Reviews\Type::select('id')->orderBy('name')->get() as $tag) {
            $sitemap->addItem('/reviews?reviewsTypes[]=' . $tag->id, 1, 'monthly', $defaultDate);
        }

        $sitemap->createSitemapIndex(public_path() . '/sitemap/sitemap.xml');

        chmod(public_path() . '/sitemap/sitemap.xml', 0777);
        chmod(public_path() . '/sitemap/sitemap-index.xml', 0777);

        if (file_exists(public_path() . '/sitemap/sitemap-index.xml')) {
            unlink(public_path() . '/sitemap/sitemap-index.xml');
        }

        $this->info('Карта сайта успешно сгенерирована.');
    }
}
