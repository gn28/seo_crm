<?php

namespace App\Providers;

use View;
use Schema;
use Jenssegers\Agent\Agent;
//use App\Models\Type;
//use App\Models\City;
use App\Models\Menu;
//use App\Models\Service;
use Ariol\Classes\Helper;
//use App\Models\Places\Place;
//use App\Models\Places\Category;
use Ariol\Classes\Localization;
use Illuminate\Support\ServiceProvider;

/**
 * Class AppServiceProvider
 *
 * @package App\Providers
 */
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function boot()
    {
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
