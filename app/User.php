<?php namespace App;

use Ariol\Models\Users\Module;
use Auth;
use Ariol\Models\Model;
use Ariol\Models\Users\Role;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

/**
 * Модель пользователей.
 *
 * @package App
 */
class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    /**
     * Используемая таблица.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * Разрешаем все поля для автозаполнения.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Атрибуты, исключенные из формы модели в json.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * Проверка на существование роли у пользователя.
     *
     * @param $alias
     * @return bool
     */
    public function hasRole($alias)
    {
        $role = Role::where('alias', $alias)->first();
        if (! $role) {
            return false;
        }

        if (Auth::user()->role_id != $role->id) {
            return false;
        }

        return true;
    }

    /**
     * Данные о роли.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function role()
    {
        return $this->hasOne('Ariol\Models\Users\Role', 'id', 'role_id');
    }
}
