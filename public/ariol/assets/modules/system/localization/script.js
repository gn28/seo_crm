$(document).ready(function() {
    var $tagLanguage = $('language');

    $.ajaxSetup({
        type: 'post',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    /* Загрузка языкового пакета. */
    $('body').on('click', '[data-language-select]', function(e) {
        if (e.target !== undefined && !$(e.target).hasClass('styled') && !$(e.target).hasClass('icon-cross2')) {
            var language = $(this).attr('data-language-select');

            loadPackage(language);
        }
    })
    /* Активация или деактивация языка. */
    .on('click', '.language-checkbox', function(e) {
        e.stopPropagation();

        var active = $(this).find('input').is(':checked') ? 'on' : 'off',
            language = $(this).closest('li').attr('data-language-select');

        $.ajax({
            url: '/' + adminPath + '/system/localization/toggle-active',
            data: {
                active: active,
                language: language
            },
            success: function(result) {
                /* Обновление списка добавленных языков. */
                updateAvailableLanguages();

                /* Уведомление о совершённом действии. */
                resultNotice(result);
            }
        });
    })
    /* Назначение нового языка по умолчанию. */
    .on('change', 'input[name="default-language"]', function(e) {
        var language = $(this).val();

        $.ajax({
            url: '/' + adminPath + '/system/localization/toggle-default',
            data: {
                language: language
            },
            success: function(result) {
                /* Обновление списка добавленных языков. */
                updateAvailableLanguages();

                /* Уведомление о совершённом действии. */
                resultNotice(result);
            }
        });
    })
    /* Обновление значений checkbox'ов. */
    .on('click', ':checkbox', function () {
        var value = this.checked === true ? 1 : 0,
            $checkbox = $(this).attr('checked', this.checked).prop('checked', this.checked);

        $(this).val(value);
        $.uniform.update($checkbox);
    })
    /* Удаление языка. */
    .on('click', '.localization-remove', function() {
        var language = $(this).closest('li').attr('data-language-select');

        swal({
            text: '',
            type: "warning",
            closeOnConfirm: true,
            showCancelButton: true,
            confirmButtonColor: "#FF7043",
            cancelButtonText: $tagLanguage.attr('data-no-cancel'),
            confirmButtonText: $tagLanguage.attr('data-yes-delete'),
            title: $tagLanguage.attr('data-language-delete-are-you-sure')
        },
        function() {
            $.ajax({
                url: '/' + adminPath + '/system/localization/remove-language',
                data: {
                    language: language
                },
                success: function(result) {
                    if (result.success) {
                        /* Обновление списка доступных для выбора языков. */
                        updateListLanguages($tagLanguage);

                        /* Обновление списка языков в шапке. */
                        updateAdminLanguages();

                        /* Если был удалён текущий активный язык, то переадресовываем на русский. */
                        if ($('.page-container').attr('data-current-language') == language) {
                            /* Обновление текущего языка в шапке. */
                            updateCurrentLanguage();
                        }

                        $('[data-lang-package="' + language + '"]').remove();
                        $('[data-language-select="' + language + '"]').remove();

                        if ($('input[name="default-language"]:checked').length == 0) {
                            var $radio = $('[data-language-select="ru"]');

                            $radio.find('span').addClass('checked');
                            $radio.find('input[name="default-language"]').attr('checked', this.checked).prop('checked', this.checked);
                        }
                    }

                    /* Уведомление о совершённом действии. */
                    resultNotice(result);
                }
            });
        });
    })
    /* Изменение языка, на основе которого будет заполняться перевод. */
    .on('change', '#select-language-for-translation', function() {
        var language = $(this).val();

        $.ajax({
            url: '/' + adminPath + '/system/localization/change-language-for-translation',
            data: {
                language: language
            },
            success: function(phrases) {
                if (phrases.error) {
                    /* Уведомление о совершённом действии. */
                    notice('error', phrases.error);
                } else {
                    $.each(phrases, function(key, words) {
                        $.each(words, function(index, word) {
                            $('#' + key + ' tbody tr:eq(' + index + ') td:first-child span').text(word);
                        });
                    });
                }
            }
        });
    })
    /* Показывать только те выражения, которые ещё не переведены. */
    .on('change', '#untranslated', function() {
        var checked = this.checked;
        var $inputs = $('.table-translate input[value!=""]');

        $.each($inputs, function() {
            if (checked) {
                $(this).closest('tr').addClass('d-n');
            } else {
                $(this).closest('tr').removeClass('d-n');
            }
        });
    });

    /* Инициализация выбора языка. */
    initLanguageSelect($tagLanguage);

    /* Добавление языка. */
    $('#add-language').ajaxForm({
        success: function(result) {
            if (! result.error) {
                $('#modal-create-language').modal('hide');

                /* Обновление списка добавленных языков. */
                updateAvailableLanguages();

                /* Обновление списка языков в шапке. */
                updateAdminLanguages();

                /* Обновление списка доступных для выбора языков. */
                updateListLanguages($tagLanguage);

                /* Обновление стилей checkbox и radio. */
                updateUniform();

                /* Уведомление о совершённом действии. */
                notice('success', $tagLanguage.attr('data-add-language-success'));
            } else {
                /* Уведомление о совершённом действии. */
                notice('error', result.error);
            }
        }
    });
});

/* Обновление списка добавленных языков. */
function updateAvailableLanguages() {
    $('body').find('[data-toggle="tooltip"]').tooltip('hide');

    $.ajax({
        url: '/' + adminPath + '/system/localization/update-available-languages',
        success: function(languages) {
            $('li[data-language-select]').remove();
            $('ul#languages li.navigation-header').after(languages);

            /* Обновление стилей checkbox и radio. */
            updateUniform();
        }
    });
}

/* Обновление списка языков в шапке. */
function updateAdminLanguages() {
    $.ajax({
        url: '/' + adminPath + '/system/localization/update-admin-languages',
        success: function(languages) {
            $('ul#admin-languages').html(languages);
        }
    });
}

/* Обновление списка доступных для выбора языков. */
function updateListLanguages($tagLanguage) {
    var $language = $('#language');

    $.ajax({
        url: '/' + adminPath + '/system/localization/update-list-languages',
        success: function(languages) {
            $language.select2('destroy').html(languages);

            /* Инициализация выбора языка. */
            initLanguageSelect($tagLanguage);
        }
    });
}

/* Инициализация выбора языка. */
function initLanguageSelect($tagLanguage) {
    $('.language-select').select2({
        placeholder: $tagLanguage.attr('data-select-item'),
        language: {
            noResults: function() {
                return $tagLanguage.attr('data-no-founded');
            }
        },
        templateResult: templateLanguages,
        templateSelection: templateLanguages,
        escapeMarkup: function (markup) {
            return markup;
        }
    });
}

/* Шаблон для вывода иконок и названий языков. */
function templateLanguages(option) {
    if (! option.id) {
        return option.text;
    }

    return '<img src="/languages/' + option.id + '.svg" class="localization-select-language">' +
        '<div class="localization-name">' +
            option.text +
        '</div>' +
    '';
}

/* Загрузка языкового пакета. */
function loadPackage(language) {
    var $tagLanguage = $('language'),
        $block = $('[data-language-select="' + language + '"]');

    $block.block({
        message: '<i class="icon-spinner2 spinner"></i>',
        overlayCSS: {
            opacity: 0.8,
            cursor: 'wait',
            backgroundColor: '#fff',
            'box-shadow': '0 0 0 1px #ddd'
        },
        css: {
            border: 0,
            padding: 0,
            backgroundColor: 'none'
        }
    });

    $.ajax({
        url: '/' + adminPath + '/system/localization/load-package',
        data: {
            language: language
        },
        success: function(langPackage) {
            if (langPackage.error) {
                /* Уведомление о совершённом действии. */
                notice('error', langPackage.error);
            } else {
                $('#lang-package').html(langPackage);

                /* Обновление стилей checkbox и radio. */
                updateUniform();

                /* Инициализация выбора языка. */
                initLanguageSelect($tagLanguage);

                /* Убрать наложение фона с загрузкой. */
                $block.unblock();

                /* Сохранение перевода. */
                $('#save-translate').ajaxForm({
                    data: {
                        selectedLanguageForTranslation: language
                    },
                    success: function(result) {
                        /* Уведомление о совершённом действии. */
                        resultNotice(result);

                        /* Загрузка языкового пакета. */
                        loadPackage(language);

                        $('html, body').animate({
                            scrollTop: 0
                        }, 500);
                    }
                });
            }
        }
    });
}

/* Обновление текущего языка в шапке. */
function updateCurrentLanguage() {
    $.ajax({
        url: '/' + adminPath + '/system/localization/change-current-admin-language',
        success: function(language) {
            $('#current-language').html(language);
        }
    });
}