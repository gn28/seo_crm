var adminPath = '',
    $tagLanguage = '',
    currentAdminLanguage = 'ru';

$(document).ready(function() {
    var $adminBlock = $('.page-container');

    adminPath = $adminBlock.attr('data-config-url');
    currentAdminLanguage = $adminBlock.attr('data-current-language');

    $tagLanguage = $('language');

    /* Обновление стилей checkbox и radio. */
    updateUniform();

    /* Скролл в самый низ страницы. */
    $('#scroll-to-bottom').on('click', function () {
        $('html, body').animate({
            scrollTop: $(document).height()
        }, 500);
    });

    /* Скролл в самый верх страницы. */
    $('#scroll-to-top').on('click', function () {
        $('html, body').animate({
            scrollTop: 0
        }, 500);
    });

    /* Выделение активного пункта меню. */
    $('div.sidebar-main li.active').last().parent('ul').show().parents('li').addClass('active').closest('ul').show();

    var $sidebarMenu = $('#sidebar-menu');

    $sidebarMenu.mCustomScrollbar({
        axis: 'y',
        theme: 'default',
        scrollButtons: {
            enable: true
        },
        scrollInertia: 60,
        mouseWheelPixels: 50
    });

    /* Скролл до активного пункта меню. */
    var $item = $('li.active:last');

    if ($item.length) {
        var height = $item.height(),
            offset = $item.offset().top,
            windowHeight = $(window).height(),
            focus = (height < windowHeight) ? offset - ((windowHeight / 2) - (height / 2)) : offset;

        $sidebarMenu.mCustomScrollbar('scrollTo', focus);
    }

    /* Обновление стилей checkbox и radio. */
    updateUniform();

    $('#sidebar-localization').mCustomScrollbar({
        axis: 'y',
        theme: 'dark',
        scrollButtons: {
            enable: true
        },
        scrollInertia: 60,
        mouseWheelPixels: 50
    });

    /* Изменение языка в админке. */
    $('body').on('click', 'ul#admin-languages li', function() {
        var code = $(this).attr('data-language-code');

        $.ajax({
            type: 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/' + adminPath + '/system/localization/change-admin-language',
            data: {code: code},
            success: function() {
                window.location.reload(true);
            }
        });
    }).tooltip({
        selector: '[data-toggle="tooltip"]',
        trigger : 'hover'
    });
});

window.onscroll = function() {
    scrollButtons()
};

/* Кнопки "вверх" и "вниз" для скроллинга по странице. */
function scrollButtons() {
    if (document.body.scrollTop > 1 || document.documentElement.scrollTop > 1) {
        $('#scroll-to-top').removeClass('hidden');
    } else {
        $('#scroll-to-top').addClass('hidden');
    }

    var scrollTop = document.body.scrollTop,
        height = document.documentElement.scrollHeight - document.documentElement.clientHeight;

    if ((scrollTop !== 0 && scrollTop < (height - 1)) || document.documentElement.scrollTop < (height - 1)) {
        $('#scroll-to-bottom').removeClass('hidden');
    } else {
        $('#scroll-to-bottom').addClass('hidden');
    }
}

/* Обновление стилей checkbox и radio. */
function updateUniform() {
    $('.styled').uniform({
        radioClass: 'choice'
    });
}

/* Задержка при вводе символов. */
var delay = (function () {
    var timer = 0;

    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();