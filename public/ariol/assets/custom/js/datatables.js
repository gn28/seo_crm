$(document).ready(function() {
    var dataTable = $('#datatable'),
        dataUrl = $('#data-url').val(),
        startPage = dataTable.attr('data-start-page'),
        model = $('[data-table-model]').attr('data-table-model');

    dataUrl = dataUrl.replace('//', '/');

    var $table = dataTable.DataTable({
        dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        buttons: [
            {
                extend: 'copyHtml5',
                className: 'btn btn-default',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'excelHtml5',
                className: 'btn btn-default',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'csvHtml5',
                className: 'btn btn-default',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'pdfHtml5',
                className: 'btn btn-default',
                exportOptions: {
                    columns: ':visible'
                }
            }
        ],
        "oLanguage": {
            "sUrl": "",
            "sInfoEmpty": "",
            "sInfoPostFix": "",
            "sInfoFiltered": "",
            "oPaginate": {
                "sLast": $tagLanguage.attr('data-last'),
                "sFirst": $tagLanguage.attr('data-first'),
                "sNext": '<i class="icon-arrow-right7">',
                "sPrevious": '<i class="icon-arrow-left7">'
            },
            "sSearch": "<span>" + $tagLanguage.attr('data-search') + ":</span>",
            "sZeroRecords": $tagLanguage.attr('data-no-data'),
            "sLengthMenu": "<span>" + $tagLanguage.attr('data-show') + ":</span> _MENU_",
            "bProcessing": "<img src='/ariol/assets/images/custom/loading.gif' alt='" + $tagLanguage.attr('data-loading') + "'>",
            "sProcessing": "<img src='/ariol/assets/images/custom/loading.gif' alt='" + $tagLanguage.attr('data-loading') + "'>",
            "sInfo": "<button id='delete-selected-items' class='btn btn-danger'>" +
                $tagLanguage.attr('data-delete-selected') +
            "</button>"
        },
        "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $('td:eq(0).text-center', nRow).addClass('text-center');
        },
        "fnDrawCallback": function () {
            var $selectAll = $('#select-all');
            var $deleteSelectedItems = $('#delete-selected-items');

            /* Если нет записей для удаления, то скрываем кнопку "Удалить выбранное" и делаем disabled checkbox'а выбора всех записей. */
            if ($('tbody input[type="checkbox"].styled:enabled').length < 1) {
                $selectAll.prop('disabled', true);
                $deleteSelectedItems.addClass('hidden');
            } else {
                $selectAll.prop('disabled', false);
                $deleteSelectedItems.removeClass('hidden');
            }

            var $searchField =  $('#datatable_filter input[type="search"]');

            /* Сохранение данных поиска в сессию, в зависимости от выбранной таблицы. */
            $searchField.unbind();
            $searchField.bind('keyup', function () {
                $('[data-table-model]').attr('data-table-search', $(this).val());

                $table.search(this.value).draw();
            });

            $.uniform.update();

            /* Первая ячейка первой колонки. */
            var $th = $('th:eq(0)');

            if ($th.find(':checkbox').length > 0) {
                /* Убираем сортировку у ячейки с выбором всех записей. */
                $th.removeClass('sorting_asc').addClass('sorting_disabled');
            }

            /* Выставляем заданные выравнивания колонкам. */
            $.each($('th'), function(index, data) {
                var align = $(this).attr('data-column-align');

                $(this).addClass('text-' + align);

                $.each($('tr'), function(i, d) {
                    $('tr:eq(' + i + ') td:eq(' + index + ')').addClass('text-' + align);
                });
            });

            $.each($('.list-activity'), function () {
                var $switch = $(this);

                /* Активируем переключатель поля с типом activity в самой таблице. */
                $(this).bootstrapSwitch({
                    size: 'mini',
                    onColor: $switch.data('color'),
                    onText: $tagLanguage.attr('data-on'),
                    offText: $tagLanguage.attr('data-off'),
                    onSwitchChange: function (event, state) {
                        $.ajax({
                            type: 'post',
                            url: '/' + adminPath + '/grid/activity',
                            data: {
                                id: $switch.data('id'),
                                model: $switch.data('model'),
                                state: (state === true ? 1 : 0),
                                updating_field: $switch.data('updating-field')
                            },
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            success: function () {}
                        })
                    }
                });
            });

            $('[name="datatable_length"]').select2({
                width: 'auto'
            });

            $('.styled').uniform({
                radioClass: 'choice'
            });

            /* Выбрать сразу все записи. */
            $selectAll.on('click', function() {
                var $select_all = this.checked;

                $('[data-item-id]').each(function() {
                    var id = $(this).attr('data-item-id');

                    $('[data-item-id="' + id + '"]:enabled').prop('checked', $select_all).attr('checked', $select_all);
                    $.uniform.update();
                });
            });

            /* Выбор записей. */
            $('#datatable').on('click', '[data-item-id]', function () {
                var checked = $('[data-item-id]:not(:checked)').length <= 0;

                $selectAll.prop('checked', checked);

                $.uniform.update();
            });

            /* Удаление выбранных записей в таблице. */
            $deleteSelectedItems.on('click', function() {
                var tr = [],
                    selected = [];

                /* Счётчик активного пункта меню. */
                var $count = $('.sidebar li.active:last').find('span.label');

                $('[data-item-id]:checked').each(function(i) {
                    selected[i] = $(this).attr('data-item-id');
                    tr[i] = $(this).closest('tr');
                });

                if (tr.length === 0) {
                    swal({
                        type: "error",
                        confirmButtonColor: "#2196F3",
                        title: $tagLanguage.attr('data-no-selected')
                    });
                } else {
                    swal({
                        type: "warning",
                        closeOnCancel: true,
                        closeOnConfirm: true,
                        showCancelButton: true,
                        showLoaderOnConfirm: true,
                        confirmButtonColor: "#EF5350",
                        title: $tagLanguage.attr('data-delete-are-you-sure'),
                        cancelButtonText: $tagLanguage.attr('data-no-cancel'),
                        confirmButtonText: $tagLanguage.attr('data-yes-delete')
                    }, function(isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                type: 'post',
                                url: '/' + adminPath + '/grid/delete-items',
                                data: {
                                    model: model,
                                    selected: selected
                                },
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                success: function(result) {
                                    if (result.error) {
                                        swal({
                                            type: "error",
                                            title: result.error,
                                            confirmButtonColor: "#2196F3"
                                        });
                                    } else {
                                        $selectAll.prop('checked', false).attr('checked', false);
                                        $.uniform.update();

                                        $.each(tr, function(index) {
                                            $table.row(index).remove();
                                        });

                                        $table.draw(false);

                                        /* Обновление счётчика в меню, если он существует. */
                                        $.ajax({
                                            type: 'post',
                                            url: '/' + adminPath + '/grid/update-count-menu-item',
                                            data: {
                                                model: model
                                            },
                                            headers: {
                                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                            },
                                            success: function(count) {
                                                $count.text(parseInt(count));
                                            }
                                        });

                                        notice('success', $tagLanguage.attr('data-delete-success'));
                                    }
                                }
                            });
                        }
                    });
                }
            });

            var $table = $('.datatable-highlight').DataTable();

            /* Выделение колонки, строки и их пересечения при наведении на ячейку. */
            if ($('.datatable-highlight tbody td').length !== 0) {
                $('.datatable-highlight tbody').on('mouseover', 'td', function() {
                    if ($table.cell(this).index() !== undefined) {
                        var colIdx = $table.cell(this).index().column;

                        if (colIdx !== null) {
                            $($table.cells().nodes()).removeClass('active');
                            $($table.column(colIdx).nodes()).addClass('active');
                        }
                    }
                }).on('mouseleave', function() {
                    $($table.cells().nodes()).removeClass('active');
                });
            }

            /* Тип ячейки и фильтра - select. */
            var $selects = $('select.select-grid');

            if ($selects.length > 0) {
                $.each($selects, function () {
                    var label = $(this).attr('data-filter-label');
                    var placeholder = label !== undefined ? label : $tagLanguage.attr('data-select-item');

                    $(this).select2({
                        placeholder: placeholder,
                        language: {
                            noResults: function() {
                                return $tagLanguage.attr('data-no-founded');
                            }
                        },
                        escapeMarkup: function (markup) {
                            return markup;
                        }
                    });
                });
            }

            /* Тип фильтра - multiselect. */
            var $multiSelects = $('select.multiselect-grid');

            if ($multiSelects.length > 0) {
                $.each($multiSelects, function () {
                    $(this).multiselect({
                        maxHeight: 300,
                        includeSelectAllOption: true,
                        enableClickableOptGroups: true,
                        enableFiltering: true,
                        templates: {
                            filter: '<li class="multiselect-item multiselect-filter">' +
                                '<i class="icon-search4"></i> <input class="form-control" type="text">' +
                            '</li>'
                        },
                        selectAllText: $tagLanguage.attr('data-select-all'),
                        allSelectedText: $tagLanguage.attr('data-all-selected'),
                        nSelectedText: $tagLanguage.attr('data-selected'),
                        nonSelectedText: $tagLanguage.attr('data-no-selected'),
                        filterPlaceholder: $tagLanguage.attr('data-search'),
                        onInitialized: function(select, container) {
                            $('.styled, .multiselect-container input').uniform({
                                radioClass: 'choice'
                            });
                        },
                        onSelectAll: function() {
                            $.uniform.update();
                        },
                        onChange: function() {
                            $.uniform.update();
                        }
                    });
                });
            }

            /* Выбранная страница. */
            var page = $('.paginate_button.current').text();

            /* Запоминание страницы, чтобы после возврата со страницы формы загружались записи с неё. */
            $.ajax({
                url: '/' + adminPath + '/grid/remember-page',
                type: 'post',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {page: page},
                success: function() {}
            });
        },
        "aoColumnDefs": dtData.arrayJSONColTable,
        "bDestroy": true,
        "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'Все']],
        "iDisplayLength": 50,
        "iDisplayStart": (50 * startPage) - 50,
        "bAutoWidth": false,
        "bSort": true,
        "bFilter": true,
        "processing": true,
        "bServerSide": true,
        "ajax": {
            url: dataUrl,
            type: 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend : function() {
                $('#tables-loading').removeClass('hidden');

                var $searchField = $('#datatable_filter input[type="search"]');
                var $searchFieldValue = $('[data-table-model]').attr('data-table-search');

                var value = $searchField.val();

                if (value == '') {
                    $searchField.val($searchFieldValue !== undefined ? $searchFieldValue : '');
                }
            },
            complete: function(){
                $('#tables-loading').addClass('hidden');
            },
            data: function(data) {
                /* Данные дополнительных фильтров. */
                var $filterForm = $('#filters');

                if ($filterForm.length) {
                    var sendFilters = +$filterForm.attr('data-send-filters');

                    if (sendFilters) {
                        data.filters = $filterForm.serializeAssoc();
                    }
                }

                var $searchFieldValue = $('[data-table-model]').attr('data-table-search');
                data.search.value = $searchFieldValue !== undefined ? $searchFieldValue : '';

                /* Передача названия текущей модели, чтобы управлять сессией текущей страницы таблицы данных. */
                data.sessionModel = model;
            }
        },
        "bProcessing": true
    });

    /* Форма с дополнительными фильтрами. */
    var $filters = $('#filters');

    /* Обновлять данные только при изменении дополнительных фильтров. */
    $filters.change(function() {
        $filters.attr('data-send-filters', 1);

        $table.ajax.reload();
    });

    /* Очистка дополнительных фильтров. */
    $('#filter-clear').on('click', function() {
        $filters.find('input, select').val('');

        $filters.find('select.multiselect-grid').multiselect('deselectAll', false).multiselect('refresh');

        $filters.find('input[type="checkbox"]').prop('checked', false);
        $.uniform.update();

        $filters.attr('data-send-filters', 0);

        $table.ajax.reload();
    });

    /* Редактирование данных прямо в таблице. */
    $('table').on('keyup', 'div[contenteditable="true"]', function(e) {
        var code = (e.keyCode ? e.keyCode : e.which);

        if (code === 27) {
            $(this).text($(this).attr('data-field-value'));
            $(this).blur();
        }
    }).on('keypress', 'div[contenteditable="true"]', function(e) {
        var code = (e.keyCode ? e.keyCode : e.which);

        if (code === 13) {
            e.preventDefault();

            var value = $(this).text(),
                id = $(this).attr('data-entry-id'),
                field = $(this).attr('data-field-name');

            $.ajax({
                type: 'post',
                url: '/' + adminPath + '/grid/save',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    id: id,
                    value: value,
                    field: field,
                    model: model
                },
                success: function(result) {
                    if (result.type === 'error') {
                        $(e.target).text($(e.target).attr('data-field-value'));
                    }

                    notice(result.type, result.message);
                }
            });

            $(this).blur();

            return false;
        }
    }).on('change', 'select.select-grid', function(e) {
        var selected = $(this).val();

        var id = $(this).attr('data-entry-id'),
            field = $(this).attr('data-field-name'),
            value = $(this).attr('data-field-value');

        if (value != selected) {
            $.ajax({
                type: 'post',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/' + adminPath + '/grid/save',
                data: {
                    id: id,
                    model: model,
                    field: field,
                    value: selected
                },
                success: function(result) {
                    if (result.type === 'error') {
                        $(e.target).val($(e.target).attr('data-field-value')).trigger('change.select2');
                    }

                    notice(result.type, result.message);

                    $(e.target).attr('data-field-value', selected);
                }
            });
        }
    }).on('click', 'td', function() {
        var $div = $(this).find('div[contenteditable="true"]');

        if ($div.length) {
            $div.focus();
        }
    });

    /* Удаление записи. */
    $('body').on('click', '.delete-item-confirm', function() {
        if (! $(this).attr('disabled')) {
            var link = $(this).attr('data-link');
            var text = $(this).attr('data-confirm-text');
            var $tr = $(this).closest('tr');

            /* Счётчик активного пункта меню. */
            var $count = $('.sidebar li.active:last').find('span.label');

            swal({
                title: text,
                type: "warning",
                closeOnCancel: true,
                closeOnConfirm: true,
                showCancelButton: true,
                showLoaderOnConfirm: true,
                confirmButtonColor: "#EF5350",
                cancelButtonText: $tagLanguage.attr('data-no-cancel'),
                confirmButtonText: $tagLanguage.attr('data-yes-delete')
            }, function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: link,
                        type: 'get',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(result) {
                            if (result.error) {
                                swal({
                                    type: "error",
                                    title: result.error,
                                    confirmButtonColor: "#2196F3"
                                });
                            } else {
                                $table.row($tr).remove().draw(false);

                                /* Обновление счётчика в меню, если он существует. */
                                $.ajax({
                                    type: 'post',
                                    url: '/' + adminPath + '/grid/update-count-menu-item',
                                    data: {
                                        model: model
                                    },
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    },
                                    success: function(count) {
                                        $count.text(parseInt(count));
                                    }
                                });

                                notice('success', $tagLanguage.attr('data-destroyed'));
                            }
                        }
                    });
                }
            });
        }
    }).on('click', '[data-refuse]', function () {
        var id = $(this).attr('data-refuse');

        $.ajax({
            type: 'post',
            url: '/refuse-event',
            data: {
                id: id
            },
            success: function (result) {
                var type = result.error ? 'error' : 'success';
                var message = result.error ? result.error : result.message;

                notice(type, message);

                $table.ajax.reload();
            }
        })
    }).on('click', '[data-confirmed]', function () {
        var id = $(this).attr('data-confirmed');

        $.ajax({
            type: 'post',
            url: '/confirm-event',
            data: {
                id: id
            },
            success: function (result) {
                var type = result.error ? 'error' : 'success';
                var message = result.error ? result.error : result.message;

                notice(type, message);

                $table.ajax.reload();
            }
        })
    });
});

$.fn.serializeAssoc = function () {
    var data = {};

    $.each(this.serializeArray(), function (key, obj) {
        var a = obj.name.match(/(.*?)\[(.*?)\]/);

        if (a !== null) {
            var subName = a[1],
                subKey = a[2];

            if (! data[subName]) {
                data[subName] = [];
            }

            if (! subKey.length) {
                subKey = data[subName].length;
            }

            if (data[subName][subKey]) {
                if ($.isArray(data[subName][subKey])) {
                    data[subName][subKey].push(obj.value);
                } else {
                    data[subName][subKey] = [];
                    data[subName][subKey].push(obj.value);
                }
            } else {
                data[subName][subKey] = obj.value;
            }
        } else {
            if (data[obj.name]) {
                if ($.isArray(data[obj.name])) {
                    data[obj.name].push(obj.value);
                } else {
                    data[obj.name] = [];
                    data[obj.name].push(obj.value);
                }
            } else {
                data[obj.name] = obj.value;
            }
        }
    });

    return data;
};