$(document).ready(function () {
    $('body').on('click', ':checkbox', function () {
        var value = this.checked === true ? 1 : 0,
            $checkbox = $(this).attr('checked', this.checked).prop('checked', this.checked);

        $(this).val(value);
        $.uniform.update($checkbox);
    }).on('click', '.remove_select_autocomplete', function () {
        var $block = $(this).closest('.form-fields');

        if ($block.length == 0) {
            $block = $(this).closest('.form-group');
        }

        $block.find('input[type="hidden"]').val('0');

        $block.find('select:not(.multiselect-select-all-filtering)').html('<option value selected></option>');

        $block.find('select:not(.multiselect-select-all-filtering)').trigger('change.select2');
        $block.find('.multiselect-select-all-filtering').multiselect('deselectAll', false).multiselect('refresh');
    }).on('click', '.remove_input', function () {
        var $block = $(this).closest('.input-group');

        $block.find('input').val('').change();
    }).on('change', '.ajax-select', function () {
        var table = $(this).val(),
            key = $(this).attr('data-ajax-key'),
            value = $(this).attr('data-ajax-value'),
            model = $(this).attr('data-ajax-model'),
            group = $(this).attr('data-ajax-group'),
            language = $(this).attr('data-ajax-language'),
            relation = $(this).attr('data-ajax-relation'),
            className = $('.sortable-fields').length ? 'form-fields' : 'row',
            $block = $(this).closest('.' + className),
            id = className != 'row' ? group + '_' + group + '_' + relation : group + '_' + relation;

        if (key != '' && value != '' && model != '' && relation != '') {
            $.ajax({
                type: 'post',
                url: '/' + adminPath + '/roles/select',
                data: {
                    key: key,
                    table: table,
                    value: value,
                    model: model,
                    language: language !== undefined && language != '' ? language : ''
                },
                success: function (data) {
                    var $select = $block.find('#' + id);

                    if ($select.hasClass('multiselect-select-all-filtering')) {
                        $select.multiselect('destroy');
                    } else {
                        $select.select2('destroy');
                    }

                    $select.html(data);

                    /* Обновление селектов. */
                    updateSelects();
                }
            });
        }
    });

    var $ajaxSelects = $('.ajax-select');
    if ($ajaxSelects.length) {
        $.each($ajaxSelects, function() {
            var table = $(this).val(),
                key = $(this).attr('data-ajax-key'),
                value = $(this).attr('data-ajax-value'),
                model = $(this).attr('data-ajax-model'),
                group = $(this).attr('data-ajax-group'),
                language = $(this).attr('data-ajax-language'),
                relation = $(this).attr('data-ajax-relation'),
                className = $('.sortable-fields').length ? 'form-fields' : 'row',
                $block = $(this).closest('.' + className),
                id = className != 'row' ? group + '_' + group + '_' + relation : group + '_' + relation;

            if (key != '' && value != '' && model != '' && relation != '') {
                $.ajax({
                    type: 'post',
                    url: '/' + adminPath + '/roles/select',
                    data: {
                        key: key,
                        table: table,
                        value: value,
                        model: model,
                        mainId: $(this).closest('.sortable-fields').attr('data-main-id'),
                        language: language !== undefined && language != '' ? language : '',
                        relationModel: $(this).closest('.sortable-fields').attr('data-builder-model')
                    },
                    success: function (data) {
                        var $select = $block.find('#' + id);

                        if ($select.hasClass('multiselect-select-all-filtering')) {
                            $select.multiselect('destroy');
                        } else {
                            $select.select2('destroy');
                        }

                        $select.html(data);

                        /* Обновление селектов. */
                        updateSelects();
                    }
                });
            }
        });
    }

    /* Применение плагинов к типам полей. */
    updateScripts();

    /* Slug строк в реальном времени. */
    var $slugFields = $('[data-slug]');

    if ($slugFields.length) {
        $.each($slugFields, function() {
            var $name = $(this),
                slug = $(this).attr('data-slug'),
                group = $(this).attr('data-group');

            $('input[name="' + group + '_' + slug  + '"]').on('keyup', function() {
                var value = aliasSlug($(this).val());

                $name.val(value);
            });
        });
    }

    /* Сохранение формы по нажатию Ctrl+S. */
    $('form').on('keydown', function(e) {
        if (e.ctrlKey && e.keyCode === 83) {
            e.preventDefault();

            $(this).find('.buttons button#save-redirect').click();

            return false;
        }
    });

    var form = '',
        first = '',
        form_group = '';

    $('form.main-form .buttons button').on('click', function() {
        var redirect = $(this).attr('id') === 'save-reload';

        $('form.main-form').ajaxForm({
            beforeSerialize: function(f, options) {
                form = f.attr('id');
                form_group = f.attr('data-group-tab');

                $('form#' + form + ' button.save-changes').attr('disabled', true);

                var $selectAjax = $('.select-ajax[multiple]');

                $.each($selectAjax, function() {
                    if ($(this).val() == null || $(this).val() == '' || $(this).val() == undefined) {
                        $(this).val(0);
                    }
                });

                var $selects = $('.form-fields-builder select');

                $.each($selects, function() {
                    if ($(this).val() == null || $(this).val() == '' || $(this).val() == undefined) {
                        $(this).prepend('<option value="0" selected></option>');

                        /* Обновление селектов. */
                        updateSelects();
                    }
                });

                $('textarea.tinymce').each(function (index, item) {
                    $(item).html(tinyMCE.editors[index].getContent());
                });

                $('textarea.ckeditor').each(function (index, item) {
                    $(item).html(CKEDITOR.instances[$(item).attr('id')].getData());
                });
            },
            beforeSubmit: function() {
                $('form#' + form + ' .save-changes').attr('disabled', true);
                $('form#' + form + ' .save-load-changes').removeClass('hidden');
            },
            success: function (result) {
                /**
                 * @param {{checkFields:array}} Список полей, которые заполнены не правильно.
                 */
                var checkFields = result.checkFields,
                    type = checkFields ? 'error' : 'success';

                $('form#' + form + ' .save-load-changes').addClass('hidden');
                $('form#' + form + ' .save-changes').attr('disabled', false);

                $('label').css('color', 'inherit');

                if (form !== 'form-base' || type === 'error') {
                    notice(type, result.message);
                }

                if (checkFields) {
                    form_group = (form_group !== undefined) ? form_group + '_' : '';

                    $.each(checkFields, function(field) {
                        var labelField = $('label[for="' + form_group + checkFields[field] + '"]');

                        labelField.css('color', 'red');

                        if (first === '') {
                            first = field;

                            var destination = labelField.offset().top;

                            $('html').animate({
                                scrollTop: destination
                            }, 100);
                        }
                    });

                    return false;
                } else if (result.message) {
                    if (form === 'form-base') {
                        if (redirect) {
                            if ($.isNumeric(+result.message)) {
                                var currentUrl = window.location.href.replace('create', 'edit/');

                                window.location.href = currentUrl + result.message;
                            } else {
                                location.reload(true);
                            }
                        } else {
                            window.location.href = $('form#' + form + ' #form-cancel').attr('href');
                        }
                    } else {
                        $('html').animate({
                            scrollTop: $('body').offset().top
                        }, 100);
                    }
                }
            },
            error: function () {
                $('form#' + form + ' .save-load-changes').addClass('hidden');
                $('form#' + form + ' .save-changes').attr('disabled', false);
            }
        });
    });

    var $sortableHash = $('.sortable_hash');

    if ($sortableHash.length > 0) {
        $sortableHash.each(function () {
            $(this).sortable({
                axis: 'y',
                cancel: '',
                opacity: 0.5,
                cursor: 'move',
                tolerance: 'pointer',
                handle: 'button.btn-primary',
                containment: $(this).closest('.array_type_data')
            });
        });

        $(document).on('click', '.add_more_element_hash', function () {
            var $current = $(this).closest('.type_array');

            $current.find('.colorpicker-show-input').spectrum('destroy');
            $current.find('.multiselect-select-all-filtering').multiselect('destroy');
            $current.find('select:not(.multiselect-select-all-filtering)').select2('destroy');

            var $new = $current.clone();
            $current.after($new);

            /* Применение плагина загрузки изображений для динамически добавленого поля. */
            updateFilesUpload();

            /* Применение плагинов к типам полей. */
            updateScripts();

            $new.find('.template-download:last-child').empty();
            $new.find('select:not(.multiselect-select-all-filtering)').val('').change();
            $new.find('.multiselect-select-all-filtering').multiselect('deselectAll', false).multiselect('refresh');

            $new.find('input[type="hidden"]').val('0');
            $new.find('input:not(:checkbox), textarea, select').val('');

            $('.sortable_hash').sortable('refresh');

            $new.find('input.first').focus();
        }).on('click', '.remove_more_element_hash', function () {
            if ($(this).closest('.array_type_data').find('.type_array').length > 1) {
                $(this).closest('.type_array').remove();
            } else {
                var $array = $(this).closest('.type_array');

                $array.find('input[type="hidden"]').val('0');
                $array.find('input:not(:checkbox), textarea, select').val('');
            }
        });
    }

    var $sortableBuilderHash = $('.sortable_builder_hash');

    if ($sortableBuilderHash.length > 0) {
        $.each($sortableBuilderHash, function() {
            $(this).sortable({
                handle: 'button.btn-builder-move',
                cancel: '',
                axis: 'y',
                opacity: 0.5,
                cursor: 'move',
                tolerance: 'pointer',
                containment: $(this).closest('.form-fields-builder')
            });
        });

        $(document).on('click', '.form-fields-builder .add_more_builder_elements_hash', function () {
            var $current = $(this).closest('.sortable-fields');

            $current.find('.colorpicker-show-input').spectrum('destroy');
            $current.find('select:not(.multiselect-select-all-filtering)').select2('destroy');
            $current.find('.multiselect-select-all-filtering').multiselect('destroy');

            tinymce.remove('#' + $current.find('.tinymce').attr('id'));

            var $new = $current.clone(),
                arrCount = $(this).parents('.form-fields-builder').find('.sortable-fields').length;

            if ($new.find('.builder-count-block').length > 0) {
                $.each($new.find('.builder-count-block'), function() {
                    var fieldName = $(this).attr('name').split('[')[0],
                        newFieldName = $(this).hasClass('builder-multiple') ? fieldName + '[' + arrCount + '][]' : fieldName + '[]';

                    $(this).attr('name', newFieldName);
                });
            }

            $new.find('.template-download').remove();
            $new.find('input:not(:checkbox), textarea').val('');
            $new.find('input[type="hidden"], input[oninput]').val('0');
            $new.find('.sortable_hash').find('.type_array:not(:first-child)').remove();
            $new.find('.tinymce').attr('id', $new.find('.tinymce').attr('id') + arrCount);

            $.each($new.find('.pickatime'), function() {
                $(this).attr('id', $(this).attr('id') + arrCount);
            });

            $.each($new.find('.pickdatetime'), function() {
                $(this).attr('id', $(this).attr('id') + arrCount);
            });

            $current.after($new);

            /* Применение плагина загрузки изображений для динамически добавленного поля. */
            updateFilesUpload();

            /* Применение плагинов к типам полей. */
            updateScripts();

            $.each($new.find('select.duplicate:not(.multiselect-select-all-filtering)'), function() {
                var value = $(this).find('option:last-child').attr('value');

                $(this).val(value).change();
            });

            $new.find('select:not(.multiselect-select-all-filtering):not(.duplicate)').val('').change();
            $new.find('.multiselect-select-all-filtering').multiselect('deselectAll', false).multiselect('refresh');

            $sortableHash = $('.sortable_hash');
            if ($sortableHash.length > 1) {
                $.each($sortableHash, function() {
                    $(this).sortable({
                        axis: 'y',
                        cancel: '',
                        opacity: 0.5,
                        cursor: 'move',
                        tolerance: 'pointer',
                        handle: 'button.btn-primary',
                        containment: $(this).closest('.array_type_data')
                    });
                });
            }

            $('.sortable_builder_hash').sortable('refresh');
        }).on('click', '.form-fields-builder .remove_more_builder_elements_hash', function () {
            if ($(this).closest('.form-fields-builder').find('.sortable-fields').length > 1) {
                $(this).closest('.sortable-fields').remove();
            } else {
                var $fields = $(this).closest('.sortable-fields');

                $fields.find('input[type="hidden"]').val('0');
                $fields.find('input:not(:checkbox), textarea').val('');

                $fields.find('.template-download').remove();

                var selectVal = $fields.find('select:not(.multiselect-select-all-filtering)').find('option[value="0"]').length ? 0 : null;

                $fields.find('select:not(.multiselect-select-all-filtering)').val(selectVal).trigger('change.select2');
                $fields.find('.multiselect-select-all-filtering').multiselect('deselectAll', false).multiselect('refresh');
            }
        });
    }

    var $sortableFiles = $('.sortable-files');

    if ($sortableFiles.length > 0) {
        $sortableFiles.each(function () {
            $(this).sortable({
                axis: 'y',
                opacity: 0.5,
                cursor: 'move',
                tolerance: 'pointer',
                cancel: 'span, button',
                beforeStop: function() {
                    var files = [];
                    var $field = $(this).closest('.col-xs-12').find('.jquery-file');

                    $.each($(this).closest('.col-xs-12').find('a.image-viewer'), function(index, value) {
                        files[index] = $(this).attr('href');
                    });

                    files = files.join(',');

                    $field.val(files);
                },
                stop: function() {
                    $('tr.ui-sortable-placeholder').remove();
                },
                containment: $(this).closest('.table-sortable')
            });
        });
    }

    /* Сохранение выбранного таба в сессию. */
    $('[data-ariol-tab]').on('click', function() {
        var tab = $(this).attr('data-ariol-tab');

        $.ajax({
            type: 'post',
            url: '/' + adminPath + '/save-tab',
            data: {tab: tab},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

    /* Рекурсивные хлебные крошки. */
    $($('.sidebar-main .active').get().reverse()).each(function () {
        $('ul.breadcrumb').prepend('<li>' +
            (($(this).find('a') && $(this).find('a').attr('href') !== undefined) ? '<a href="' + $(this).find('a').attr('href') + '">' : '') +
                $(this).find('span.sidebar-menu-name:first').text().trim() +
            (($(this).find('a') && $(this).find('a').attr('href') !== undefined) ? '</a>' : '') +
        '</li>');
    });

    if ($('tbody.files').length) {
        /* Применение плагина просмотра изображений к медиафайлам. */
        updateGallery();
    }

    if ($('.jquery-file').length) {
        /* Применение плагина загрузки изображений для динамически добавленого поля. */
        updateFilesUpload();
    }
});

/* Обновление плагина загрузки файлов. */
function updateFilesUpload() {
    var $files = [];

    $.each($('.jquery-file'), function(index, value) {
        if ($(this).hasClass('multiple-files')) {
            $files[index] = [];

            $.each($(this).closest('.col-xs-12').find('.image-viewer'), function(i, image) {
                $files[index].push($(image).attr('href'));
            });

            $(this).val($files[index]);
        }

        var html = $(this).closest('.col-xs-12').find('.template-download').html();

        $('.file-upload-count').fileupload({
            url: '/' + adminPath + '/file-uploader',
            maxFileSize: 25000000,
            previewCrop: true,
            autoUpload: true,
            dropZone: $(this),
            destroy: function(e, data) {
                swal({
                    text: "",
                    type: "warning",
                    closeOnConfirm: true,
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    title: $tagLanguage.attr('data-delete-are-you-sure'),
                    cancelButtonText: $tagLanguage.attr('data-no-cancel'),
                    confirmButtonText: $tagLanguage.attr('data-yes-delete')
                },
                function() {
                    var $data = data.url.split('?selectedFilesForUploa=');

                    $.ajax({
                        type: 'post',
                        url: '/' + adminPath + '/delete-thumbnail',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: {file: $data[1]},
                        success: function() {}
                    });

                    var number = $(e.target).closest('.col-xs-12').children('.jquery-file').index('.jquery-file');

                    if ($files[number] === undefined) {
                        $(e.target).closest('.col-xs-12').children('.jquery-file').val('');
                        $(data.context).remove();
                    } else {
                        $.each($files[number], function (i, file) {
                            var getFile = data.url.split('?selectedFilesForUploa=')[0],
                                getFileTemp = '/temp/' + data.url.split('?selectedFilesForUploa=')[1];

                            if (file === getFile || file === getFileTemp) {
                                delete $files[number][i];
                            }
                        });

                        $(data.context).remove();

                        $(e.target).closest('.col-xs-12').children('.jquery-file').val($files[number]);
                    }
                });
            },
            process: function(data, td) {
                var $td = td.files[0].name,
                    number = $(this).index('.file-upload-count'),
                    $td_name = $('[data-td-file-name="' + $td + '"]');

                $td_name.prepend('<img src="/ariol/assets/images/custom/file.svg">');

                if ($files[number] === undefined) {
                    $(this).find('.template-download').remove();

                    $(this).find('.cancel').on('click', function() {
                        $(this).find('.template-upload').addClass('template-download').removeClass('template-upload');
                        $(this).find('.template-download').html(html);
                    });
                } else {
                    var $arr = $td.split('.');

                    if ($.inArray($arr[1], ['jpg', 'jpeg', 'png', 'gif', 'bmp', 'svg', 'ico']) > -1) {
                        $('[data-td-file-name="' + $td + '"] span.preview').show();
                        $('[data-td-file-name="' + $td + '"] img').remove();
                    }
                }
            },
            success: function(data) {
                /**
                 * @param {{selectedFilesForUpload:array}} Список загружаемых файлов.
                 */
                var selectedFilesForUpload = data.selectedFilesForUpload,
                    number = $(this).closest('.col-xs-12').children('.jquery-file').index('.jquery-file');

                if ($files[number] === undefined) {
                    $(this).closest('.col-xs-12').children('.jquery-file').val(selectedFilesForUpload[0].url);
                } else {
                    $.each(selectedFilesForUpload, function (j, file) {
                        $files[number].push(file.url);
                    });

                    $(this).closest('.col-xs-12').children('.jquery-file').val($files[number]);
                }
                var widthimg = 300;
                var heightimg = 200;
                var basic = $('.crop').croppie({
                    viewport: {
                        width: widthimg,
                        height: heightimg
                    }
                });
                basic.croppie('bind', {
                    url: selectedFilesForUpload[0].url
                });

                $('.basic-result').on('click', function() {
            			var w = parseInt(widthimg, 10),
            				h = parseInt(heightimg, 10),s
            				size = 'viewport';
            			if (w || h) {
            				size = { width: w, height: h };
            			}
            			basic.croppie('result', {
            				type: 'canvas',
            				size: size,
            				resultSize: {
            					width: 50,
            					height: 50
            				}
            			}).then(function (resp) {
                    alert(resp);
            				$('input[name='+$(this).data('target')+"]").val(resp);
            			});
            		});


            }
        }).on('fileuploadfinished', function() {
            updateGallery();
        });
    });
}

/* Примение плагина просмотра изображений.  */
function updateGallery() {
    $('tbody.files').find('td').lightGallery({
        pager: true,
        thumbnail: true,
        animateThumb: false,
        selector: '.image-viewer',
        showThumbByDefault: false
    });
}

/* Обновление селектов. */
function updateSelects() {
    var $notMultiSelectFiltering = $('select:not(.multiselect-select-all-filtering)');

    /* Выбор одного элемента из списка. */
    if ($notMultiSelectFiltering.length > 0) {
        $.each($notMultiSelectFiltering, function () {
            $(this).select2({
                placeholder: $tagLanguage.attr('data-select-item'),
                language: {
                    noResults: function() {
                        return $tagLanguage.attr('data-no-founded');
                    }
                },
                escapeMarkup: function (markup) {
                    return markup;
                }
            });
        });
    }

    var $multiSelectFiltering = $('.multiselect-select-all-filtering');

    /* Выбор нескольких элементов из списка. */
    if ($multiSelectFiltering.length > 0) {
        $.each($multiSelectFiltering, function () {
            $(this).multiselect({
                maxHeight: 300,
                includeSelectAllOption: true,
                enableClickableOptGroups: true,
                enableFiltering: true,
                templates: {
                    filter: '<li class="multiselect-item multiselect-filter">' +
                        '<i class="icon-search4"></i> <input class="form-control" type="text">' +
                    '</li>'
                },
                selectAllText: $tagLanguage.attr('data-select-all'),
                allSelectedText: $tagLanguage.attr('data-add-selected'),
                nSelectedText: $tagLanguage.attr('data-selected'),
                nonSelectedText: $tagLanguage.attr('data-no-selected'),
                filterPlaceholder: $tagLanguage.attr('data-search'),
                enableCaseInsensitiveFiltering: true,
                onInitialized: function(select, container) {
                    $('.styled, .multiselect-container input').uniform({
                        radioClass: 'choice'
                    });
                },
                onSelectAll: function() {
                    $.uniform.update();
                },
                onChange: function() {
                    $.uniform.update();
                }
            });
        });
    }

    var $selectAjax = $('.select-ajax');

    if ($selectAjax.length > 0) {
        $.each($selectAjax, function() {
            var url = $(this).attr('data-url');

            $(this).select2({
                placeholder: $tagLanguage.attr('data-input-text'),
                ajax: {
                    type: 'post',
                    url: url,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term,
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        return {results: data};
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                },
                minimumInputLength: 1,
                formatResult: formatResult,
                formatSelection: formatSelection,
                language: {
                    inputTooShort: function () {
                        return $tagLanguage.attr('data-input-1-symbol');
                    },
                    inputTooLong: function() {
                        return $tagLanguage.attr('data-input-too-long');
                    },
                    errorLoading: function() {
                        return $tagLanguage.attr('data-input-error-loading')
                    },
                    loadingMore: function() {
                        return $tagLanguage.attr('data-input-loading-more');
                    },
                    noResults: function() {
                        return $tagLanguage.attr('data-input-no-results');
                    },
                    searching: function() {
                        return $tagLanguage.attr('data-input-searching');
                    },
                    maximumSelected: function() {
                        return $tagLanguage.attr('data-input-maximum-selected');
                    }
                }
            });
        });

        function formatResult(item) {
            return '<div>' + item.title + '</div>';
        }

        function formatSelection(data) {
            return data.title;
        }
    }
}

/* Обновление плагинов для типов полей. */
function updateScripts() {
    /* Обновление селектов. */
    updateSelects();

    /* Маска для ввода телефонного номера. */
    $('.phone-mask').each(function() {
        var placeholder = $(this).attr('placeholder'),
            mask = placeholder.replace(/_/g , '0');

        if (placeholder !== '') {
            $(this).mask(mask);
        }
    });

    var $pickDate = $('.pickadate');

    /* Выбор даты. */
    if ($pickDate.length > 0) {
        $pickDate.pickadate({
            monthsFull: [
                $tagLanguage.attr('data-date-January'), $tagLanguage.attr('data-date-February'), $tagLanguage.attr('data-date-March'), $tagLanguage.attr('data-date-April'),
                $tagLanguage.attr('data-date-May'), $tagLanguage.attr('data-date-June'), $tagLanguage.attr('data-date-July'), $tagLanguage.attr('data-date-August'),
                $tagLanguage.attr('data-date-September'), $tagLanguage.attr('data-date-October'), $tagLanguage.attr('data-date-November'), $tagLanguage.attr('data-date-December')
            ],
            weekdaysShort: [
                $tagLanguage.attr('data-date-Sun'), $tagLanguage.attr('data-date-Mon'), $tagLanguage.attr('data-date-Tue'),
                $tagLanguage.attr('data-date-Wed'), $tagLanguage.attr('data-date-Thu'), $tagLanguage.attr('data-date-Fri'), $tagLanguage.attr('data-date-Sat')
            ],
            showMonthsShort: undefined,
            showWeekdaysFull: undefined,
            closeOnSelect: true,
            closeOnClear: true,

            clear: '',
            close: $tagLanguage.attr('data-date-close'),
            today: $tagLanguage.attr('data-date-today'),

            format: "yyyy-mm-dd",

            labelMonthNext: $tagLanguage.attr('data-date-next-month'),
            labelMonthPrev: $tagLanguage.attr('data-date-prev-month'),
            labelMonthSelect: $tagLanguage.attr('data-date-select-month'),
            labelYearSelect: $tagLanguage.attr('data-date-select-year')
        });
    }

    var $pickTime = $('.pickatime');

    /* Выбор времени. */
    if ($pickTime.length > 0) {
        $pickTime.AnyTime_noPicker().AnyTime_picker({
            format: "%H:%i",
            labelTitle: $tagLanguage.attr('data-date-select-time'),
            labelMinute: $tagLanguage.attr('data-date-minutes'),
            labelHour: $tagLanguage.attr('data-date-hour')
        });
    }

    var $pickDateTime = $('.pickdatetime');

    if ($pickDateTime.length > 0) {
        $('.pickdatetimepicker').on('click', function(e) {
            $pickDateTime.AnyTime_noPicker().AnyTime_picker({
                labelTitle: $tagLanguage.attr('data-date-select-date-and-time'),
                labelMonth: $tagLanguage.attr('data-date-month'),
                labelMinute: $tagLanguage.attr('data-date-minutes'),
                labelSecond: $tagLanguage.attr('data-date-seconds'),
                labelHour: $tagLanguage.attr('data-date-hour'),
                labelYear: $tagLanguage.attr('data-date-year'),
                labelDayOfMonth: $tagLanguage.attr('data-date-day'),
                dayAbbreviations: [
                    $tagLanguage.attr('data-date-Mon'), $tagLanguage.attr('data-date-Tue'), $tagLanguage.attr('data-date-Wed'),
                    $tagLanguage.attr('data-date-Thu'), $tagLanguage.attr('data-date-Fri'), $tagLanguage.attr('data-date-Sat'), $tagLanguage.attr('data-date-Sun')
                ],
                monthAbbreviations: [
                    $tagLanguage.attr('data-date-Jan'), $tagLanguage.attr('data-date-Feb'), $tagLanguage.attr('data-date-Mar'), $tagLanguage.attr('data-date-Apr'),
                    $tagLanguage.attr('data-date-May'), $tagLanguage.attr('data-date-Jun'), $tagLanguage.attr('data-date-Jul'), $tagLanguage.attr('data-date-Aug'),
                    $tagLanguage.attr('data-date-Sep'), $tagLanguage.attr('data-date-Oct'), $tagLanguage.attr('data-date-Nov'), $tagLanguage.attr('data-date-Dec')
                ]
            }).focus();

            e.preventDefault();
        });
    }

    var $colorPicker = $('.colorpicker-show-input');

    /* Выбор цвета. */
    if ($colorPicker.length > 0) {
        var localization = $.spectrum.localization[currentAdminLanguage] = {
            cancelText: $tagLanguage.attr('data-color-cancel'),
            chooseText: $tagLanguage.attr('data-color-choose'),
            clearText: $tagLanguage.attr('data-color-clear'),
            noColorSelectedText: $tagLanguage.attr('data-color-no-selected'),
            togglePaletteMoreText: $tagLanguage.attr('data-color-more'),
            togglePaletteLessText: $tagLanguage.attr('data-color-hide')
        };

        $.extend($.fn.spectrum.defaults, localization);

        $colorPicker.spectrum({
            showInput: true
        });
    }

    /* Выбор места на карте. */
    $('.addressPicker').each(function() {
        var id = $(this).attr('data-id-address'),
            $lat = $('#' + id + '_lat'),
            $lng = $('#' + id + '_lng');

        var address = ($(this).val() !== '') ? $(this).val() : '',
            lat = ($lat.val() !== '') ? $lat.val() : '53.9045398',
            lng = ($lng.val() !== '') ? $lng.val() : '27.561524400000053';

        $('#' + id).val(address);

        $lat.val(lat);
        $lng.val(lng);

        var $addressPickerMap = $(this).addresspicker({
            regionBias: "by",
            reverseGeocode: true,
            updateCallback: getCoordinates,
            mapOptions: {
                zoom: 17,
                center: new google.maps.LatLng(lat, lng),
                scrollwheel: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            },
            elements: {
                map: '#' + id + '_map'
            }
        });

        var $marker = $addressPickerMap.addresspicker('marker');
        $marker.setVisible(true);

        $addressPickerMap.addresspicker('updatePosition');

        function getCoordinates(geocodeResult, parsedGeocodeResult) {
            $lat.val(parsedGeocodeResult.lat);
            $lng.val(parsedGeocodeResult.lng);
        }
    });

    /* Текстовый редактор. */
    if ($('.tinymce').length > 0) {
        tinyMCE.init({
            height: 200,
            theme: 'modern',
            selector: '.tinymce',
            forced_root_block : '',
            force_p_newlines : false,
            force_br_newlines : false,
            language: currentAdminLanguage,
            fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
            ],
            toolbar1: 'undo redo | styleselect | sizeselect | bold italic | fontselect |  fontsizeselect | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | preview'
        });
    }

    /* Автозаполнение полей. */
    $('.autoComplete').each(function (i, el) {
        function split(val) {
            return val.split(/,\s*/);
        }

        function extractLast(term) {
            return split(term).pop();
        }

        $(el).autocomplete({
            minLength: 0,
            source: function (request, response) {
                $.ajax({
                    url: '/' + adminPath + '/autoComplete',
                    dataType: 'json',
                    data: {
                        field: $(el).data('field'),
                        model: $(el).data('model'),
                        value: extractLast($(el).val())
                    },
                    success: function (data) {
                        response($.ui.autocomplete.filter(data, extractLast(request.term)));
                    }
                });
            },
            focus: function () {
                return false;
            },
            select: function (event, ui) {
                var terms = split(this.value);

                // Удаление текущего значения.
                terms.pop();

                // Добавление выбранного элемента.
                terms.push(ui.item.value);

                // Добавление запятой и пробела после выбранного значения.
                terms.push("");
                this.value = terms.join(", ");

                return false;
            }
        });
    });
}

/* Применение плагина автокомплита адресов от яндекса. */
function onLoad(ymaps) {
    $.each($('.yandex-address'), function() {
        var suggestView = new ymaps.SuggestView($(this).attr('id'), {
            boundedBy: [[56.149907, 23.109954], [51.116532, 32.909759]]
        });
    });
}
