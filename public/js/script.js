$.fn.equalizeHeights = function () {
    var maxHeight = this.map(function (i, e) {
        return $(e).height();
    }).get();

    return this.height(Math.max.apply(this, maxHeight));
};

var generateSlimScroll = function (element) {
    var dataHeight = $(element).attr('data-height');
    dataHeight = (!dataHeight) ? $(element).height() : dataHeight;

    $(element).mCustomScrollbar({
        setHeight: dataHeight,
        //autoHideScrollbar: true,
        theme: "dark-3",
        scrollInertia: 100,
        callbacks: {
            onScrollStart: function () {
                $(this).removeClass('mCS_reach_end');
            },
            onTotalScroll: function () {
                $(this).addClass('mCS_reach_end');
            }
        }
    });
};

function scrollbarWidth() {
    var block = $('<div>').css({'height': '50px', 'width': '50px'}),
        indicator = $('<div>').css({'height': '200px'});

    $('body').append(block.append(indicator));
    var w1 = $('div', block).innerWidth();
    block.css('overflow-y', 'scroll');
    var w2 = $('div', block).innerWidth();
    $(block).remove();
    return (w1 - w2);
}

function initDatetimepicker($datetimepicker) {
    if (!$datetimepicker || !$datetimepicker.length) {
        $datetimepicker = $('.datetimepicker');
    }

    $datetimepicker.datetimepicker({
        format: "DD.MM.YYYY",
        locale: 'ru',
        //inline: true,
        //debug: true,
        useCurrent: false,
        sideBySide: true,
        widgetPositioning: {
            //horizontal: 'auto',
            //vertical: 'bottom'
        },
        icons: {
            previous: "fa fa-angle-left",
            next: "fa fa-angle-right"
        },
        tooltips: {
            selectMonth: 'Выбрать месяц',
            prevMonth: 'Предыдущий месяц',
            nextMonth: 'Следующий месяц',
            selectYear: 'Выбрать год',
            prevYear: 'Предыдущий год',
            nextYear: 'Следующий год',
            selectDecade: 'Выбрать десятилетие',
            prevDecade: 'Предыдущее десятилетие',
            nextDecade: 'Следующее десятилетие',
            prevCentury: 'Предыдущий век',
            nextCentury: 'Следующий век'
        }
    });
}

function initClockpicker($clockpicker) {
    if (!$clockpicker || !$clockpicker.length) {
        $clockpicker = $('.clockpicker');
    }

    $clockpicker.datetimepicker({
        format: 'LT',
        locale: 'ru',
        //inline: true,
        //debug: true,
        useCurrent: false,
        sideBySide: true,
        widgetPositioning: {
            //horizontal: 'auto',
            //vertical: 'bottom'
        },
        icons: {
            previous: "fa fa-angle-left",
            next: "fa fa-angle-right"
        },
        tooltips: {
            selectMonth: 'Выбрать месяц',
            prevMonth: 'Предыдущий месяц',
            nextMonth: 'Следующий месяц',
            selectYear: 'Выбрать год',
            prevYear: 'Предыдущий год',
            nextYear: 'Следующий год',
            selectDecade: 'Выбрать десятилетие',
            prevDecade: 'Предыдущее десятилетие',
            nextDecade: 'Следующее десятилетие',
            prevCentury: 'Предыдущий век',
            nextCentury: 'Следующий век'
        }
    });
}

function initDaterangepicker($daterangepicker) {
    if (!$daterangepicker || !$daterangepicker.length) {
        $daterangepicker = $('.datepicker-here');
    }

    //$daterangepicker.datepicker();

    // $daterangepicker.daterangepicker({
    //     linkedCalendars: false,
    //     singleDatePicker: true,
    //     autoUpdateInput: false,
    //     autoApply: true,
    //     opens: "center",
    //     ranges: {
    //         'Сегодня': [moment(), moment()],
    //         'Неделя': [moment().subtract(6, 'days'), moment()],
    //         'В этом месяце': [moment().startOf('month'), moment().endOf('month')]
    //     },
    //     locale: {
    //         applyLabel: "Применить",
    //         cancelLabel: "Очистить",
    //         fromLabel: "От",
    //         toLabel: "До",
    //         customRangeLabel: "Выбрать дату",
    //         weekLabel: "Н",
    //     },
    //     minDate: moment()
    // });
}

function initTextEditor($htmlEditors) {
    if (!$htmlEditors || !$htmlEditors.length) {
        $htmlEditors = $('textarea.html-editor');
    }
    $htmlEditors.each(function (index, item) {
        tinymceId = 'tinymceId_' + (new Date()).getTime();
        $(this).attr('id', tinymceId);
        tinymce.init({
            selector: '#' + tinymceId,
            language: 'ru',
            menubar: false,
            height: 280,
            plugins: "link autolink paste",
            toolbar: [
                "bold italic underline strikethrough link removeformat alignleft"
            ],
            browser_spellcheck: true,
            default_link_target: "_blank",
            extended_valid_elements: 'a[href|target=_blank]',
            setup: function (editor) {
                editor.on('init', function () {
                    this.getBody().style.fontSize = '14px';
                }).on('change', function () {
                    editor.save();
                    //$(editor.targetElm);
                });
            },
            content_css: "./css/custom-tinymce.css"
        });
    });
}

function initTimePicker(timeElem) {
    var optionsLSL = {
        now: "00:00", //hh:mm 24 hour format only, defaults to current time 
        twentyFour: true, //Display 24 hour format, defaults to false 
        upArrow: 'wickedpicker__controls__control-up', //The up arrow class selector to use, for custom CSS 
        downArrow: 'wickedpicker__controls__control-down', //The down arrow class selector to use, for custom CSS 
        close: 'wickedpicker__close', //The close class selector to use, for custom CSS 
        hoverState: 'hover-state', //The hover state class to use, for custom CSS 
        title: 'Timepicker', //The Wickedpicker's title, 
        showSeconds: false, //Whether or not to show seconds, 
        secondsInterval: 1, //Change interval for seconds, defaults to 1  , 
        minutesInterval: 1, //Change interval for minutes, defaults to 1 
        beforeShow: null, //A function to be called before the Wickedpicker is shown 
        show: null, //A function to be called when the Wickedpicker is shown 
        clearable: false, //Make the picker's input clearable (has clickable "x")  
    };
}

// A $( document ).ready() block.
$(document).ready(function () {
    var id = 0;
    if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
        $('body').addClass('isIOS');
    }
    /*if(navigator.platform.indexOf('Mac') > -1 || /^((?!chrome|android).)*safari/i.test(navigator.userAgent)){
        $('body').addClass('isMac');
    }*/

    bWidth = $('body').width();

    $(window).load(function () {
    });
    $(window).scroll(function () {
    });
    $(window).resize(function () {
    });

    /*$(function($) {
        $.localScroll({
            duration: 1000,
            hash: false });
    })*/
    // ;

    $('[data-scrollbar=true]').each(function () {
        generateSlimScroll($(this));
    });

    initDaterangepicker();
    initDatetimepicker();
    initClockpicker();
    initTextEditor();

    $('.modal').on('show.bs.modal', function () {
        var $canvas = $('[canvas]');
        if ($canvas.get(0).scrollHeight > $canvas.height()) {
            $('body').css('padding-right', scrollbarWidth());
        }
    }).on('hidden.bs.modal', function () {
        $('body').css('padding-right', '');
    });

    /*$('#menu-dropdown').on('show.bs.dropdown', function () {
        $('body').addClass('menu-open').css('padding-right',scrollbarWidth());
    }).on('hide.bs.dropdown', function () {
        $('body').removeClass('menu-open').css('padding-right','');
    });*/

    var controller = new slidebars();
    controller.init();
    // Add close class to canvas container when Slidebar is opened
    $(controller.events).on('opening', function (event) {
        $('[canvas]').addClass('js-close-any');
        $('body').addClass('menu-open');
    });
    // Add close class to canvas container when Slidebar is opened
    $(controller.events).on('closing', function (event) {
        $('[canvas]').removeClass('js-close-any');
        $('body').removeClass('menu-open');
    });
    $('.js-open-demo-left').on('click', function (event) {
        event.preventDefault();
        event.stopPropagation();
        controller.toggle('demo-left');
    });
    $(document).on('click', '.js-close-any', function (event) {
        if (controller.getActiveSlidebar()) {
            event.preventDefault();
            event.stopPropagation();
            controller.close();
        }
    });


    function setFullBannerOffset() {
        var $fullPageBanner = $('.full-page-banner');
        var $canvasContainer = $('[canvas="container"]');
        if ($('body').hasClass('with-full-page-promo') &&
            $fullPageBanner.is(':visible') &&
            $canvasContainer.get(0).scrollHeight > $canvasContainer.height()) {
            $fullPageBanner.css('margin-right', scrollbarWidth());
        }
    }

    setFullBannerOffset();

    function generateMainSlider($swiperContainer) {
        var sliderId = $swiperContainer.attr('id');
        if (!sliderId) {
            return 0;
        }
        var mainSlider = new Swiper('#' + sliderId, {
            slidesPerGroup: 4,
            spaceBetween: 30,
            slidesPerView: 4,
            loop: true,
            loopFillGroupWithBlank: true,
            loopAdditionalSlides: 0,
            navigation: {
                nextEl: '[data-target="#' + sliderId + '"].swiper-button-next',
                prevEl: '[data-target="#' + sliderId + '"].swiper-button-prev',
            },
            pagination: {
                el: '#' + sliderId + ' .swiper-pagination',
                clickable: true,
            },
            grabCursor: true,
            breakpoints: {
                1024: {
                    slidesPerGroup: 3,
                    slidesPerView: 3,
                    loop: true,
                    loopFillGroupWithBlank: true,
                    loopAdditionalSlides: 0
                },
                640: {
                    slidesPerGroup: 2,
                    slidesPerView: 2,
                    loop: true,
                    loopFillGroupWithBlank: true,
                    loopAdditionalSlides: 0
                },
                530: {
                    slidesPerGroup: 1,
                    slidesPerView: 1.5,
                    slidesOffsetBefore: 15,
                    slidesOffsetAfter: 15,
                    loop: true,
                    loopFillGroupWithBlank: true,
                    loopAdditionalSlides: -1,
                }
            }
        });

        return mainSlider;
    }

    $('[data-main-slider]').each(function () {
        generateMainSlider($(this));
    });


    var performanceSliders = {};
    changePerformanceSlider = function (target) {
        var $rowWrapper = $(target).children('div').first();
        var RWcurrentClasses = $rowWrapper.attr('class');
        var RWnewClasses = $rowWrapper.attr('data-class');
        $rowWrapper.attr('class', RWnewClasses);
        $rowWrapper.attr('data-class', RWcurrentClasses);

        var $colSlides = $rowWrapper.children('div');
        $colSlides.each(function () {
            var $this = $(this);
            var currentClasses = $this.attr('class');
            var newClasses = $this.attr('data-class');

            $this.attr('class', newClasses);
            $this.attr('data-class', currentClasses);
        });
    };
    updatePerformanceSlider = function (sliderId, forceUpdate) {
        forceUpdate = forceUpdate || false;

        var width = $(window).width();
        var target = '#' + sliderId;
        var $rowWrapper = $(target).children('div').first();
        var $colSlides = $rowWrapper.children('div');

        if ($(target).length) {
            if ($colSlides.length <= performanceSlidesNumber(width)) {
                if ($rowWrapper.hasClass('swiper-wrapper')) {
                    changePerformanceSlider(target);
                    performanceSliders[target].destroy();
                }
            } else {
                if (!$rowWrapper.hasClass('swiper-wrapper')) {
                    changePerformanceSlider(target);
                    performanceSliders[target] = new Swiper(target, {
                        slidesPerGroup: 5,
                        slidesPerView: 5,
                        spaceBetween: 30,
                        navigation: {
                            nextEl: '[data-target="' + target + '"].swiper-button-next',
                            prevEl: '[data-target="+' + target + '"].swiper-button-prev',
                        },
                        pagination: {
                            el: target + ' .swiper-pagination',
                            clickable: true,
                        },
                        grabCursor: true,
                        breakpoints: {
                            1200: {
                                slidesPerGroup: 4,
                                slidesPerView: 4
                            },
                            992: {
                                slidesPerGroup: 3,
                                slidesPerView: 3
                            },
                            640: {
                                slidesPerGroup: 2,
                                slidesPerView: 2
                            },
                            530: {
                                slidesPerGroup: 1,
                                slidesPerView: 1.5,
                                slidesOffsetBefore: 15,
                                slidesOffsetAfter: 15
                            }
                        }
                    });
                }
                if (forceUpdate) {
                    performanceSliders[target].update();
                }
                performanceSliders[target].update();
            }
        }
    };

    function performanceSlidesNumber(width) {
        var number = 0;
        if (width) {
            var breakpoints = {
                10000: 5,
                1200: 4,
                992: 3,
                640: 2,
                530: 1.5,
            };
            $.each(breakpoints, function (index, value) {
                number = value;
                if (index > width) {
                    return false;
                }
            });
        }
        return number;
    }

    function generateDynamicSlider($swiperContainer) {
        var sliderId = $swiperContainer.attr('id');
        if (!sliderId) {
            return 0;
        }
        updatePerformanceSlider(sliderId);
        console.log('generate slider');
    }

    $('[data-dynamic-slider]').each(function () {
        generateDynamicSlider($(this));
    });


    var photosSlider = new Swiper('#photos-slider', {
        slidesPerGroup: 2,
        slidesPerView: 2,
        spaceBetween: 30,
        pagination: {
            el: '#photos-slider .swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        grabCursor: true,
        breakpoints: {
            992: {
                slidesPerGroup: 1,
                slidesPerView: 1,
            }
        }
    });

    var performancesSlider = new Swiper('#performances-slider', {
        slidesPerGroup: 4,
        slidesPerView: 4,
        spaceBetween: 30,
        pagination: {
            el: '#performances-slider .swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        grabCursor: true,
        breakpoints: {
            992: {
                slidesPerGroup: 3,
                slidesPerView: 3
            },
            640: {
                slidesPerGroup: 2,
                slidesPerView: 2
            },
            530: {
                slidesPerGroup: 1,
                slidesPerView: 1,
                slidesOffsetBefore: 15,
                slidesOffsetAfter: 15
            }
        }
    });

    var slidesPerView;
    var slidesPerGroup;

    if (screen.width <= 600 && screen.height <= 1000) {
        slidesPerView = 2;
        slidesPerGroup = 2;
    } else {
        slidesPerView = 4;
        slidesPerGroup = 4;
    }

    var photosSlider = new Swiper('#tags-slider', {
        slidesPerView: slidesPerView,
        spaceBetween: 30,
        slidesPerGroup: slidesPerGroup,
        loop: false,
        loopFillGroupWithBlank: true,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev'
        },
        grabCursor: true
    });

    var articleSlider = new Swiper('.article-slider', {
        spaceBetween: 30,
        loopFillGroupWithBlank: true,
        pagination: {
            el: '.article-slider .swiper-pagination',
            clickable: true
        },
        grabCursor: true
    });

    $(document).click(function (event) {
        if ($(event.target).closest(".hb-search-group").length == 0) {
            $(".hb-search-group").removeClass('open');
        }
        event.stopPropagation();
    });

    $("#event-price-toggle").on('click', function (e) {
        var $switcher = $(this);
        var freeClass = 'event-free';
        var blueClass = 'label-white-2 active';
        var greyClass = 'label-white-2';
        var $eventPriceGroup = $('#event-price-group');
        var $inputs = $eventPriceGroup.find('.form-control');
        if ($switcher.hasClass(blueClass)) {
            $eventPriceGroup.removeClass(freeClass);
            $switcher.removeClass(blueClass).addClass(greyClass);
            $inputs.eq(0).val('');
            $inputs.eq(1).val('');
        } else {
            $eventPriceGroup.addClass(freeClass);
            $switcher.removeClass(greyClass).addClass(blueClass);
            $inputs.eq(0).val('Бесплатно');
            $inputs.eq(1).val('Стоимость');
        }
    });
    $(".event-sale-toggle").on('click', function (e) {
        var $switcher = $(this);
        var freeClass = 'event-free';
        var blueClass = 'label-white-2 active';
        var greyClass = 'label-white-2';
        var $input = $('#event-sale').find('.form-control');
        var value = $input.val();
        var type = $switcher.text();
        var oldType = $switcher.siblings().text();
        var newValue = value.replace(oldType, type);
        if ($switcher.hasClass(blueClass)) {
            $switcher.removeClass(blueClass).addClass(greyClass);
            $switcher.siblings().removeClass(greyClass).addClass(blueClass);
        } else {
            $switcher.removeClass(greyClass).addClass(blueClass);
            $switcher.siblings().removeClass(blueClass).addClass(greyClass);
        }
        $input.val(newValue);
    });

    $('#toggle-event-filters').on('click', function (e) {
        e.preventDefault();
        var $switcher = $(this);
        var plusIcon = 'plus-icon';
        var minusIcon = 'minus-icon';
        var filtersClass = 'tfb-form-full';
        var $eventFilters = $('#event-filters');
        if ($eventFilters.hasClass(filtersClass)) {
            $eventFilters.removeClass(filtersClass);
            $switcher.find('.' + minusIcon).removeClass(minusIcon).addClass(plusIcon);
        } else {
            $eventFilters.addClass(filtersClass);
            $switcher.find('.' + plusIcon).removeClass(plusIcon).addClass(minusIcon);
        }
    });

    $('.wi-toggle').click(function (e) {
        e.preventDefault();
        $(this).parents('.wi-info-limit').toggleClass('wi-info-full');
    });

    $('.mhb-search-toggle').click(function (e) {
        e.preventDefault();

        var $block = $(this).parents('.mhb-search');

        if ($block.hasClass('mhb-search-open') === false) {
            setTimeout(function () {
                $('#mobile-search').focus();
            }, 100);
        }

        $block.toggleClass('mhb-search-open');
    });

    $('.extensible-options').selectpicker({
        noneResultsText: 'Добавить {0} <div class="add-new-place add-option btn btn-white btn-plus m-l-3 m-r-3"><i class="plus-icon"></i></div>'
    });

    $(document).on('click', '.extensible-options li.no-results', function () {
        var value = $(this).text().split('"')[1];
        var newPalce = $(this).closest(".row").children(".new-place");

        $(this).closest('.bootstrap-select').find('select')
            .append('<option value="' + value + '">' + value + '</option>')
            .val(value).selectpicker('refresh');

        $(this).closest('.bootstrap-select').find('li[data-original-index]').removeClass('hidden');

        newPalce.fadeIn();
    });

    $('#filter-date').on('hidden.bs.select', function (e) {
        var $this = $(this),
            value = $this.val(),
            $standardInputGroup = $this.closest('.standard-input-group');

        if (value == '') {
            $standardInputGroup.find('.sig-inner').toggleClass('hidden');
            $standardInputGroup.find('.sig-inner:not(.hidden) .form-control').focus();
        }
    });

    $('#sigInput3').easyAutocomplete({
        data: [
            'Дворец детей и молодёжи',
            'Дворец спорта',
            'Ледовый дворец'
        ],
        highlightPhrase: true,
        list: {
            //maxNumberOfElements: 3,
            sort: {
                enabled: true
            },
            match: {
                enabled: true
            },
            'onShowListEvent': function () {
            },
            'onHideListEvent': function () {
            }
        }
    });

    var states = [];

    $.ajax({
        type: 'post',
        url: '/load-tags',
        success: function (tags) {
            $.each(tags, function (id, value) {
                states.push({
                    "value": id,
                    "text": value
                });
            });

            var statesBH = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace('text'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                local: states
            });

            statesBH.initialize();

            $('#add-event-tags').tagsinput({
                itemValue: 'value',
                itemText: 'text',
                typeaheadjs: [
                    {
                        hint: true,
                        highlight: true,
                        minLength: 1
                    }, {
                        name: 'states',
                        displayKey: 'text',
                        source: statesBH.ttAdapter()
                    }
                ]
            });
        }
    });

    /*$('#esbRangePicker').on('apply.daterangepicker', function(ev, picker) {
        var daterange = picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY');
        console.log('daterange',daterange);
    });*/

    var $dropzoneSelectors = ['.esb-over-image', '.esb-dz-input-btn'];

    $.each($dropzoneSelectors, function (index, item) {
        if ($(item).length) {
            var eventsDropzone = new Dropzone(item, {
                url: '/upload-event-image',
                thumbnailWidth: null,
                thumbnailHeight: null,
                previewsContainer: ".esb-dz-previews-cnt",
                maxFilesize: 0.5,
                acceptedFiles: ".jpeg,.jpg,.png,.gif,.svg",
                dictFileTooBig: "Файл слишком большой ({{filesize}}Мб). Максимальный размер: {{maxFilesize}}Мб.",
                dictInvalidFileType: "Вы не можете загружать файлы данного типа."
            });

            eventsDropzone.on("complete", function (file) {
                if (file.xhr !== undefined && file.xhr.responseText !== undefined) {
                    $('#image').val(file.xhr.responseText);
                    $('.esb-dz-previews-cnt .dz-preview').not(file.previewElement).remove();

                    var $afisha = $('#afisha-image'),
                        $block = $('.esb-dz-previews-cnt');

                    $afisha.attr('src', '/img/afisha2.png');

                    if ($block.hasClass('border-red')) {
                        $block.removeClass('border-red');
                    }
                }
            });

            eventsDropzone.on("error", function(file, message) {
                alert(message);
                this.removeFile(file);

                var $afisha = $('#afisha-image'),
                    $block = $afisha.closest('.esb-dz-previews-cnt');

                $afisha.attr('src', '/img/afisha2.png');

                if ($block.hasClass('border-red')) {
                    $block.removeClass('border-red');
                }
            });
        }
    });

    $('[data-provide="slider"]').on('change', function (slideEvt) {
        var target = $(this).attr('data-target');
        var postfix = $(this).attr('data-postfix');
        var value = slideEvt.value.newValue;
        var $target = $(target);
        if (!value) {
            var min = +$target.attr('data-slider-min');
            var max = +$target.attr('data-slider-max');
            value = [min, max];
        }
        if (postfix) {
            if ($.isArray(value)) {
                if (! value[0] && ! value[1]) {
                    value = 'Бесплатно';
                } else {
                    value = value[0] + postfix + ' - ' + value[1] + postfix;
                }
            } else {
                value = value + postfix;
            }
        }

        if ($target.length) {
            if ($target.is('input')) {
                $target.val(value);
            } else {
                $target.text(value);
            }
        }
    });
    $('.range-billet').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        var $this = $(this);
        var range = $this.attr('data-range');
        var target = $this.attr('data-target');
        var $target = $(target);
        if ($target.length && range) {
            range = JSON.parse("[" + range + "]")[0];
            $target.slider('setValue', range, true, true);
        }
    });

    $('#prices').focus(function () {
        $(this).parent().addClass('open');
    }).focusout(function () {
        //$(this).parent().removeClass('open');
    }).change(function (e) {
        var $this = $(this);
        var range = $this.val();
        range = range.replace(/руб/g, '').replace(/\s/g, '').replace('-', ',');
        range = '[' + range + ']';
        var $target = $('#price-slider');
        var defaultValue = $target.attr('data-slider-value');
        if ($target.length && range) {
            try {
                range = JSON.parse("[" + range + "]")[0];
            } catch (e) {
                range = JSON.parse(defaultValue);
            }
            $target.slider('setValue', range, true, true);
        }
    });

    if ($('#prices').val() == '0 руб - 0 руб') {
        $('#prices').val('Бесплатно');
    }

    $(document).click(function (event) {
        if ($(event.target).closest(".tfb-dropdown-price, #prices").length == 0) {
            $(".tfb-dropdown-price").closest('.open').removeClass('open');
        }
        event.stopPropagation();
    });

    $('.open-thanks-modal').click(function (e) {
        //$('.modal.in').modal('hide');
        var $currentModal = $('.modal.in');
        $currentModal.removeClass('in');
        $currentModal.css('padding-right', '').hide().attr('aria-hidden', 'true');
        $currentModal.find('.modal-backdrop').remove();
        $('body').css('padding-right', 0);
        $('#thanksModal').modal('show');
    });

    $('#thanksModal').on('hidden.bs.modal', function (event) {
        $('#eventModal').modal('hide');
    });

    $('.standard-input-group .sig-bs-toggle').click(function (e) {
        var $this = $(this);
        var $standardInputGroup = $this.closest('.standard-input-group');
        $standardInputGroup.find('.selectpicker').selectpicker('toggle');
    });
    $(document).on('change', '.standard-input-group .std-file-input', function (e) {
        var $this = $(this);
        var $standardInputGroup = $this.closest('.standard-input-group');
        var $textInput = $standardInputGroup.find('input[type="text"]');
        if ($this.val() != '') {
            var filename = $this.val().split('\\').pop();
            $textInput.val(filename);
        } else {
            $textInput.val('');
        }
    });


    /*$(document).on('click', '.side-filters-block .sfb-toggle', function (e) {
        var $this = $(this);
        var group = $this.attr('data-group');
        var $sideFiltersBlock = $this.closest('.side-filters-block');
        var $sfbSection = $sideFiltersBlock.find('[data-group="'+group+'"].sfb-section');
        $sfbSection.find('').toggle();
    });*/
    $(document).on('change', '.sfb-section input[type="checkbox"].sfb-select-all', function (e) {
        var $this = $(this);
        var $sfbSection = $this.closest('.sfb-section');
        var $otherCheckboxes = $sfbSection.find('input[type="checkbox"]').not($this);
        if ($this.prop('checked')) {
            $otherCheckboxes.prop('checked', true);
            $otherCheckboxes.parent().addClass('checked');
        } else {
            $otherCheckboxes.prop('checked', false);
            $otherCheckboxes.parent().removeClass('checked');
        }
    });
    $(document).on('change', '.sfb-section input[type="checkbox"]:not(.sfb-select-all)', function (e) {
        var $this = $(this);
        var $sfbSection = $this.closest('.sfb-section');
        var $selectAll = $sfbSection.find('input[type="checkbox"].sfb-select-all');
        if (!$this.prop('checked')) {
            $selectAll.prop('checked', false);
            $selectAll.parent().removeClass('checked');
        }
    });

    var $scrollableElement = $('[canvas="container"]');

    if ($("[data-fancybox]").length) {
        $("[data-fancybox]").fancybox({
            arentEl: '[canvas="container"]',
            hideScrollbar: false,
        });
    }

    /*if(bWidth > 992){
        function stickyBannerScroll() {
            var $stickySideBanner = $('#sticky-side-banner');
            $stickySideBanner.width($stickySideBanner.width());
            var wrapperId = $stickySideBanner.attr('id') + '-wrapper';
            var wrapper = $('<div></div>').attr('id', wrapperId).css('height', $stickySideBanner.outerHeight());
            $stickySideBanner.wrapAll(function() {
                if ($(this).parent("#" + wrapperId).length == 0) {
                    return wrapper;
                }
            });

            var scrollTop = $scrollableElement.scrollTop();
            var bannerOffsetY = $stickySideBanner.parent()[0].offsetTop;

            if($stickySideBanner.parent().offset().top < 0 ) {
                $stickySideBanner.addClass('side-banner-fixed');
            }else if(scrollTop < bannerOffsetY) {
                $stickySideBanner.removeClass('side-banner-fixed');
            }
        }
        $scrollableElement.on("scroll", stickyBannerScroll);
    }*/

    function addIncrementBlock(incrementGroup, insertTarget) {
        var $incrementBlock = $('[data-increment-group="' + incrementGroup + '"]:last-child');
        var incrementBlockChildId = $('[data-increment-group="' + incrementGroup + '"]').length;
        if (!insertTarget) {
            insertTarget = $incrementBlock.attr('data-insert-target');
        }

        var $clone = $incrementBlock.clone().addClass('increment-block-child');

        if (insertTarget) {
            $clone.appendTo($(insertTarget));
        } else {
            $clone.insertAfter('[data-increment-group="' + incrementGroup + '"]:eq(-1)');
        }

        $incrementBlock.find('.addIncrementBlockButton .plus-icon').removeClass('plus-icon').addClass('minus-icon').end()
            .find('.addIncrementBlockButton .fa-plus-circle').removeClass('fa-plus-circle').addClass('fa-times-circle').end()
            .find('.addIncrementBlockButton').removeClass('addIncrementBlockButton').addClass('removeIncrementBlockButton').end();

        $clone.find('input[type="text"], input[type="hidden"], textarea, select').val('').end();

        //for selects with selectpicker
        var hasSelectPicker = $clone.has('.bootstrap-select');
        var $selectPickers = $();
        if (hasSelectPicker) {
            $clone.find('.bootstrap-select').each(function (index) {
                var $boostrapSelect = $(this);
                var $originalSelect = $boostrapSelect.find('select');
                $boostrapSelect.replaceWith($originalSelect);
                $selectPickers = $selectPickers.add($originalSelect);
            });
            $selectPickers.selectpicker();
        }

        //for datetimepicker
        var hasDatetimePicker = $clone.has('.datetimepicker');
        var $datetimePickers = $();
        if (hasDatetimePicker) {
            $datetimePickers = $clone.find('.datetimepicker');
            initDatetimepicker($datetimePickers);
        }

        //for time
        var hasClockPicker = $clone.has('.clockpicker');
        var $clockPickers = $();
        if (hasDatetimePicker) {
            $clockPickers = $clone.find('.clockpicker');
            initClockpicker($clockPickers);
        }

        //for daterangepicker
        var hasDaterangePicker = $clone.has('.datepicker-here');
        var $daterangepickers = $();
        if (hasDaterangePicker) {
            $daterangepickers = $clone.find('.datepicker-here');
            initDaterangepicker($daterangepickers);
        }

        //remove Dropzone previews
        $clone.find('.dz-image-preview').remove();

        $clone.find('input[type="text"],input[type="hidden"], textarea, select').not('[aria-label="Search"]').each(function (index) {
            var $this = $(this);

            var thisId = $this.attr('id');
            if (thisId) {
                var newId = thisId + '_' + (new Date()).getTime();
                $this.attr('id', newId);
                $clone.find('label[for="' + thisId + '"]').each(function () {
                    $(this).attr('for', newId);
                });
            }
        });
    }

    function removeIncrementBlock($incrementBlockChild) {
        var incrementGroup = $incrementBlockChild.attr('data-increment-group');
        $incrementBlockChild.remove();
    }

    $(document).on('click', '.increment-block .addIncrementBlockButton', function (e) {
        e.preventDefault();
        var incrementGroup = $(this).parents('.increment-block').attr('data-increment-group');
        addIncrementBlock(incrementGroup);
    });
    $(document).on('click', '.increment-block .removeIncrementBlockButton', function (e) {
        e.preventDefault();
        var $incrementBlockChild = $(this).parents('.increment-block');
        removeIncrementBlock($incrementBlockChild);
    });


    $(document).on('mouseenter', '.checkboxCustom, .radioCustom', function (e) {
        if (($(this).find('input').hasClass('disabled')) || ($(this).find('input').prop("disabled"))) {
            return 0;
        } else {
            $(this).addClass('hover');
        }
    });
    $(document).on('mouseleave', '.checkboxCustom, .radioCustom', function (e) {
        if (($(this).find('input').hasClass('disabled')) || ($(this).find('input').prop("disabled"))) {
            return 0;
        } else {
            $(this).removeClass('hover');
        }
    });
    $(document).on('change', 'input[type="checkbox"]', function () {
        var $checkbox = $(this);
        var $parent = $checkbox.parent('.checkboxCustom');
        if (($checkbox.find('input').hasClass('disabled')) || ($checkbox.find('input').prop("disabled"))) {
            return 0;
        }
        if ($checkbox.prop("checked")) {
            $parent.addClass('checked');
        } else {
            $parent.removeClass('checked');
        }
    });
    $(document).on('change', 'input[type="radio"]', function () {
        var $radio = $(this);
        var $parent = $radio.parent('.radioCustom');
        var name = $radio.attr('name');
        if (($radio.find('input').hasClass('disabled')) || ($radio.find('input').prop("disabled"))) {
            return 0;
        }
        if ($radio.prop("checked")) {
            //$('input[type="radio"][name="'+name+'"]').prop("checked", false);
            $('input[type="radio"][name="' + name + '"]').parent('.radioCustom').removeClass('checked');
            $parent.addClass('checked');
        } else {
            $parent.removeClass('checked');
        }
    });
    $(document).on('click', ".extensible-options", function () {
        var elemPluss = $(this).closest(".form-group").next();
        elemPluss.fadeIn();
    });
    $(document).on('click', '.data-from-group', function () {
        var elem = $(this).closest(".container-date").find(".add-date-template");
        elem.fadeIn();
    });
    $("#esbInput1-5").on("dp.change", function (e) {
        $('#esbInput1-6').data("DateTimePicker").minDate(e.date);
    });
    $("#esbInput1-6").on("dp.change", function (e) {
        $('#esbInput1-5').data("DateTimePicker").maxDate(e.date);
    });

    $('body').on('click', '.sig-feedback', function () {
        var $parent = $(this).closest('.standard-input-group'),
            $dateTimePicker = $parent.find('.datetimepicker');

        $dateTimePicker.focus();
    });

    /*update id for input elements*/
    function updateInputID(elem) {
        //for selects with selectpicker
        var hasSelectPicker = elem.has('.bootstrap-select');
        var $selectPickers = $();
        if (hasSelectPicker) {
            elem.find('.bootstrap-select').each(function (index) {
                var $boostrapSelect = $(this);
                var $originalSelect = $boostrapSelect.find('select');
                $boostrapSelect.replaceWith($originalSelect);
                $selectPickers = $selectPickers.add($originalSelect);
            });
            $selectPickers.selectpicker();
        }

        //for datetimepicker
        var hasDatetimePicker = elem.has('.datetimepicker');
        var $datetimePickers = $();
        if (hasDatetimePicker) {
            $datetimePickers = elem.find('.datetimepicker');
            initDatetimepicker($datetimePickers);
        }

        //for time
        var hasClockPicker = elem.has('.clockpicker');
        var $clockPickers = $();
        if (hasDatetimePicker) {
            $clockPickers = elem.find('.clockpicker');
            initClockpicker($clockPickers);
        }

        //for daterangepicker
        var hasDaterangePicker = elem.has('.datepicker-here');
        var $daterangepickers = $();
        if (hasDaterangePicker) {
            $daterangepickers = elem.find('.datepicker-here');
            initDaterangepicker($daterangepickers);
        }

        //remove Dropzone previews
        elem.find('.dz-image-preview').remove();

        elem.find('input[type="text"],input[type="hidden"], textarea, select').not('[aria-label="Search"]').each(function (index) {
            var $this = $(this);

            var thisId = $this.attr('id');
            if (thisId) {
                var newId = thisId + '_' + (new Date()).getTime();
                $this.attr('id', newId);
                elem.find('label[for="' + thisId + '"]').each(function () {
                    $(this).attr('for', newId);
                });
            }
        });

        return elem;
    }

    function addTemplateInstitution() {
        $('#tempalteInstitution').find('select').selectpicker('destroy');

        /*find template*/
        var template = $("#tempalteInstitution").clone();
        var date = $("#tempalteDate").clone();

        /*remove id*/
        template = template.removeAttr('id');
        date = date.removeAttr('id');
        date.find('.clockpicker').val('').trigger('change');

        /*update id*/
        date = updateInputID(date);

        var places = template.find('[data-id="place"]').html();

        template.find('[data-id="place"]').html(places);
        template.find('select').selectpicker('refresh');

        template.find('.extensible-options').selectpicker({
            noneResultsText: 'Добавить {0} <div class="add-new-place add-option btn btn-white btn-plus m-l-3 m-r-3"><i class="plus-icon"></i></div>'
        });

        /*add dateTemplate in the template*/
        template.find(".container-date").append(date);

        return template;
    }

    /*click for add or remove template in pop-up*/
    $(document).on('click', ".add-institution", function () {
        $(".container-institutions").append(addTemplateInstitution());
    });
    $(document).on('click', ".template-institution_remove", function () {
        $(this).closest('.template-institution').remove();
    }).on('click', ".template-increment_remove", function () {
        $(this).closest('.template-institution').remove();
    });
    $(document).on('click', ".add-date-template", function () {
        var date = $(this).closest('.increment-block').clone();

        date.prepend('<a class="ed-btn-remove btn btn-white m-t-0 m-b-30 template-increment_remove cursor-pointer">\n' +
            '+\n' +
        '</a>');

        date = date.removeAttr('id');
        date.find('.clockpicker').val('').trigger('change');

        date
            .find('.add-date-template .plus-icon').removeClass('plus-icon').addClass('minus-icon').end()
            .find('.add-date-template .fa-plus-circle').removeClass('fa-plus-circle').addClass('fa-times-circle').end()
            .find('.add-date-template').removeClass('add-date-template').removeClass('display-none').addClass('remove-date-template').end();

        var $block = $(this).closest('.template-institution');

        if (! $block.length) {
            $block = $(this).closest('.esb-col-inputs');
        }

        var place = $block.find('[data-id="place"]').val();

        date
            .append('<select name="place[]" class="hidden">\n' +
                '<option value="' + place + '" selected></option>\n' +
            '</select>')
            .append('<input type="hidden" name="address[]" value="">')
            .append('<select name="place_category[]" class="hidden">\n' +
                '<option value="" selected></option>\n' +
            '</select>');

        date = updateInputID(date);

        $(this).closest(".container-date").append(date);
    }).on('change', '[data-id="place"]', function () {
        var $block = $(this).closest('.template-institution'),
            parentBlock = 0;

        if (! $block.length) {
            parentBlock = 1;
            $block = $(this).closest('.esb-col-inputs');
        }

        var place = $block.find('[data-id="place"]').val();

        if (parentBlock) {
            $block = $block.find('.container-date');
        }

        $block.find('.increment-block').find('select').find('option').attr('value', place);
    });
    $(document).on('click', ".remove-date-template", function () {
        $(this).closest('.increment-block').remove();
    });

    /*Slider*/
    /*open*/
    $(document).on('click', "#photo-slide__close", function () {
        $(this).closest('#photo-slide').fadeOut();
    });
});

// $(document).ready(function() {
//     setTimeout(() => { $(".swiper-wrapper").children(".swiper-slide.swiper-slide-duplicate").remove() }, 50);
// });
