$(document).ready(function() {
    $.ajaxSetup({
        type: 'post',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).on('focus', ':input', function () {
        $(this).attr('autocomplete', 'off');
    });

    /* Маска для ввода телефонного номера. */
    $('.phone-mask').each(function() {
        var placeholder = '+375 (__) ___-__-__',
            mask = placeholder.replace(/_/g , '0');

        if (placeholder !== '') {
            $(this).mask(mask);
        }
    });

    $('#dates').on('keyup', function (e) {
        if (! $(e.target).val()) {
            $('#dates').val('').trigger('change');
            var instance = $('#dates').data('daterangepicker');
            var container = instance.container;
            container.remove();
            $('#dates').val('').trigger('change');

            $('[name="dates"]').daterangepicker({
                separator: '-',
                autoClose: true,
                format: 'DD.MM.YYYY',
                alwaysShowCalendars: true,
                ranges: {
                    'Сегодня': [moment(), moment()],
                    'Завтра': [moment().add(1, 'days'), moment().add(1, 'days')],
                    'Неделя': [moment().startOf('week'), moment().endOf('week')],
                    'В этом месяце': [moment().startOf('month'), moment().endOf('month')]
                },
                locale: {
                    'customRangeLabel': 'Выбрать дату',
                    "applyLabel": "Подтвердить",
                    "cancelLabel": "Очистить",
                    "fromLabel": "От",
                    "toLabel": "До"
                }
            }).val('').trigger('change');
        }
    }).on('cancel.daterangepicker', function(ev, picker) {
        $('#dates').val('');
    }).on('change', function(ev, picker) {
        var val = $('#dates').val();
        val = val.replace(/\s+/g, '');

        $('#dates').val(val);
    });

    sendAjaxForm('#subscribe');
    sendAjaxForm('#footer-subscribe');

    $('.dialog-close').on('click', function () {
        var id = $(this).closest('.modal').attr('id');

        $('#' + id).modal('hide').attr('style', 'display: none;');
    });

    $('[data-selected-city]').on('click', function () {
        var city = $(this).attr('data-selected-city');

        $.ajax({
            type: 'post',
            url: '/update-city',
            data: {city: city},
            success: function () {
                window.location.reload();
            }
        });
    });

    var keyupTimer = null;

    $(".header-search").on('keyup', function (e) {
        var input = $(this),
            search = $(this).val(),
            $searchGroup = $(this).closest('.header-search-group');

        clearTimeout(keyupTimer);

        keyupTimer = setTimeout(function () {
            if (input.val() == '' || input.val() == null) {
                $searchGroup.removeClass('open');
                $('.search-result').addClass('hidden');
            } else {
                $('.search-result').removeClass('hidden');
            }

            if (e.keyCode == 8 || e.keyCode > 45 || e.keyCode == 0) {
                $.ajax({
                    type: 'post',
                    url: '/search',
                    data: {
                        search: search
                    },
                    success: function (result) {
                        var $block = $('.search-result');

                        $block.html(result);

                        $('.output-search-results').mCustomScrollbar({
                            axis: 'y',
                            theme: 'default',
                            scrollButtons: {
                                enable: false
                            },
                            scrollInertia: 60,
                            mouseWheelPixels: 50
                        });

                        $.each($block.find('.sr-category'), function () {
                            var newText = $(this).text().replace(new RegExp(search, "i"), function(matched) {
                                return '<strong>' + matched + '</strong>';
                            });

                            $(this).html(newText);
                        });

                        if (input.val() != '' && input.val() != null) {
                            $searchGroup.addClass('open');
                        }
                    }
                });
            }
        }, 500);
    }).on('click', function () {
        var $searchGroup = $(this).closest('.hb-search-group');

        if ($(this).val() != '') {
            $searchGroup.addClass('open');
        }
    });

    /*$(".owl-carousel").owlCarousel({
        items: 4,
        nav: true,
        rows: true,
        rowsCount: 2
    });*/

    $('[data-id="city_id"]').on('change', function () {
        var city = $(this).val();

        $.ajax({
            type: 'post',
            url: '/upload-places',
            data: {
                city: city
            },
            success: function (places) {
                $('select.data-id-place').html(places).selectpicker("refresh");

                $('body').find('select.data-id-place').closest('.bootstrap-select')
                    .find('.filter-option').text('Название заведения');
            }
        });
    });

    $('body').on('click', '#add-event-cancel', function () {
        var $select = $(this).closest('.bootstrap-select').find('select');

        $select.val(0).selectpicker('refresh');

        $('.add-institution').removeClass('display-none');
    }).on('changed.bs.select', 'select.data-id-place', function (e) {
        var place = $(this).val(),
            $block = $(this).closest('.bootstrap-select');

        if (place == 'add-place') {
            $block.find('select').selectpicker("refresh");

            $block.find('li[data-original-index]').addClass('hidden');
            $block.find('.dropdown-menu.inner').append('<li id="add-event-cancel"><a href=""><span>Отменить</span></a></li>');

            setTimeout(function () {
                $(e.target).closest('.bootstrap-select').addClass('open').find('input').focus();

                $block.find('div.dropdown-menu').removeAttr('style');
                $block.find('ul.dropdown-menu').removeAttr('style');
            }, 0);

            $(this).closest('.bootstrap-select').find('.filter-option').text('Введите название');
        }
    }).on('hidden.bs.select', 'select.data-id-place', function (e) {
        var place = $(this).val(),
            $block = $(this).closest('.bootstrap-select');

        if ((place == 'add-place' || place == 'cancel') && $block.find('input').val() != null && $block.find('input').val() != '') {
            $(this).val(0).selectpicker('refresh');
        }
    });

    $('a[href="#tab-step-1"], a[href="#tab-step-2"]').on('click', function () {
        $("#eventModal").animate({
            scrollTop: 0
        }, "slow");
    });

    $('#tab-step-2-link').on('click', function () {
        var $errors = [],
            image = $('#image').val(),
            name = $('[data-id="name"]').val(),
            type = $('[data-id="type_id"]').val(),
            city = $('[data-id="city_id"]').val(),
            dateFrom = $('[data-id="date_from"]').val();

        $('#afisha-image').attr('src', '/img/afisha.png');
        $('.esb-dz-previews-cnt').removeClass('border-red');
        $('[data-id]').closest('.standard-input-group').removeClass('border-red');

        if (image == '') {
            $errors.push('image');
            $('#afisha-image').attr('src', '/img/afisha2.png');
        }

        if (name == '') {
            $errors.push('name');
        }

        if (type === undefined || type == '' || type == null) {
            $errors.push('type_id');
        }

        if (city === undefined || city == '' || city == null) {
            $errors.push('city_id');
        }

        if (dateFrom === undefined || dateFrom == '') {
            $errors.push('date_from');
        }

        if ($errors.length) {
            notice('error', 'Проверьте правильность заполнения полей.');

            $.each($errors, function (i, field) {
                if (field == 'image') {
                    $('.esb-dz-previews-cnt').addClass('border-red');
                } else {
                    $('[data-id="' + field + '"]').closest('.standard-input-group').addClass('border-red');
                }
            });
        } else {
            $('#tab-step-1').toggleClass('active').toggleClass('fade').toggleClass('in');
            $('#tab-step-2').toggleClass('active').toggleClass('in');

            $("#eventModal").animate({ scrollTop: 0 }, "slow");
        }
    });

    $('#tab-step-3-link').on('click', function () {
        var $price = $('[data-id="price_from"]');

        if ($price.val() == '' || $price.val() === undefined) {
            $('#event-price-group').addClass('border-red').addClass('border-radius-4');
        } else {
            $('#event-price-group').removeClass('border-red').removeClass('border-radius-4');

            $('#tab-step-2').toggleClass('active').toggleClass('fade').toggleClass('in');
            $('#tab-step-3').toggleClass('active').toggleClass('in');

            $("#eventModal").animate({ scrollTop: 0 }, "slow");
        }
    });

    var $datetangepicker = $('[name="dates"]').daterangepicker({
        separator: '-',
        autoClose: true,
        format: 'DD.MM.YYYY',
        setDate: $('#dates').val(),
        alwaysShowCalendars: true,
        ranges: {
            'Сегодня': [moment(), moment()],
            'Завтра': [moment().add(1, 'days'), moment().add(1, 'days')],
            'Неделя': [moment().startOf('week'), moment().endOf('week')],
            'В этом месяце': [moment().startOf('month'), moment().endOf('month')]
        },
        locale: {
            'customRangeLabel': 'Выбрать дату',
            "applyLabel": "Подтвердить",
            "cancelLabel": "Очистить",
            "fromLabel": "От",
            "toLabel": "До"
        }
    });

    if ($('#dates').attr('value') == '') {
        $datetangepicker.val('');
    }

    var $form = $('#add-event'),
        $button = $form.find('button');

    $('.type-discount').on('click', '.event-sale-toggle', function () {
        $('#type-discount').val($('.type-discount').find('.label-white-2.active').text());
    });

    $form.ajaxForm({
        beforeSubmit: function() {
            $button.attr('disabled', true);

            $('.esb-dz-previews-cnt').removeClass('border-red');
            $('[data-id]').closest('.standard-input-group').removeClass('border-red');
        },
        success: function (result) {
            if (result.success) {
                $('#thanksModal').modal('show');

                setTimeout(function () {
                    window.location.reload(true);
                }, 3000);
            } else if (result.error) {
                $button.attr('disabled', false);

                notice('error', 'Проверьте правильность заполнения полей.');

                $.each(result.error, function (field, text) {
                    if (field == 'image') {
                        $('.esb-dz-previews-cnt').addClass('border-red');
                    } else {
                        $('[data-id="' + field + '"]').closest('.standard-input-group').addClass('border-red');
                    }
                });
            }
        },
        error: function () {
            $button.attr('disabled', false);
        }
    });

    /* Валидация 2 шага получения скидки. */
    $('#discount-step-2').ajaxForm({
        beforeSubmit: function() {
            $('#discount-step-2').find('button').attr('disabled', true);
        },
        success: function (result) {
            $('#discount-step-2').find('button').attr('disabled', false);

            if (result.error) {
                notice('error', result.error);
            } else {
                var discounts = result.discounts;

                $('#discount-email').text(result.email);
                $('#discount-username').text(result.username);

                if (discounts > 0) {
                    $('#count-discount').text(discounts);
                } else {
                    $('.special-offer').remove();
                }

                $('#sale1Modal').modal('hide');
                $('#sale2Modal').modal('hide');
                $('#sale3Modal').modal('show');
            }
        }
    });

    $(document).on('focusin', function(e) {
        if ($(e.target).closest(".mce-window").length) {
            e.stopImmediatePropagation();
        }
    });
});

/* Ajax-обработка форм. */
function sendAjaxForm(id) {
    var $form = $(id),
        $button = $form.find('button');

    $form.ajaxForm({
        beforeSubmit: function() {
            $button.attr('disabled', true);

            $('.esb-dz-previews-cnt').removeClass('border-red');
            $('[data-id]').closest('.standard-input-group').removeClass('border-red');
        },
        success: function (result) {
            $button.attr('disabled', false);

            if (id != '#add-event') {
                resultNotice(result);

                if (result.success) {
                    $form.find('input, textarea').val('');
                }
            } else {
                if (result.success) {
                    $('#thanksModal').modal('show');

                    setTimeout(function () {
                        window.location.reload(true);
                    }, 3000);
                } else if (result.error) {
                    $('#subscribeModal').modal('show');
                    $('#subscribe-modal-title').text('Заполните все обязательные поля.');

                    $.each(result.error, function (field, text) {
                        if (field == 'image') {
                            $('.esb-dz-previews-cnt').addClass('border-red');
                        } else {
                            $('[data-id="' + field + '"]').closest('.standard-input-group').addClass('border-red');
                        }
                    });
                }
            }
        },
        error: function () {
            $button.attr('disabled', false);
        }
    });
}

/* Уведомления. */
function notice(type, message, width, timeout, layout) {
    width = width || 200;
    timeout = timeout || 3000;
    layout = layout || 'topRight';

    noty({
        type: type,
        width: width,
        text: message,
        layout: layout,
        timeout: timeout,
        dismissQueue: true
    });
}

/* Уведомление с получением типа. */
function resultNotice(result) {
    var message = result.error ? result.error : result.success;

    $('#subscribeModal').modal('show');
    $('#subscribe-modal-title').text(message);
}