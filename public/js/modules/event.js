$(document).ready(function () {
    var event = $('.bg-block-13').attr('data-event-id');

    $('body').on('click', '[data-event-action]', function () {
        var action = $(this).attr('data-event-action');
        var reverseAction = action == 'like' ? 'dislike' : 'like';

        $('[data-event-action="' + reverseAction +'"] i').removeClass('active');
        $(this).find('i').toggleClass('active');

        $.ajax({
            url: '/events/like',
            data: {
                event: event,
                action: action
            },
            success: function (count) {
                $('.like').find('.ei-number').text(count.likes);
                $('.dislike').find('.ei-number').text(count.dislikes);
            }
        });
    })
    /* Подгрузка дополнительных мероприятий. */
    .on('click', '#show-more', function () {
        $(this).attr('disable', true);

        events.pagination();
    }).on('click', 'label.a-label, .checkboxCustom', function (e) {
        e.preventDefault();

        var $checkbox = $(this).find('input[type="checkbox"]');

        if ($(this).hasClass('checkboxCustom')) {
            $(this).toggleClass('checked');
        } else {
            $(this).find('.checkboxCustom').toggleClass('checked');
        }

        $checkbox.attr('checked', !$checkbox.attr('checked'));

        events.page = 1;
        filteredEvents(true);
    });

    $('[data-tag]').on('click', function () {
        $(this).toggleClass('active');

        events.page = 1;
        filteredEvents(true);
    });

    $('#date').on('dp.change', function (e) {
        events.page = 1;
        filteredEvents(true);
    });

    $('#filter-clear').on('click', function () {
        $('#date').val('');
        $('#search').val('');

        var $types = $('.list-types');

        $types.find('.checkboxCustom.checked').removeClass('checked');
        $types.find('.checkboxCustom.checked input').attr('checked', false);

        var $places = $('.list-places');

        $places.find('.checkboxCustom.checked').removeClass('checked');
        $places.find('.checkboxCustom.checked input').attr('checked', false);

        $('div#tags a.active').removeClass('active');

        filteredEvents(true);

        var newUrl = document.location.href.split('events')[0] + 'events';

        /* Добавляем параметр выбранного заведения в адресную строку. */
        window.history.replaceState(null, null, newUrl);
    });

    var $eventImage = $('.psb-image-cnt');

    if ($eventImage.length) {
        $eventImage.lightGallery({
            pager: true,
            thumbnail: true,
            animateThumb: false,
            selector: '.image-viewer',
            showThumbByDefault: false
        });
    }
});

/* Заведения. */
var events = {
    page: 1,
    pagination: function () {
        filteredEvents();
    }
};

function filteredEvents(replace) {
    var $tags = [],
        $types = [],
        $places = [],
        $block = $('#events'),
        date = $('#date').val(),
        $promo = $block.find('.visible-sm'),
        $button = $('body').find('#show-more');

    $.each($('.list-types').find('.checkboxCustom.checked input'), function () {
        $types.push($(this).val());
    });

    $.each($('.list-places').find('.checkboxCustom.checked input'), function () {
        $places.push($(this).val());
    });

    $.each($('div#tags-slider a.active'), function () {
        $tags.push($(this).attr('data-tag'));
    });

    $('#type').selectpicker('val', $types);
    $('#place').selectpicker('val', $places);

    $('.selectpicker').selectpicker('refresh');

    var newUrl = document.location.href.split('events')[0] + 'events';

    $('[name="dates"]').daterangepicker({
        separator: '-',
        autoClose: true,
        format: 'DD.MM.YYYY',
        alwaysShowCalendars: true,
        ranges: {
            'Сегодня': [moment(), moment()],
            'Завтра': [moment().add(1, 'days'), moment().add(1, 'days')],
            'Неделя': [moment().startOf('week'), moment().endOf('week')],
            'В этом месяце': [moment().startOf('month'), moment().endOf('month')]
        },
        locale: {
            'customRangeLabel': 'Выбрать дату',
            "applyLabel": "Подтвердить",
            "cancelLabel": "Очистить",
            "fromLabel": "От",
            "toLabel": "До"
        }
    }).val('').trigger('change');

    $('#filter-date').val('0');
    $('#prices').val('0 руб - 2000 руб');
    $('#price-slider').attr('data-slider-value', '[0,2000]');

    if ($types.length) {
        newUrl = newUrl + '?types[]=' + $types.join('&types[]=');
    }

    if ($places.length) {
        var point = $types.length ? '&' : '?';
        newUrl = newUrl + point + 'places[]=' + $places.join('&places[]=');
    }

    if ($tags.length) {
        var point2 = $types.length || $places.length ? '&' : '?';
        newUrl = newUrl + point2 + 'tags[]=' + $tags.join('&tags[]=');
    }

    if (date.length) {
        var point3 = $types.length || $places.length || $tags.length ? '&' : '?';
        newUrl = newUrl + point3 + 'date=' + date;
    }

    /* Добавляем параметр выбранного заведения в адресную строку. */
    window.history.replaceState(null, null, newUrl);

    $.ajax({
        url: '/events/more',
        dataType: 'json',
        data: {
            date: date,
            tags: $tags,
            types: $types,
            places: $places,
            page: replace === undefined ? events.page : 0
        },
        success: function (data) {
            var content = data.content;

            $block.find('.hidden-sm').removeClass('hidden-sm');

            if (replace !== undefined) {
                $('#events').find('.event-block').remove();
            }

            if ($promo.length) {
                $promo.before(content);
            } else {
                $block.append(content);
            }

            if (data.content) {
                $('#count-events').text('Мероприятия');

                if (! replace) {
                    events.page++;
                }
            } else {
                $('#count-events').text('Мероприятий не найдено');
            }

            if (! data.more) {
                $button.closest('div').addClass('hidden');
            } else {
                $button.closest('div').removeClass('hidden');
            }

            if (data.description) {
                $('#description').html(data.description);
            }

            $button.attr('disable', false);
        },
        error: function () {
            $button.closest('div').addClass('hidden');
        }
    });
}