$(document).ready(function () {
    var review = $('.bg-block-13').attr('data-review-id');

    $('body').on('click', '[data-review-action]', function () {
        var action = $(this).attr('data-review-action');
        var reverseAction = action == 'like' ? 'dislike' : 'like';

        $('body').find('[data-review-action="' + reverseAction + '"] i').removeClass('active');
        $(this).find('i').toggleClass('active');

        $.ajax({
            url: '/reviews/like',
            data: {
                review: review,
                action: action
            },
            success: function (count) {
                $('.like').find('.ei-number').text(count.likes);
                $('.dislike').find('.ei-number').text(count.dislikes);
            }
        });
    })
    /* Подгрузка дополнительных мероприятий. */
    .on('click', '#show-more', function () {
        $(this).attr('disable', true);

        reviews.pagination();
    });

    $('label.a-label, .checkboxCustom').on('click', function (e) {
        e.preventDefault();

        var $checkbox = $(this).find('input[type="checkbox"]');

        if ($(this).hasClass('checkboxCustom')) {
            $(this).toggleClass('checked');
        } else {
            $(this).find('.checkboxCustom').toggleClass('checked');
        }

        $checkbox.attr('checked', !$checkbox.attr('checked'));

        reviews.page = 1;
        filteredReviews(true);
    });

    $('[data-type]').on('click', function () {
        $(this).toggleClass('active');

        reviews.page = 1;
        filteredReviews(true);
    });

    $('#date').on('dp.change', function (e) {
        reviews.page = 1;
        filteredReviews(true);
    });

    $('#filter-clear').on('click', function () {
        $('#date').val('');
        $('#search').val('');

        var $types = $('.list-types');

        $types.find('.checkboxCustom.checked').removeClass('checked');
        $types.find('.checkboxCustom.checked input').attr('checked', false);

        var $places = $('.list-places');

        $places.find('.checkboxCustom.checked').removeClass('checked');
        $places.find('.checkboxCustom.checked input').attr('checked', false);

        $('div#reviews-types a.active').removeClass('active');

        filteredReviews(true);
    });
});

/* Обзоры. */
var reviews = {
    page: 1,
    pagination: function () {
        filteredReviews();
    }
};

function filteredReviews(replace) {
    var $types = [],
        $places = [],
        $reviewsTypes = [],
        $block = $('#reviews'),
        date = $('#date').val(),
        search = $('#search').val(),
        $button = $('body').find('#show-more');

    $.each($('.list-types').find('.checkboxCustom.checked input'), function () {
        $types.push($(this).val());
    });

    $.each($('.list-places').find('.checkboxCustom.checked input'), function () {
        $places.push($(this).val());
    });

    $.each($('div#reviews-types a.active'), function () {
        $reviewsTypes.push($(this).attr('data-type'));
    });

    var newUrl = document.location.href.split('reviews')[0] + 'reviews';

    if ($types.length) {
        newUrl = newUrl + '?types[]=' + $types.join('&types[]=');
    }

    if ($places.length) {
        var point = $types.length ? '&' : '?';
        newUrl = newUrl + point + 'places[]=' + $places.join('&places[]=');
    }

    if ($reviewsTypes.length) {
        var point2 = $types.length || $places.length ? '&' : '?';
        newUrl = newUrl + point2 + 'reviewsTypes[]=' + $reviewsTypes.join('&reviewsTypes[]=');
    }

    if (date.length) {
        var point3 = $types.length || $places.length || $reviewsTypes.length ? '&' : '?';
        newUrl = newUrl + point3 + 'date=' + date;
    }

    /* Добавляем параметр выбранного заведения в адресную строку. */
    window.history.replaceState(null, null, newUrl);

    $.ajax({
        url: '/reviews/more',
        dataType: 'json',
        data: {
            date: date,
            types: $types,
            search: search,
            places: $places,
            reviewsTypes: $reviewsTypes,
            page: replace === undefined ? reviews.page : 0
        },
        success: function (data) {
            var content = data.content;

            if (replace !== undefined) {
                $('#reviews').find('.preview-block').remove();
            }

            $block.append(content);

            if (content) {
                $('#count-reviews').text('Свежие публикации');

                if (! replace) {
                    reviews.page++;
                }
            } else {
                $('#count-reviews').text('Публикаций не найдено');
            }

            if (! data.more) {
                $button.closest('div').addClass('hidden');
            } else {
                $button.closest('div').removeClass('hidden');
            }

            if (data.description) {
                $('#description').html(data.description);
            }

            $button.attr('disable', false);
        },
        error: function () {
            $button.closest('div').addClass('hidden');
        }
    });
}