<?php return [
    'packageTitle' => '',
    'menu' => [
        'packageSubTitle' => '',
        'packageItems' => [
            'my-account' => '',
            'exit' => '',
            'menu' => '',
            'users' => [
                'packageSubTitle' => '',
                'packageItems' => [
                    'users' => '',
                    'roles' => '',
                    'permissions' => '',
                ],
            ],
            'pages' => '',
            'system-settings' => [
                'packageSubTitle' => '',
                'packageItems' => [
                    'cache-clear' => '',
                    'localization' => '',
                ],
            ],
        ],
    ],
    'grid' => [
        'packageSubTitle' => '',
        'packageItems' => [
            'on' => '',
            'off' => '',
            'create' => '',
            'search' => '',
            'show' => '',
            'no-data' => '',
            'clear-filter' => '',
            'loading' => '',
            'no-selected' => '',
            'no-empty' => '',
            'no-isset' => '',
            'success' => '',
            'delete-selected' => '',
            'first' => '',
            'last' => '',
            'yes-delete' => '',
            'no-cancel' => '',
            'delete-success' => '',
            'delete-are-you-sure' => '',
        ],
    ],
    'form' => [
        'packageSubTitle' => '',
        'packageItems' => [
            'tab' => '',
            'creating' => '',
            'editing' => '',
            'select-item' => '',
            'no-founded' => '',
            'select-all' => '',
            'all-selected' => '',
            'selected' => '',
            'no-selected' => '',
            'alias' => '',
            'save' => '',
            'save-and-cancel' => '',
            'cancel' => '',
            'add' => '',
            'change' => '',
            'delete' => '',
            'input-text' => '',
            'input-1-symbol' => '',
            'input-too-long' => '',
            'input-error-loading' => '',
            'input-loading-more' => '',
            'input-no-results' => '',
            'input-searching' => '',
            'input-maximum-selected' => '',
            'no-file' => '',
            'success' => '',
            'field-require' => '',
            'field-unique' => '',
            'field-validate' => '',
            'no-isset' => '',
            'destroyed' => '',
            'no-permissions' => '',
            'length-password' => '',
            'no-confirmed-passwords' => '',
            'isset-value' => '',
            'back' => '',
            'changes' => '',
            'and-close' => '',
            'delete-are-you-sure' => '',
            'are-you-sure' => '',
            'date-and-time' => [
                'packageSubTitle' => '',
                'packageItems' => [
                    'Jan' => '',
                    'Feb' => '',
                    'Mar' => '',
                    'Apr' => '',
                    'May' => '',
                    'Jun' => '',
                    'Jul' => '',
                    'Aug' => '',
                    'Sep' => '',
                    'Oct' => '',
                    'Nov' => '',
                    'Dec' => '',
                    'January' => '',
                    'February' => '',
                    'March' => '',
                    'April' => '',
                    'June' => '',
                    'July' => '',
                    'August' => '',
                    'September' => '',
                    'October' => '',
                    'November' => '',
                    'December' => '',
                    'Sun' => '',
                    'Mon' => '',
                    'Tue' => '',
                    'Wed' => '',
                    'Thu' => '',
                    'Fri' => '',
                    'Sat' => '',
                    'close' => '',
                    'today' => '',
                    'next-month' => '',
                    'prev-month' => '',
                    'select-month' => '',
                    'select-year' => '',
                    'select-time' => '',
                    'select-date' => '',
                    'select-date-and-time' => '',
                    'month' => '',
                    'minutes' => '',
                    'seconds' => '',
                    'hour' => '',
                    'year' => '',
                    'day' => '',
                ],
            ],
            'color' => [
                'packageSubTitle' => '',
                'packageItems' => [
                    'cancel' => '',
                    'choose' => '',
                    'clear' => '',
                    'no-selected' => '',
                    'more' => '',
                    'hide' => '',
                ],
            ],
        ],
    ],
    'modules' => [
        'packageSubTitle' => '',
        'packageItems' => [
            'main-page' => [
                'packageSubTitle' => '',
                'packageItems' => [
                    'free-space' => ''
                ],
            ],
            'cache-clear' => [
                'packageSubTitle' => '',
                'packageItems' => [
                    'mb' => '',
                    'clear' => '',
                    'cache-site' => '',
                    'trash' => '',
                    'cache-system' => '',
                    'useless-images' => '',
                ],
            ],
            'localization' => [
                'packageSubTitle' => '',
                'packageItems' => [
                    'add' => '',
                    'close' => '',
                    'add-language' => '',
                    'untranslated' => '',
                    'select-language' => '',
                    'available-languages' => '',
                    'tooltip-remove-language' => '',
                    'isset-language' => '',
                    'tooltip-active-language' => '',
                    'tooltip-default-language' => '',
                    'russian-no-delete' => '',
                    'no-isset-language' => '',
                    'add-language-success' => '',
                    'remove-language-success' => '',
                    'success-save-translate' => '',
                    'toggle-activate' => '',
                    'enter-a-translation' => '',
                    'toggle-deactivate' => '',
                    'error-save-translate' => '',
                    'toggle-default' => '',
                    'delete-are-you-sure' => '',
                    'no-active' => '',
                    'set-default' => '',
                    'only-active' => '',
                ],
            ],
        ],
    ],
    'authorization' => [
        'packageSubTitle' => '',
        'packageItems' => [
            'title' => '',
            'remember-me' => '',
            'enter' => '',
        ],
    ],
    'system' => [
        'packageSubTitle' => '',
        'packageItems' => [
            'model_not_exist' => '',
            'specify_group' => '',
        ],
    ],
];
