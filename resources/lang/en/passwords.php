<?php return [
    'packageTitle' => 'Passwords',
    'password' => 'The password must contain at least six characters and match the confirmed one.',
    'reset' => 'Your password is successfully reset.',
    'sent' => 'We have sent a password recovery link to Your email.',
    'token' => 'The password recovery token is invalid.',
    'user' => 'We cannot find a user with this email address.',
];