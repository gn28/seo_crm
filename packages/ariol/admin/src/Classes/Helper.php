<?php

namespace Ariol\Classes;

use Route;
use Schema;
use Request;
use DateTime;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Класс с полезными функциями.
 *
 * @package Ariol\Classes
 */
class Helper
{
    /**
     * Транслит и ретранслит текста.
     *
     * @param $string
     * @param bool $re
     * @return string
     */
    public static function translit($string, $re = false)
    {
        $converter = [
            'а' => 'a',   'б' => 'b',   'в' => 'v',
            'г' => 'g',   'д' => 'd',   'е' => 'e',
            'ё' => 'yo',  'ж' => 'zh',  'з' => 'z',
            'и' => 'i',   'й' => 'j',   'к' => 'k',
            'л' => 'l',   'м' => 'm',   'н' => 'n',
            'о' => 'o',   'п' => 'p',   'р' => 'r',
            'с' => 's',   'т' => 't',   'у' => 'u',
            'ф' => 'f',   'х' => 'x',   'ц' => 'c',
            'ч' => 'ch',  'ш' => 'sh',  'щ' => 'shh',
            'ь' => '\'',  'ы' => 'y',   'ъ' => '\'\'',
            'э' => 'e\'', 'ю' => 'yu',  'я' => 'ya',

            'А' => 'A',   'Б' => 'B',   'В' => 'V',
            'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
            'Ё' => 'YO',  'Ж' => 'Zh',  'З' => 'Z',
            'И' => 'I',   'Й' => 'J',   'К' => 'K',
            'Л' => 'L',   'М' => 'M',   'Н' => 'N',
            'О' => 'O',   'П' => 'P',   'Р' => 'R',
            'С' => 'S',   'Т' => 'T',   'У' => 'U',
            'Ф' => 'F',   'Х' => 'X',   'Ц' => 'C',
            'Ч' => 'CH',  'Ш' => 'SH',  'Щ' => 'SHH',
            'Ь' => '\'',  'Ы' => 'Y\'', 'Ъ' => '\'\'',
            'Э' => 'E\'', 'Ю' => 'YU',  'Я' => 'YA', ' ' => '_'
        ];

        return !$re ? strtr($string, $converter) : strtr($string, array_flip($converter));
    }

    public static function changeKeyboardLayout($string, $re = false)
    {
        $converter = [
            "Q" => "Й", "W" => "Ц", "E" => "У", "R" => "К",
            "T" => "Е", "Y" => "Н", "U" => "Г", "I" => "Ш",
            "O" => "Щ", "P" => "З", "{" => "Х", "}" => "Ъ",
            "A" => "Ф", "S" => "Ы", "D" => "В", "F" => "А",
            "G" => "П", "H" => "Р", "J" => "О", "K" => "Л",
            "L" => "Д", ":" => "Ж", "\"" => "Э", "Z" => "Я",
            "X" => "Ч", "C" => "С", "V" => "М", "B" => "И",
            "N" => "Т", "M" => "Ь", "<" => "Б", ">" => "Ю",
            "q" => "й", "w" => "ц", "e" => "у", "r" => "к",
            "t" => "е", "y" => "н", "u" => "г", "i" => "ш",
            "p" => "з", "[" => "х", "]" => "ъ", "a" => "ф",
            "s" => "ы", "d" => "в", "f" => "а", "g" => "п",
            "h" => "р", "j" => "о", "k" => "л", "l" => "д",
            ";" => "ж", "'" => "э", "z" => "я", "x" => "ч",
            "c" => "с", "v" => "м", "b" => "и", "n" => "т",
            "m" => "ь", "," => "б", "." => "ю"
        ];

        return !$re ? strtr($string, $converter) : strtr($string, array_flip($converter));
    }

    /**
     * Генерация строк.
     *
     * @param $number
     * @param bool $symbols
     * @return string
     */
    public static function generate($number, $symbols = true)
    {
        $arr = [
            'a','b','c','d','e','f','g','h','i','j','k','l',
            'm','n','o','p','r','s','t','u','v','x','y','z',
            'A','B','C','D','E','F','G','H','I','J','K','L',
            'M','N','O','P','R','S','T','U','V','X','Y','Z',
            '1','2','3','4','5','6','7','8','9','0','-','_'
        ];

        if ($symbols) {
            $arr = $arr + ['(',')','[',']','!','@','<','>','|','+',',','*','$','%','{','}'];
        }

        $code = '';

        for ($i = 0; $i < $number; $i++) {
            $index = rand(0, count($arr) - 1);
            $code .= $arr[$index];
        }

        return $code;
    }

    /**
     * Конвертирование дат в русский формат.
     *
     * @param $date
     * @param bool $slashes Для дат типа d/m/Y, так как strtotime не хочет работать с таким форматом.
     * @param bool $time
     * @return mixed
     */
    public static function getConvertedDate($date, $slashes = false, $time = false)
    {
        $en_month = [
            'January', 'February', 'March', 'April', 'May', 'June',
            'July', 'August', 'September', 'October', 'November', 'December'
        ];

        $ru_month = [
            'января', 'февраля', 'марта', 'апреля', 'мая', 'июня',
            'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'
        ];

        if (! $slashes) {
            if ($time) {
                $date = date('j F Y H:i', strtotime($date));
            } else {
                $date = date('j F Y', strtotime($date));
            }
        } else {
            $datetime = new DateTime();
            $newDate = $datetime->createFromFormat('d/m/Y', $date);

            if ($time) {
                $date = $newDate->format('j F Y H:i');
            } else {
                $date = $newDate->format('j F Y');
            }
        }

        return str_replace($en_month, $ru_month, $date);
    }

    /**
     * Разбивка текущей даты и времени на отдельные компоненты.
     *
     * @return array
     */
    public static function parseCurrentDate()
    {
        $currentDate = date('Y-m-d H:i:s');

        $parseDateAndTime = explode(' ', $currentDate);
        $parseDate = explode('-', $parseDateAndTime[0]);
        $parseTime = explode(':', $parseDateAndTime[1]);

        return [
            'day' => $parseDate[2],
            'month' => $parseDate[1],
            'year' => $parseDate[0],
            'seconds' => $parseTime[2],
            'minutes' => $parseTime[1],
            'hour' => $parseTime[0]
        ];
    }

    /**
     * День недели на русском языке.
     *
     * @param $date
     * @return mixed
     */
    public static function getRussianWeekday($date)
    {
        $weekdays = [
            'Monday' => 'Понедельник',
            'Tuesday' => 'Вторник',
            'Wednesday' => 'Среда',
            'Thursday' => 'Четверг',
            'Friday' => 'Пятница',
            'Saturday' => 'Суббота',
            'Sunday' => 'Воскресенье'
        ];

        return $weekdays[date('l', strtotime($date))];
    }

    /**
     * Получение читабельного названия файла.
     *
     * @param $name
     * @return mixed
     */
    public static function getReadableFileName($name)
    {
        return str_replace(['%20', '%28', '%29'], [' ', '(', ')'], $name);
    }

    /**
     * Преобразование названия файла в правильный вид.
     *
     * @param $name
     * @return mixed
     */
    public static function getWritableFileName($name)
    {
        return self::translit(str_replace(['(', ')', ' ', '%'], ['', '', '-', ''], $name));
    }

    /**
     * Определение размера файла.
     *
     * @param $file
     * @return string
     */
    public static function getFileSize($file)
    {
        $formats = array('B', 'KB', 'MB', 'GB', 'TB');
        $format = 0; // Формат размера по умолчанию.

        $fileSize = filesize($file);

        while ($fileSize > 1024 && count($formats) != ++$format) {
            $fileSize = round($fileSize / 1024, 2);
        }

        /* Если число большое, то выходим из цикла с форматом, превышающим максимальное значение,
        поэтому нужно добавить последний возможный размер файла в массив еще раз. */
        $formats[] = 'TB';

        return $fileSize . ' ' . $formats[$format];
    }

    /**
     * Конвертирование байтов в другие величины.
     *
     * @param $bytes
     * @return string
     */
    public static function getSymbolByQuantity($bytes)
    {
        $symbols = array('B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
        $exp = (string) floor(log($bytes) / log(1024));

        return sprintf('%.2f ' . $symbols[$exp], ($bytes / pow(1024, floor($exp))));
    }

    /**
     * Пагинация элементов.
     *
     * @param $items
     * @param $perPage
     * @param $path
     * @return LengthAwarePaginator
     */
    public static function paginate($items, $perPage, $path = '/')
    {
        $pageStart = Request::get('page', 1);

        /* Отображение элементов с заданной позиции. */
        $offSet = ($pageStart * $perPage) - $perPage;

        /* Получение необходимых элементов. */
        $itemsForCurrentPage = array_slice($items, $offSet, $perPage, true);

        return new LengthAwarePaginator(
            $itemsForCurrentPage,
            count($items),
            $perPage,
            Paginator::resolveCurrentPage(),
            ['path' => Paginator::resolveCurrentPath($path)]
        );
    }

    /**
     * Конвертирование json значения поля builder в массив.
     *
     * @param $json
     * @return array
     */
    public static function getBuilderValues($json)
    {
        $array = [];
        $data = self::getJson($json);

        if (! empty($data) && is_array($data)) {
            foreach ($data as $key => $values) {
                foreach ($values as $index => $value) {
                    $arr = [$key => $value];

                    if (! isset($array[$index])) {
                        $array[$index] = $arr;
                    } else {
                        $array[$index] += $arr;
                    }
                }
            }
        }

        return $array;
    }

    /**
     * Проверка на пустое значение одного из полей builder.
     *
     * @param $field
     * @return bool
     */
    public static function emptyBuilderValues($field)
    {
        return empty($field) || $field == '[null]' ? true : false;
    }

    /**
     * Назначение роутов для форм настроек
     *
     *@param $controllers
     * @param string $path
     */
    public static function getSettingsRoutes($controllers, $path = null)
    {
        $controllerPath = !empty($path) ? str_replace('/', '\\', $path) . '\\' : null;

        foreach ($controllers as $controller) {
            $prefix = strtolower($path . '/' . $controller);

            Route::group(['prefix' => $prefix], function () use ($controllerPath, $controller) {
                Route::get('/', 'Admin\\' . $controllerPath . $controller. 'Controller@index');
                Route::post('/', 'Admin\\' . $controllerPath . $controller. 'Controller@update');
            });
        }
    }

    /**
     * Назначение роутов для CRUD.
     *
     * @param $controllers
     * @param string $path
     */
    public static function getBaseRoutes($controllers, $path = null)
    {
        $controllerPath = !empty($path) ? str_replace('/', '\\', $path) . '\\' : null;

        foreach ($controllers as $controller) {
            $prefix = strtolower($path . '/' . $controller);

            Route::group(['prefix' => $prefix], function () use ($controllerPath, $controller) {
                Route::get('/', 'Admin\\' . $controllerPath . $controller. 'Controller@index');
                Route::post('/data', 'Admin\\' . $controllerPath . $controller. 'Controller@data');
                Route::get('/create', 'Admin\\' . $controllerPath . $controller. 'Controller@create');
                Route::post('/create', 'Admin\\' . $controllerPath . $controller. 'Controller@update');
                Route::get('/edit/{id}', 'Admin\\' . $controllerPath . $controller. 'Controller@edit');
                Route::post('/edit/{id}', 'Admin\\' . $controllerPath . $controller. 'Controller@update');
                Route::get('/delete/{id}', 'Admin\\' . $controllerPath . $controller. 'Controller@destroy');
            });
        }
    }

    /**
     * Получение данных из таблиц настроек.
     *
     * @param $model
     * @param $group
     * @param bool $like
     * @param null $replace
     * @return array|mixed
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public static function getSettings($model, $group, $like = false, $replace = null)
    {
        if (! class_exists($model)) {
            return print_r(translate('system.system.packageItems.model_not_exist'));
        }

        if (empty($group)) {
            return print_r(translate('system.system.packageItems.specify_group'));
        }

        /* Наименование таблицы. */
        $model = new $model;
        $tableName = with($model)->getTable();

        $settings = [];

        if (! $like) {
            $dataSettings = $model->where('group', $group);

            if ($model->languagable && Schema::hasColumn($tableName, $model->languageField)) {
                $dataSettings = $dataSettings->where($model->languageField, Localization::getLocale());
            }

            $dataSettings = $dataSettings->get();

            foreach ($dataSettings as $group) {
                $settings += [$group->key => $group->value];
            }
        } else {
            $replace = empty($replace) ? $group : $replace;
            $dataSettings = $model->where('group', 'like', '%' . $group . '%');

            if ($model->languagable && Schema::hasColumn($tableName, $model->languageField)) {
                $dataSettings = $dataSettings->where($model->languageField, Localization::getLocale());
            }

            $dataSettings = $dataSettings->get();

            foreach ($dataSettings as $group) {
                $key = str_replace($replace, '', $group->group);

                $settings += [$key => []];
                $settings[$key] += [$group->key => $group->value];
            }
        }

        return $settings;
    }

    /**
     * Конвертирование любого представления телефонного номера в цифровую последовательность для вызова в браузерах.
     *
     * @param $phone
     * @return mixed
     */
    public static function getConvertedPhone($phone)
    {
        return str_replace(['(', ')', '-', ' '], '', $phone);
    }

    /**
     * Вывод текста с переносами строк.
     *
     * @param $text
     * @return string
     */
    public static function getText($text)
    {
        return nl2br(e($text));
    }

    /**
     * Декодирование json-строки.
     *
     * @param $json
     * @return mixed
     */
    public static function getJson($json)
    {
        $data = !empty($json) ? json_decode($json, true) : [];

        return !empty($data) && !empty($data[key($data)]) ? $data : [];
    }

    /**
     * Получение адреса из json.
     *
     * @param $address
     * @return null
     */
    public static function getAddress($address)
    {
        $address = self::getJson($address);
        $address = !empty($address) ? $address[0] : null;

        return $address;
    }

    /**
     * Склонения слов в зависимости от числа.
     *
     * @param $number
     * @param $before
     * @param $after
     * @return string
     */
    public static function plural($number, $before, $after)
    {
        $cases = [2, 0, 1, 1, 1, 2];

        $before = $before[($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[min($number % 10, 5)]] . ' ';
        $after = ' ' . $after[($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[min($number % 10, 5)]];

        return $before . $number . $after;
    }
}
