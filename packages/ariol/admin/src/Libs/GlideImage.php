<?php

namespace Ariol\Admin\Libs;

use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use Orchestra\Imagine\Facade as Imagine;

/**
 * Класс для обрезки изображений.
 *
 * @package Ariol\Admin\Libs
 */
class GlideImage
{
    /**
     * Путь до изображений.
     *
     * @return string
     */
    public static function getMainPath()
    {
        return public_path() . config('ariol.files-path');
    }

    /**
     * Загрузка изображения с заданными размерами.
     *
     * @param $path
     * @param $params
     * @return string
     */
    public static function load($path, $params)
    {
        self::modifyParams($params);

        if (is_dir(public_path() . $path) || !file_exists(public_path() . $path)) {
            if (file_exists(public_path() . '/img/thumb' . $params['w'] . 'x' . $params['h'] . '.png')) {
                return '/img/thumb' . $params['w'] . 'x' . $params['h'] . '.png';
            }

            return $path;
        }

        $nameParts = explode('/', $path);
        $name = str_replace(' ', '', end($nameParts));

        $thumbpath = self::getMainPath() . 'resized/' . $params['w'] . 'x' .  $params['h'] . '-' . $name;
        $thumbpath = str_replace(' ', '', $thumbpath);

        if (! file_exists($thumbpath)) {
            $mode = ImageInterface::THUMBNAIL_OUTBOUND;
            $size = new Box($params['w'], $params['h']);

            $thumbnail = Imagine::open(public_path() . $path)->thumbnail($size, $mode);
            $thumbnail->save($thumbpath);
        }

        return config('ariol.files-path') . 'resized/' . $params['w'] . 'x' .  $params['h'] . '-' . $name;
    }

    /**
     * Изменение размера изображения.
     *
     * @param $path
     * @param $params
     * @return string
     */
    public static function resize($path, $params)
    {
        self::modifyParams($params);

        if (is_dir(public_path() . $path) || !file_exists(public_path() . $path)) {
            if (file_exists(public_path() . '/img/thumb' . $params['w'] . 'x' . $params['h'] . '.png')) {
                return '/img/thumb' . $params['w'] . 'x' . $params['h'] . '.png';
            }

            return $path;
        }

        $nameParts = explode('/', $path);
        $name = str_replace(' ', '', end($nameParts));

        $pathFile = self::getMainPath() . 'resized/' . $params['w'] . 'x' .  $params['h'] . '-' . $name;
        $thumbpath = str_replace(' ', '', $pathFile);

        if (! file_exists($thumbpath)) {
            $mode = ImageInterface::THUMBNAIL_OUTBOUND;
            $size = new Box($params['w'], $params['h']);

            $thumbnail = Imagine::open(public_path() . $path)->thumbnail($size, $mode);
            $thumbnail->save($thumbpath);

            $imagine = new \Imagick($thumbpath);
            $imagine->thumbnailImage($params['w'], $params['h']);
            $imagine->setCompressionQuality(100);
            $imagine->writeImage($pathFile);
        }

        return config('ariol.files-path') . 'resized/' . $params['w'] . 'x' .  $params['h'] . '-' . $name;
    }

    /**
     * Изменение размера большого изображения.
     *
     * @param $path
     * @param $params
     * @return string
     */
    public static function resizeBig($path, $params)
    {
        self::modifyParams($params);

        if (is_dir(public_path() . $path) || !file_exists(public_path() . $path)) {
            if (file_exists(public_path() . '/img/thumb' . $params['w'] . 'x' . $params['h'] . '.png')) {
                return '/img/thumb' . $params['w'] . 'x' . $params['h'] . '.png';
            }

            return $path;
        }

        $nameParts = explode('/', $path);
        $name = str_replace(' ', '', end($nameParts));

        $pathFile = self::getMainPath() . 'resized/bg-' . $params['w'] . 'x' .  $params['h'] . '-' . $name;
        $thumbpath = str_replace(' ', '', $pathFile);

        if (! file_exists($thumbpath)) {
            $imagine = new \Imagick(public_path() . $path);
            $imagine->thumbnailImage($params['w'], $params['h'], true, true);
            $imagine->setCompressionQuality(100);
            $imagine->writeImage($pathFile);
        }

        return config('ariol.files-path') . 'resized/bg-' . $params['w'] . 'x' .  $params['h'] . '-' . $name;
    }

    /**
     * Компактное изображение.
     *
     * @param $path
     * @param $params
     * @return string
     */
    public static function compact($path, $params)
    {
        if (is_dir(public_path() . $path) || !file_exists(public_path() . $path)) {
            return $path;
        }

        $nameParts = explode('/', $path);
        $name = str_replace(' ', '', end($nameParts));

        $pathFile = self::getMainPath() . 'resized/rez-' . $name;
        $thumbpath = str_replace(' ', '', $pathFile);

        if (! file_exists($thumbpath)) {
            $imagine = new \Imagick(public_path() . $path);
            $imagine->resizeImage($params['w'], $params['h'], \Imagick::FILTER_LANCZOS, 1);
            $imagine->setCompressionQuality(100);
            $imagine->writeImage($pathFile);
        }

        return config('ariol.files-path') . 'resized/rez-' . $name;
    }

    /**
     * Изменение параметров.
     *
     * @param $params
     */
    protected static function modifyParams(&$params)
    {
        if (!isset($params['w']) && isset($params['h'])) {
            $params['w'] = $params['h'];
        } elseif (isset($params['w']) && !isset($params['h'])) {
            $params['h'] = $params['w'];
        }
    }
}
