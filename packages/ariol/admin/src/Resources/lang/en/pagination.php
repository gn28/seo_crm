<?php return [
    'packageTitle' => 'Pagination',
    'previous' => '« Previous',
    'next' => 'Next »',
];