<?php return [
    'packageTitle' => 'Система',
    'menu' => [
        'packageSubTitle' => 'Админ-меню',
        'packageItems' => [
            'my-account' => 'Мой аккаунт',
            'exit' => 'Выйти',
            'menu' => 'Меню',
            'users' => [
                'packageSubTitle' => 'Пользователи',
                'packageItems' => [
                    'users' => 'Пользователи',
                    'roles' => 'Роли',
                    'permissions' => 'Права'
                ]
            ],
            'pages' => 'Страницы сайта',
            'system-settings' => [
                'packageSubTitle' => 'Системные настройки',
                'packageItems' => [
                    'cache-clear' => 'Очистка кеша',
                    'localization' => 'Локализация'
                ]
            ]
        ]
    ],
    'grid' => [
        'packageSubTitle' => 'Таблица данных',
        'packageItems' => [
            'on' => 'Вкл',
            'off' => 'Выкл',
            'create' => 'Создать',
            'search' => 'Поиск',
            'show' => 'Показать',
            'no-data' => 'Записи отсутствуют.',
            'clear-filter' => 'Очистить фильтр',
            'loading' => 'Загрузка...',
            'no-selected' => 'Вы ничего не выбрали.',
            'no-empty' => 'Поле не может быть пустым.',
            'no-isset' => 'Такого поля не существует в таблице.',
            'success' => 'Запись успешно изменена.',
            'delete-selected' => 'Удалить выбранное',
            'first' => 'Первая',
            'last' => 'Последняя',
            'yes-delete' => 'Да, удалить',
            'no-cancel' => 'Нет, отменить',
            'delete-success' => 'Выбранные записи успешно удалены.',
            'delete-are-you-sure' => 'Вы уверены, что хотите удалить всё, что выделено?'
        ]
    ],
    'form' => [
        'packageSubTitle' => 'Формы',
        'packageItems' => [
            'tab' => 'Вкладка',
            'creating' => 'Создание',
            'editing' => 'Редактирование',
            'select-item' => 'Выберите элемент',
            'no-founded' => 'Ничего не найдено.',
            'select-all' => 'Выбрать все',
            'all-selected' => 'Все выбраны',
            'selected' => 'Выбранные',
            'no-selected' => 'Ничего не выбрано',
            'alias' => 'Если оставить пустым, то автоматически сгенерируется.',
            'save' => 'Сохранить',
            'save-and-cancel' => 'Сохранить и закрыть',
            'cancel' => 'Отменить',
            'add' => 'Добавить',
            'change' => 'Изменить',
            'delete' => 'Удалить',
            'input-text' => 'Введите текст',
            'input-1-symbol' => 'Введите 1 или несколько символов.',
            'input-too-long' => 'Слишком длинный запрос.',
            'input-error-loading' => 'Ошибка при загрузке результатов.',
            'input-loading-more' => 'Загрузка дополнительных результатов.',
            'input-no-results' => 'Ничего не найдено.',
            'input-searching' => 'Поиск...',
            'input-maximum-selected' => 'Ошибка при загрузке результатов.',
            'no-file' => 'У этой записи нет прикреплённого файла.',
            'success' => 'Успешно сохранено.',
            'field-require' => 'Заполните обязательные поля.',
            'field-unique' => 'Такое значение уже используется.',
            'field-validate' => 'Проверьте введённые данные.',
            'no-isset' => 'Такой записи не существует.',
            'destroyed' => 'Успешно удалено.',
            'no-permissions' => 'У Вас нет прав для входа в административную панель.',
            'length-password' => 'Длина пароля должна составлять минимум 6 символов.',
            'no-confirmed-passwords' => 'Пароли не совпадают.',
            'isset-value' => 'Такое значение (сгенерированное) уже используется.',
            'back' => 'Вернуться назад',
            'changes' => 'изменения',
            'and-close' => 'и закрыть',
            'delete-are-you-sure' => 'Вы уверены, что хотите удалить данную запись?',
            'are-you-sure' => 'Вы уверены?',
            'date-and-time' => [
                'packageSubTitle' => 'Дата и время',
                'packageItems' => [
                    'Jan' => 'Янв',
                    'Feb' => 'Фев',
                    'Mar' => 'Мрт',
                    'Apr' => 'Апр',
                    'May' => 'Май',
                    'Jun' => 'Июн',
                    'Jul' => 'Июл',
                    'Aug' => 'Авг',
                    'Sep' => 'Сен',
                    'Oct' => 'Окт',
                    'Nov' => 'Нбр',
                    'Dec' => 'Дек',
                    'January' => 'Январь',
                    'February' => 'Февраль',
                    'March' => 'Март',
                    'April' => 'Апрель',
                    'June' => 'Июнь',
                    'July' => 'Июль',
                    'August' => 'Август',
                    'September' => 'Сентябрь',
                    'October' => 'Октябрь',
                    'November' => 'Ноябрь',
                    'December' => 'Декабрь',
                    'Sun' => 'Вс',
                    'Mon' => 'Пн',
                    'Tue' => 'Вт',
                    'Wed' => 'Ср',
                    'Thu' => 'Чт',
                    'Fri' => 'Пт',
                    'Sat' => 'Сб',
                    'close' => 'Закрыть',
                    'today' => 'Сегодня',
                    'next-month' => 'След. месяц',
                    'prev-month' => 'Пред. месяц',
                    'select-month' => 'Выберите месяц',
                    'select-year' => 'Выберите год',
                    'select-time' => 'Выберите время',
                    'select-date' => 'Выберите дату',
                    'select-date-and-time' => 'Выберите дату и время',
                    'month' => 'Месяц',
                    'minutes' => 'Минуты',
                    'seconds' => 'Секунды',
                    'hour' => 'Час',
                    'year' => 'Год',
                    'day' => 'День'
                ]
            ],
            'color' => [
                'packageSubTitle' => 'Выбор цвета',
                'packageItems' => [
                    'cancel' => 'Отмена',
                    'choose' => 'Выбрать',
                    'clear' => 'Сбросить',
                    'no-selected' => 'Цвет не выбран',
                    'more' => 'Ещё',
                    'hide' => 'Скрыть'
                ]
            ]
        ]
    ],
    'modules' => [
        'packageSubTitle' => 'Модули',
        'packageItems' => [
            'main-page' => [
                'packageSubTitle' => 'Главная страница',
                'packageItems' => [
                    'free-space' => 'Свободно места'
                ],
            ],
            'cache-clear' => [
                'packageSubTitle' => 'Очистка кеша',
                'packageItems' => [
                    'mb' => 'Мб',
                    'clear' => 'Очистить',
                    'cache-site' => 'Кеш сайта',
                    'trash' => 'Скопившийся мусор',
                    'cache-system' => 'Системный кеш',
                    'useless-images' => 'Неиспользуемые изображения'
                ]
            ],
            'localization' => [
                'packageSubTitle' => 'Локализация',
                'packageItems' => [
                    'of' => 'из',
                    'add' => 'Добавить',
                    'close' => 'Закрыть',
                    'phrases' => 'Выражений',
                    'translated' => 'Переведено',
                    'add-language' => 'Добавить язык',
                    'untranslated' => 'Непереведённые',
                    'select-language' => 'Выберите язык',
                    'available-languages' => 'Доступные языки',
                    'tooltip-remove-language' => 'Удаление языка',
                    'isset-language' => 'Данный язык уже добавлен.',
                    'tooltip-active-language' => 'Активность языка',
                    'tooltip-default-language' => 'Язык по умолчанию',
                    'russian-no-delete' => 'Русский язык нельзя удалять.',
                    'no-isset-language' => 'Данного языка не существует.',
                    'add-language-success' => 'Язык был успешно добавлен.',
                    'remove-language-success' => 'Язык был успешно удалён.',
                    'success-save-translate' => 'Перевод успешно сохранён.',
                    'toggle-activate' => 'Языковой пакет успешно активирован.',
                    'enter-a-translation' => 'Кликните, чтобы ввести перевод.',
                    'toggle-deactivate' => 'Языковой пакет успешно деактивирован.',
                    'error-save-translate' => 'Произошла ошибка при сохранении перевода.',
                    'toggle-default' => 'Языковой пакет успешно установлен по умолчанию.',
                    'delete-are-you-sure' => 'Вы уверены, что хотите удалить данный язык?',
                    'no-active' => 'Нельзя назначить язык по умолчанию, так как он деактивирован.',
                    'set-default' => 'Нельзя деактивировать язык, так как он выбран по умолчанию.',
                    'only-active' => 'Нельзя деактивировать язык, так как он единственный активный.',
                ]
            ]
        ]
    ],
    'authorization' => [
        'packageSubTitle' => 'Вход в панель управления',
        'packageItems' => [
            'title' => 'Авторизация',
            'remember-me' => 'Запомнить меня',
            'enter' => 'Войти'
        ]
    ],
    'system' => [
        'packageSubTitle' => 'Система',
        'packageItems' => [
            'model_not_exist' => 'Такой модели не существует.',
            'specify_group' => 'Укажите группу.'
        ]
    ]
];
