<?php return [
    'packageTitle' => 'Админка',
    'common' => [
        'packageSubTitle' => 'Общее',
        'packageItems' => [
            'header' => 'Шапка',
            'footer' => 'Подвал',
            'name' => 'Наименование',
            'title' => 'Заголовок',
            'subtitle' => 'Подзаголовок',
            'description' => 'Описание',
            'short_description' => 'Краткое описание',
            'full_description' => 'Полное описание',
            'content' => 'Контент',
            'information' => 'Информация',
            'url' => 'Ссылка',
            'alias' => 'Алиас',
            'alias-description' => 'Если оставить пустым, то автоматически сгенерируется.',
            'active' => 'Активный',
            'image' => 'Изображение',
            'favicon' => 'Иконка во вкладке браузера',
            'logo' => 'Логотип',
            'position' => 'Позиция',
            'e-mail' => 'E-mail',
            'phone' => 'Телефон',
            'address' => 'Адрес',
            'copyright' => 'Копирайт',
            'seo' => 'Поисковая оптимизация',
            'keywords' => 'Ключевые слова'
        ]
    ],
    'modules' => [
        'packageSubTitle' => 'Модули',
        'packageItems' => [
            'menu' => [
                'packageSubTitle' => 'Меню',
                'packageItems' => [
                    'filter' => 'Активные',
                    'active' => 'Отображение',
                    'parent' => 'Категория',
                    'main_category' => 'Основная категория'
                ]
            ],
            'users' => [
                'packageSubTitle' => 'Пользователи',
                'packageItems' => [
                    'name' => 'Имя',
                    'role_id' => 'Роль',
                    'password' => 'Пароль',
                    'permissions' => 'Права',
                    'repeat-password' => 'Повторите пароль',
                    'module' => 'Модуль',
                    'entries' => 'Записи',
                    'actions' => 'Действия',
                    'readonly' => 'Только просмотр',
                    'creatable' => 'Создание',
                    'editable' => 'Редактирование',
                    'destroyable' => 'Удаление'
                ]
            ]
        ]
    ]
];
