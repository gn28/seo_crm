<div class="col-xs-12 col-md-{{ $column[0] }} col-md-offset-{{ $column[1] }} col-md-offset-right-{{ $column[2] }}">
    <select @if (! $searchable) data-minimum-results-for-search="Infinity" @endif
            class="select-grid" data-filter-label="{{ $label }}" name="{{ $name }}">
        <option value="no-selected">{{ $title }}</option>
        @foreach ($values as $key => $value)
            <option value="{{ $key }}">{{ $value }}</option>
        @endforeach
    </select>
</div>