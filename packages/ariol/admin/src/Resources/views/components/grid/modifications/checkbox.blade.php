<label for="{{ $id }}" class="checkbox-table">
    <input data-item-id="{{ $id }}" value="{{ $id }}" class="styled" type="checkbox" name="items[]"
           {{ !empty($disabled) ? 'disabled' : null }}>
</label>