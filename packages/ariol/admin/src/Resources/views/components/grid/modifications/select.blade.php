<select data-live-search="true" class="form-control select-grid" data-field-name="{{ $field }}"
        data-entry-id="{{ $id }}" data-field-value="{{ $selected }}">
    <option value="0" selected>Не выбрано</option>
    @foreach ($values as $key => $value)
        <option {{ ($key == $selected) ? 'selected=selected' : null }}
                value="{{ $key }}">{{ $value }}</option>
    @endforeach
</select>