<a data-link="{{ $link }}" class="delete-item-confirm btn btn-danger"
   data-confirm-text="{{ translate('system.form.packageItems.delete-are-you-sure') }}"
   {{ !empty($disabled) ? 'disabled' : null }}>
    <i class="icon-trash"></i>
</a>