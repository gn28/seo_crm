<a @if (empty($alias)) href="{{ $url }}" @else data-{{ $alias }}="{{ $id }}" @endif>
    <button class="btn btn-{{ $color }}">
        <i class="{{ $icon }}"></i>
    </button>
</a>