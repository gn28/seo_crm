<div contenteditable="true" data-field-name="{{ $field }}" data-entry-id="{{ $id }}"
     data-field-value="{{ $value }}">
    {{ $value }}
</div>