<div class="col-xs-12 col-md-{{ $column[0] }} col-md-offset-{{ $column[1] }} col-md-offset-right-{{ $column[2] }}">
    <div class="col-xs-12 col-md-{{ $column[0] }} col-md-offset-{{ $column[1] }} col-md-offset-right-{{ $column[2] }}">
        <select @if (! $searchable) data-minimum-results-for-search="Infinity" @endif
        class="multiselect-grid multiselect-select-all-filtering" data-filter-label="{{ $label }}" name="{{ $name }}[]" multiple>
            @foreach ($values as $key => $value)
                <option value="{{ $key }}">{{ $value }}</option>
            @endforeach
        </select>
    </div>
</div>