<div class="col-xs-12 col-md-{{ $column[0] }} col-md-offset-{{ $column[1] }} col-md-offset-right-{{ $column[2] }}">
    <div class="p-t-8">
        <label class="checkbox-inline">
            <input class="styled" type="checkbox" data-filter-field="{{ $field }}"
                   name="{{ $name }}" {{ !empty($checked) ? 'checked' : null }}>
            <strong>{!! $label !!}</strong>
        </label>
    </div>
</div>