<li class="{{ $tabActive }}">
    <a href="#tab_{{ $tabGroup }}" data-toggle="tab" data-ariol-tab="tab_{{ $tabGroup }}">
        {{ $tabTitle }}
    </a>
</li>