<div class="col-xs-12 col-md-{{ $column[0] }} col-md-offset-{{ $column[1] }} col-md-offset-right-{{ $column[2] }}">
    <div class="form-group">
        @if (! empty($label))
            <label for="{{ (! empty($groupTab) ? $groupTab . '_' : null) . $name }}">
                <strong>{!! $label !!}</strong>
                @if ($require)
                    <span class="text-danger-800">*</span>
                @endif
            </label>
        @endif
        <select data-live-search="true" class="form-control @if ($duplicate) duplicate @endif" name="{{ $name . $builder }}"
                id="{{ (! empty($groupTab) ? $groupTab . '_' : null) . $name }}"
                {{ !empty($readonly) ? 'disabled' : null }}>
            @if ($nullable)
                <optgroup label="Управление">
                    <option {{ ($selected == 0) ? 'selected=selected' : null }} value="0">Выберите элемент</option>
                </optgroup>
            @endif
            @foreach ($values as $key => $value)
                <optgroup label="{{ $value['name'] }}">
                    @foreach ($value['values'] as $id => $val)
                        <option {{ ($id == $selected) ? 'selected=selected' : null }}
                                value="{{ $id }}">{{ $val }}</option>
                    @endforeach
                </optgroup>
            @endforeach
        </select>
        @if (! empty($description))
            <span class="help-block">{{ $description }}</span>
        @endif
    </div>
</div>