<div class="col-xs-12 col-md-{{ $column[0] }} col-md-offset-{{ $column[1] }} col-md-offset-right-{{ $column[2] }}">
    <div class="form-group">
        @if (! empty($label))
            <label for="{{ (! empty($groupTab) ? $groupTab . '_' : null) . $name }}">
                <strong>{!! $label !!}</strong>
                @if ($require)
                    <span class="text-danger-800">*</span>
                @endif
            </label>
        @endif
        <div class="input-group">
            <select class="form-control select-ajax select-remote-data @if ($duplicate) duplicate @endif" name="{{ $name . $builder }}"
                    id="{{ (! empty($groupTab) ? $groupTab . '_' : null) . $name }}" data-url="{{ $url }}"
                    {{ !empty($readonly) ? 'disabled' : null }} data-model="{{ $model }}" data-field="{{ $field }}">
                <option value="{{ $value['id'] }}" selected>{{ $value['name'] }}</option>
            </select>
            <div class="input-group-btn">
                <button type="button" class="btn btn-danger legitRipple remove_select_autocomplete">
                    <i class="icon-trash"></i>
                </button>
            </div>
        </div>
        @if (! empty($description))
            <span class="help-block">{{ $description }}</span>
        @endif
    </div>
</div>