<div class="col-xs-12 col-md-{{ $column[0] }} col-md-offset-{{ $column[1] }} col-md-offset-right-{{ $column[2] }}">
    <div class="form-group">
        @if (! empty($label))
            <label for="{{ (! empty($groupTab) ? $groupTab . '_' : null) . $name }}">
                <strong>{!! $label !!}</strong>
                @if ($require)
                    <span class="text-danger-800">*</span>
                @endif
            </label>
        @endif
        <div class="input-group">
            <span class="input-group-addon"><i class="icon-watch2"></i></span>
            <input id="{{ (! empty($groupTab) ? $groupTab . '_' : null) . $name . $number }}"
                   style="cursor: pointer;" class="form-control pickatime" autocomplete="off"
                   value="{{ $value }}" name="{{ $name . $builder }}"
                   placeholder="{{ translate('system.form.packageItems.date-and-time.packageItems.select-time') }}">
            <div class="input-group-btn">
                <button type="button" class="btn btn-danger legitRipple remove_input">
                    <i class="icon-trash"></i>
                </button>
            </div>
        </div>
        @if (! empty($description))
            <span class="help-block">{{ $description }}</span>
        @endif
    </div>
</div>