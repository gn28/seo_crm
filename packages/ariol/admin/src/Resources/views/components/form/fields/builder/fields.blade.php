<div class="sortable-fields" @if (! empty($model)) data-builder-model="{{ $model }}" @endif data-main-id="{{ $mainId }}">
    <div class="form-fields">
        {!! $fields !!}
    </div>
    <div class="form-fields-buttons text-{{ $align }}" data-builder-buttons="{{ $name }}">
        <div class="input-group-btn">
            <button type="button" class="btn btn-success legitRipple add_more_builder_elements_hash">
                <i class="icon-add"></i>
            </button>
            <button type="button" class="btn btn-danger legitRipple remove_more_builder_elements_hash">
                <i class="icon-trash"></i>
            </button>
            <button type="button" class="btn btn-primary btn-builder-move legitRipple">
                <i class="icon-move"></i>
            </button>
        </div>
    </div>
</div>