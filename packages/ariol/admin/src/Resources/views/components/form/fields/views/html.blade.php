@if (! empty($value))
    <div class="col-xs-12 col-md-{{ $column[0] }} col-md-offset-{{ $column[1] }} col-md-offset-right-{{ $column[2] }}">
        <div class="form-group">
            @if (! empty($label))
                <label for="{{ (! empty($groupTab) ? $groupTab . '_' : null) . $name }}">
                    <strong>{!! $label !!}</strong>
                </label><br>
            @endif
            {!! $value !!}
            @if (! empty($description))
                <span class="help-block">{{ $description }}</span>
            @endif
        </div>
    </div>
@endif