    <div class="form-group buttons {{ !empty($settings) ? 'm-l-10' : null }}">
        @if (! empty($buttons) && is_array($buttons))
            @foreach ($buttons as $button)
                {!! $button !!}
            @endforeach
        @else
            @if (empty($readonly))
                @if (empty($groupTab))
                    <button id="save-reload" class="btn btn-success btn-form save-changes legitRipple">
                        <i class="icon-spinner4 spinner position-left save-load-changes hidden"></i>
                        {{ translate('system.form.packageItems.save') }}
                    </button>
                @endif
                <button id="save-redirect" class="btn btn-success btn-form save-changes legitRipple">
                    <i class="icon-spinner4 spinner position-left save-load-changes hidden"></i>
                    {{ translate('system.form.packageItems.save') }}
                    {{ !empty($groupTab) ? translate('system.form.packageItems.changes') : translate('system.form.packageItems.and-close') }}
                </button>
                @if (empty($groupTab))
                    <a id="form-cancel" href="{{ $mainPath }}" class="btn btn-danger btn-form">
                        {{ translate('system.form.packageItems.cancel') }}
                    </a>
                @endif
            @else
                <a href="{{ $mainPath }}" class="btn btn-warning legitRipple">
                    <i class="icon-spinner4 spinner position-left hidden"></i>
                    {{ translate('system.form.packageItems.back') }}
                </a>
            @endif
        @endif
    </div>
</form>