<div class="col-xs-12 col-md-{{ $column[0] }} col-md-offset-{{ $column[1] }} col-md-offset-right-{{ $column[2] }}">
    <div class="form-group">
        @if (! empty($label))
            <label for="{{ (! empty($groupTab) ? $groupTab . '_' : null) . $name }}">
                <strong>{!! $label !!}</strong>
                @if ($require)
                    <span class="text-danger-800">*</span>
                @endif
            </label>
        @endif
        <div class="multi-select-full">
            <select class="{{ empty($standart) ? 'multiselect-select-all-filtering' : null }} {{ !empty($builder) ? 'builder-count-block builder-multiple' : null }}" name="{{ $name . $builder }}[]"
                    id="{{ (! empty($groupTab) ? $groupTab . '_' : null) . $name }}" multiple
                    {{ !empty($readonly) ? 'disabled' : null }}>
                @foreach ($values as $key => $value)
                    <optgroup label="{{ $value['name'] }}">
                        @foreach ($value['values'] as $id => $val)
                            <option {{ !empty($selected) && is_array($selected) && in_array($id, $selected) ? 'selected=selected' : null }}
                                    value="{{ $id }}">{{ $val }}</option>
                        @endforeach
                    </optgroup>
                @endforeach
            </select>
        </div>
        @if (! empty($description))
            <span class="help-block">{{ $description }}</span>
        @endif
    </div>
</div>