<div class="col-xs-12 col-md-{{ $column[0] }} col-md-offset-{{ $column[1] }} col-md-offset-right-{{ $column[2] }}">
    <div class="form-group">
        @if (! empty($label))
            <label for="{{ $name }}">
                <strong>{!! $label !!}</strong>
                @if ($require)
                    <span class="text-danger-800">*</span>
                @endif
            </label>
        @endif
        <input id="{{ $name }}" data-id-address="{{ $name }}"
               class="addressPicker form-control" name="{{ $name . $builder }}[]"
               placeholder="{{ $placeholder }}" value="{{ $value[0] }}">
        <input type="hidden" id="{{ $name }}_lat"
               name="{{ $name . $builder }}[]" value="{{ $value[1] }}">
        <input type="hidden" id="{{ $name }}_lng"
               name="{{ $name . $builder }}[]" value="{{ $value[2] }}">
    </div>
    <div class="form-group">
        <div id="{{ $name }}_map" class="map-wrapper content-group-sm"></div>
        @if (! empty($description))
            <span class="help-block">{{ $description }}</span>
        @endif
    </div>
</div>