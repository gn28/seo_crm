<div class="col-xs-12 col-md-{{ $column[0] }} col-md-offset-{{ $column[1] }} col-md-offset-right-{{ $column[2] }}">
    <div class="form-group">
        @if (! empty($label))
            <label class="checkbox-inline">
                <input id="{{ (! empty($groupTab) ? $groupTab . '_' : null) . $name }}"
                       name="{{ $name . $builder }}" class="styled" type="checkbox" value="{{ $value }}"
                        {{ ($value == 1 || ($value != 0 && $checked)) ? 'checked=checked' : null }}
                        {{ !empty($readonly) ? 'disabled' : null }}>
                <strong>{!! $label !!}</strong>
                @if ($require)
                    <span class="text-danger-800">*</span>
                @endif
            </label>
        @endif
        @if (! empty($description))
            <span class="help-block">{{ $description }}</span>
        @endif
    </div>
</div>