<div class="col-xs-12 col-md-{{ $column[0] }} col-md-offset-{{ $column[1] }} col-md-offset-right-{{ $column[2] }}">
    @if (! empty($label))
        <label for="{{ (! empty($groupTab) ? $groupTab . '_' : null) . $name }}">
            <strong>{!! $label !!}</strong>
            @if ($require)
                <span class="text-danger-800">*</span>
            @endif
        </label>
    @endif
    <a class="btn btn-small resize hidden" data-toggle="modal" data-target="#cropModal{{ $name . $builder }}">
        <i class="icon-edit position-left"></i> Изменить размер
    </a>
    <input name="{{ $name . $builder }}" type="hidden" value="{{ $value }}"
           class="jquery-file {{ !empty($builder) ? 'builder-count-block' : null }}">
           <!-- Modal -->
          <div class="modal fade" id="cropModal{{ $name . $builder }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-body crop">

                </div>
                <div class="footer">
                  <a href="#" class="btn btn-success basic-result" data-target="{{ $name . $builder }}">Обновить</a>
                </div>
              </div>
            </div>
          </div>
    @if (! $creatable && empty($value))
        <div class="alert alert-primary alert-bordered m-b-0">
            <span class="text-semibold">
                {{ translate('system.form.packageItems.no-file') }}
            </span>
        </div>
    @endif
    <div class="file-upload-count">
        <div class="row fileupload-buttonbar">
            <div class="col-lg-7">
                @if ($creatable)
                    <div class="uploader">
                        <input class="file-styled-primary" name="selectedFilesForUpload[]" type="file">
                        <span class="action btn bg-blue legitRipple" style="-moz-user-select: none;">
                            <i class="icon-add position-left"></i>
                            {{ !empty($value) ? translate('system.form.packageItems.change') : translate('system.form.packageItems.add') }}
                        </span>
                    </div>
                @endif
                @if (! empty($description))
                    <span class="help-block">{{ $description }}</span>
                @endif
                <span class="fileupload-process"></span>
            </div>
            <div class="col-lg-5 fileupload-progress fade">
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                    <div class="progress-bar progress-bar-success" style="width: 0;"></div>
                </div>
                <div class="progress-extended"></div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-bordered" role="presentation">
                <tbody class="files">
                    <tr class="template-download fade in {{ empty($value) || !file_exists(public_path() . $value) ? 'no-file' : null }}">
                        @if (! empty($value) && file_exists(public_path() . $value))
                            <td style="width: 10%;">
                                <a class="image-viewer" download="" title="" href="{{ $value }}">
                                    @if (! empty($mime) && in_array($mime, $allowedMimeTypes))
                                        @if ($mime == 'image/svg+xml' || $mime == 'image/x-icon')
                                            <img src="{{ $value }}" style="width: {{ config('ariol.preview-size.width') }}px; height: {{ config('ariol.preview-size.height') }}px">
                                        @else
                                            <img src="{{ GlideImage::load($value, [
                                                'h' => config('ariol.preview-size.height'),
                                                'w' => config('ariol.preview-size.width')
                                            ]) }}">
                                        @endif
                                    @else
                                        <img src="{{ URL::asset('ariol/assets/images/custom/file.svg') }}" alt="File">
                                    @endif
                                </a>
                            </td>
                            <td style="width: 80%;">
                                <span>{{ $filename }}</span>
                            </td>
                            @if ($destroyable)
                                <td style="width: 10%;">
                                    <button class="btn btn-danger delete legitRipple" data-url="{{ $value }}" data-type="DELETE">
                                        <i class="icon-trash position-left"></i>
                                        {{ translate('system.form.packageItems.delete') }}
                                    </button>
                                </td>
                            @endif
                        @endif
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
