<div class="col-xs-12 col-md-{{ $column[0] }} col-md-offset-{{ $column[1] }} col-md-offset-right-{{ $column[2] }}">
    <div class="form-group">
        @if (! empty($label))
            <label for="{{ (! empty($groupTab) ? $groupTab . '_' : null) . $name }}">
                <strong>{!! $label !!}</strong>
                @if ($require)
                    <span class="text-danger-800">*</span>
                @endif
            </label>
        @endif
        <div class="array_type_data">
            @if ($value && is_array($value))
                <div class="sortable_hash">
                    @foreach ($value as $key => $item)
                        <div class="type_array">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6">
                                            <input class="form-control first" name="{{ $name . $builder }}[]" value="{{ !empty($item[0]) ? $item[0] : null }}"
                                                   autocomplete="off" placeholder="{{ !empty($placeholder[0]) ? $placeholder[0] : null }}">
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                            <div class="input-group">
                                                <input class="form-control" name="{{ $name . $builder }}[]" value="{{ !empty($item[1]) ? $item[1] : null }}"
                                                       autocomplete="off" placeholder="{{ !empty($placeholder[1]) ? $placeholder[1] : null }}">
                                                <div class="input-group-btn">
                                                    <button type="button" class="btn btn-success legitRipple element_arr add_more_element_hash">
                                                        <i class="icon-add"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-danger legitRipple element_arr remove_more_element_hash">
                                                        <i class="icon-trash"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-primary legitRipple">
                                                        <i class="icon-move"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endif
        </div>
        @if (! empty($description))
            <span class="help-block">{{ $description }}</span>
        @endif
    </div>
</div>