<div class="col-xs-12 col-md-{{ $column[0] }} col-md-offset-{{ $column[1] }} col-md-offset-right-{{ $column[2] }}">
    <div class="form-group">
        @if (! empty($label))
            <label for="{{ (! empty($groupTab) ? $groupTab . '_' : null) . $name }}">
                <strong>{!! $label !!}</strong>
                @if ($require)
                    <span class="text-danger-800">*</span>
                @endif
            </label>
        @endif
        <input id="{{ (! empty($groupTab) ? $groupTab . '_' : null) . $name }}" class="form-control" name="{{ $name . $builder }}"
               value="{{ $value }}" type="url" autocomplete="off" placeholder="{{ $placeholder }}"
               {{ !empty($maxLength) ? 'maxlength=' . $maxLength : null }}>
        @if (! empty($description))
            <span class="help-block">{{ $description }}</span>
        @endif
    </div>
</div>