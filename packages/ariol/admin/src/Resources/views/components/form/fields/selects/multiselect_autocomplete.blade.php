<div class="col-xs-12 col-md-{{ $column[0] }} col-md-offset-{{ $column[1] }} col-md-offset-right-{{ $column[2] }}">
    <div class="form-group">
        @if (! empty($label))
            <label for="{{ (! empty($groupTab) ? $groupTab . '_' : null) . $name }}">
                <strong>{!! $label !!}</strong>
                @if ($require)
                    <span class="text-danger-800">*</span>
                @endif
            </label>
        @endif
        <select class="form-control select-ajax select-remote-data" name="{{ $name . $builder }}[]" multiple
                id="{{ (! empty($groupTab) ? $groupTab . '_' : null) . $name }}" data-url="{{ $url }}"
                {{ !empty($readonly) ? 'disabled' : null }} data-model="{{ $model }}" data-field="{{ $field }}">
            <option value="0"></option>
            @foreach ($values as $key => $value)
                <option value="{{ $key }}" selected>{{ $value }}</option>
            @endforeach
        </select>
        @if (! empty($description))
            <span class="help-block">{{ $description }}</span>
        @endif
    </div>
</div>