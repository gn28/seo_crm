<div class="col-xs-12 col-md-{{ $column[0] }} col-md-offset-{{ $column[1] }} col-md-offset-right-{{ $column[2] }}">
    <div class="form-group">
        @if (! empty($label))
            <label for="{{ (! empty($groupTab) ? $groupTab . '_' : null) . $name }}">
                <strong>{!! $label !!}</strong>
                @if ($require)
                    <span class="text-danger-800">*</span>
                @endif
            </label>
        @endif
        <input class="form-control autoComplete" id="{{ (! empty($groupTab) ? $groupTab . '_' : null) . $name }}"
               name="{{ $name . $builder }}" value="{{ $value }}" autocomplete="off"
               placeholder="{{ $placeholder }}" data-model="{{ $model }}" data-field="{{ $field }}">
        @if (! empty($description))
            <span class="help-block">{{ $description }}</span>
        @endif
    </div>
</div>