<div class="col-xs-12 col-md-{{ $column[0] }} col-md-offset-{{ $column[1] }} col-md-offset-right-{{ $column[2] }}">
    <div class="form-group">
        @if (! empty($label))
            <label for="{{ (! empty($groupTab) ? $groupTab . '_' : null) . $name }}">
                <strong>{!! $label !!}</strong>
                @if ($require)
                    <span class="text-danger-800">*</span>
                @endif
            </label>
        @endif
        @if ($clearable)
        <div class="input-group">
        @endif
        <input id="{{ $name }}" class="form-control" name="{{ $name . $builder }}"
               value="{{ $value }}" autocomplete="off" placeholder="{{ $placeholder }}" {{ !empty($readonly) ? 'readonly' : null }}
               oninput="this.value = this.value.replace(/[^0-9.,]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');"
                {{ !empty($maxLength) ? 'maxlength=' . $maxLength : null }}>
        @if ($clearable)
            <div class="input-group-btn">
                <button type="button" class="btn btn-danger legitRipple remove_input">
                    <i class="icon-trash"></i>
                </button>
            </div>
            </div>
        @endif
        @if (! empty($description))
            <span class="help-block">{{ $description }}</span>
        @endif
    </div>
</div>