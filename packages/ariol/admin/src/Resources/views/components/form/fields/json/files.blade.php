<div class="col-xs-12 col-md-{{ $column[0] }} col-md-offset-{{ $column[1] }} col-md-offset-right-{{ $column[2] }}">
    @if (! empty($label))
        <label for="{{ (! empty($groupTab) ? $groupTab . '_' : null) . $name }}">
            <strong>{!! $label !!}</strong>
            @if ($require)
                <span class="text-danger-800">*</span>
            @endif
        </label>
    @endif
    <input name="{{ $name . $builder }}" type="hidden" value="{{ json_encode($value) }}"
           class="jquery-file multiple-files {{ !empty($builder) ? 'builder-count-block' : null }}">
    @if (! $creatable && empty($value))
        <div class="alert alert-primary alert-bordered m-b-0">
            <span class="text-semibold">
                {{ translate('system.form.packageItems.no-file') }}
            </span>
        </div>
    @endif
    <div class="file-upload-count admin-sortable-files">
        <div class="row fileupload-buttonbar">
            <div class="col-lg-7">
                @if ($creatable)
                    <div class="uploader">
                        <input class="file-styled-primary" name="selectedFilesForUpload[]" type="file" multiple>
                        <span class="action btn bg-blue legitRipple" style="-moz-user-select: none;">
                            <i class="icon-add position-left"></i>
                            {{ translate('system.form.packageItems.add') }}
                        </span>
                    </div>
                @endif
                @if (! empty($description))
                    <span class="help-block">{{ $description }}</span>
                @endif
                <span class="fileupload-process"></span>
            </div>
            <div class="col-lg-5 fileupload-progress fade">
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                    <div class="progress-bar progress-bar-success" style="width: 0;"></div>
                </div>
                <div class="progress-extended"></div>
            </div>
        </div>
        <div class="table-responsive table-sortable">
            <table class="table table-striped table-bordered table-form-margin" role="presentation">
                <tbody class="sortable-files files">
                    @if (! empty($value) && is_array($value))
                        @foreach ($value as $file)
                            @if (file_exists(public_path() . $file))
                                <tr class="template-download fade in">
                                    <td style="width: 10%;">
                                        <?php $contentType = mime_content_type(public_path() . $file); ?>
                                        <a class="image-viewer" download="" title="" href="{{ $file }}">
                                            @if (in_array($contentType, $allowedMimeTypes))
                                                @if ($contentType == 'image/svg+xml' || $contentType == 'image/x-icon')
                                                    <img src="{{ $file }}" style="width: {{ config('ariol.preview-size.width') }}px; height: {{ config('ariol.preview-size.height') }}px">
                                                @else
                                                    <img src="{{ GlideImage::load($file, [
                                                        'h' => config('ariol.preview-size.height'),
                                                        'w' => config('ariol.preview-size.width')
                                                    ]) }}">
                                                @endif
                                            @else
                                                <img src="{{ URL::asset('ariol/assets/images/custom/file.svg') }}" alt="File">
                                            @endif
                                        </a>
                                    </td>
                                    <td style="width: 70%;">
                                        <?php $fileName = explode('/', $file); ?>
                                        <span>{{ end($fileName) }}</span>
                                    </td>
                                    <td style="width: 10%;">
                                        {{ Helper::getFileSize(public_path() . $file) }}
                                    </td>
                                    @if ($destroyable)
                                        <td style="width: 10%;">
                                            <button class="btn btn-danger delete legitRipple" data-url="{{ $file }}" data-type="DELETE">
                                                <i class="icon-trash position-left"></i>
                                                {{ translate('system.form.packageItems.delete') }}
                                            </button>
                                        </td>
                                    @endif
                                </tr>
                            @endif
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>