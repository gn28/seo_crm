<div class="col-xs-12 col-md-{{ $column[0] }} col-md-offset-{{ $column[1] }} col-md-offset-right-{{ $column[2] }}">
    <div class="form-group">
        @if (! empty($label))
            <label for="{{ (! empty($groupTab) ? $groupTab . '_' : null) . $name }}">
                <strong>{!! $label !!}</strong>
                @if ($require)
                    <span class="text-danger-800">*</span>
                @endif
            </label>
        @endif
        <select data-live-search="true" class="form-control @if ($duplicate) duplicate @endif @if (! empty($model)) ajax-select @endif" name="{{ $name . $builder }}"
                @if (! empty($model)) data-ajax-model="{{ $model }}" data-ajax-group="{{ $groupTab }}" @endif
                @if (! empty($relation)) data-ajax-relation="{{ $relation }}" @endif
                @if (! empty($key)) data-ajax-key="{{ $key }}" @endif
                @if (! empty($field)) data-ajax-value="{{ $field }}" @endif
                @if (! empty($language)) data-ajax-language="{{ $language }}" @endif
                id="{{ (! empty($groupTab) ? $groupTab . '_' : null) . $name }}"
                {{ !empty($readonly) ? 'disabled' : null }}>
            @if ($nullable)
                <option {{ ($selected == 0) ? 'selected=selected' : null }}></option>
            @endif
            @foreach ($values as $key => $value)
                <option {{ ($key == $selected) ? 'selected=selected' : null }}
                        value="{{ $key }}">{{ $value }}</option>
            @endforeach
        </select>
        @if (! empty($description))
            <span class="help-block">{{ $description }}</span>
        @endif
    </div>
</div>