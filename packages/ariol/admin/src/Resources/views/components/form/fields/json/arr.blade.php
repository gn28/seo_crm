<div class="col-xs-12 col-md-{{ $column[0] }} col-md-offset-{{ $column[1] }} col-md-offset-right-{{ $column[2] }}">
    <div class="form-group">
        @if (! empty($label))
            <label for="{{ (! empty($groupTab) ? $groupTab . '_' : null) . $name }}">
                <strong>{!! $label !!}</strong>
                @if ($require)
                    <span class="text-danger-800">*</span>
                @endif
            </label>
        @endif
        <div class="array_type_data">
            <div class="sortable_hash">
                @if ($value && is_array($value))
                    @foreach ($value as $key => $item)
                        <div class="type_array">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="input-group">
                                        <input class="form-control {{ !empty($field) && $field == 'phone' ? 'phone-mask' : null }} first {{ !empty($builder) ? 'builder-count-block builder-multiple' : null }}"
                                               name="{{ $name . $builder }}[]" value="{{ $item }}"
                                               autocomplete="off" placeholder="{{ $placeholder }}">
                                        <div class="input-group-btn">
                                            <button type="button" class="btn btn-success legitRipple element_arr add_more_element_hash">
                                                <i class="icon-add"></i>
                                            </button>
                                            <button type="button" class="btn btn-danger legitRipple element_arr remove_more_element_hash">
                                                <i class="icon-trash"></i>
                                            </button>
                                            <button type="button" class="btn btn-primary legitRipple">
                                                <i class="icon-move"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
        @if (! empty($description))
            <span class="help-block">{{ $description }}</span>
        @endif
    </div>
</div>