<img src="{{ URL::asset("languages/{$language['code']}.svg") }}" class="position-left header-language"
     alt="{{ $language['code'] }}">
{{ $language['name'] }}
<span class="caret"></span>