@extends('ariol::layouts.master')

@section('content')
    <div class="row">
        @if (!empty(Auth::user()->role_id) && Auth::user()->role_id == 1)
            <div class="col-md-3">
                <a href="/{{ config('ariol.admin-path') }}/system/cache">
                    <div class="panel panel-body text-center bg-slate-800 has-bg-image">
                        <div class="svg-center position-relative" id="free-space"></div>
                        <h2 class="progress-percentage mt-15 mb-5 text-semibold">{{ $freeSpace }}%</h2>
                        {{ translate('system.modules.packageItems.main-page.packageItems.free-space') }}
                    </div>
                </a>
            </div>
        @endif
        @if (config('ariol.multi-languages'))
                <div class="col-md-3">
                    <a href="/{{ config('ariol.admin-path') }}/system/localization">
                        <div class="panel panel-body text-center bg-indigo-800 has-bg-image">
                            <div class="svg-center position-relative" id="count-languages"></div>
                            <h2 class="progress-percentage mt-15 mb-5 text-semibold">{{ $countLanguages }}</h2>
                            {{ translate('system.modules.packageItems.main-page.packageItems.count-languages') }}
                        </div>
                    </a>
                </div>
        @endif
    </div>
@endsection

@section('js_files')
    <script src="{{ URL::asset('ariol/assets/modules/index.js') }}"></script>
    <script>
        /* Виджет свободного места на диске. */
        progressIcon('#free-space', 42, 2.5, "#37474F", "#fff", 0.{{ $freeSpace }}, "icon-server");

        /* Виджет количества используемых языков. */
        progressIcon('#count-languages', 42, 2.5, "#283593", "#fff", 0.{{ $countLanguages }}, "icon-archive");
    </script>
@endsection