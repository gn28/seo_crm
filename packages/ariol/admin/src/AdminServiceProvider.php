<?php

namespace Ariol\Admin;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Ariol\Commands\ControllerAriolCommand;
use Ariol\Admin\Middleware\OwnerMiddleware;

/**
 * Провайдер пакета админки.
 *
 * @package Ariol\Admin
 */
class AdminServiceProvider extends ServiceProvider
{
    /**
     * Начальная загрузка приложения.
     *
     * @param Router $router
     */
    public function boot(Router $router)
    {
        $this->loadRoutesFrom(__DIR__ . '/routes.php');
        $this->loadMigrationsFrom(__DIR__ . '/Database/migrations');
        $this->loadViewsFrom(__DIR__ . '/Resources/views', 'ariol');
        $this->loadTranslationsFrom(__DIR__ . '/Resources/lang', 'ariol');

        $router->aliasMiddleware('role', OwnerMiddleware::class);

        if ($this->app->runningInConsole()) {
            $this->commands([
                ControllerAriolCommand::class
            ]);
        }

        $this->publishes([
            __DIR__ . '/Config' => base_path() . '/config'
        ], 'ariol-config');

        $this->publishes([
            __DIR__ . '/Resources/lang' => base_path() . '/resources/lang'
        ], 'ariol-lang');

        $this->publishes([
            __DIR__ . '/Models' => base_path() . '/app/Models',
        ], 'ariol-models');

        $this->publishes([
            __DIR__ . '/Public' => public_path()
        ], 'ariol-public');

        $this->publishes([
            __DIR__ . '/Public' => public_path(),
            __DIR__ . '/Config' => base_path() . '/config',
            __DIR__ . '/Models' => base_path() . '/app/Models',
            __DIR__ . '/Resources/lang' => base_path() . '/resources/lang'
        ], 'ariol');
    }

    /**
     * Регистрация приложения.
     *
     * @return void
     */
    public function register() {
        
    }
}
