<?php

namespace config;

use Ariol\Models\Page;
use Ariol\Models\Model;
use Ariol\Models\Users\User;
use Ariol\Models\Users\Role;
use Ariol\Models\Menu as SiteMenu;

/**
 * Класс меню админки.
 *
 * @package config
 */
class Menu extends Model
{
    /**
     * Левое меню админки.
     *
     * @return array
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public static function getMenu()
    {
        return [
            [
                'table' => 'menu',
                'icon' => '<i class="icon-puzzle3 position-left"></i>',
                'name' => translate('system.menu.packageItems.menu'),
                'url' => 'menu',
                'count' => SiteMenu::countWithLang()
            ],
            [
                'table' => 'users',
                'icon' => '<i class="icon-users4 position-left"></i>',
                'name' => translate('system.menu.packageItems.users.packageSubTitle'),
                'sub' => [
                    [
                        'icon' => '<i class="icon-people position-left"></i>',
                        'name' => translate('system.menu.packageItems.users.packageSubTitle'),
                        'url' => 'users',
                        'count' => User::count()
                    ],
                    [
                        'table' => 'roles',
                        'icon' => '<i class="icon-chess-king position-left"></i>',
                        'name' => translate('system.menu.packageItems.users.packageItems.roles'),
                        'url' => 'roles',
                        'count' => Role::count()
                    ]
                ]
            ],
            [
                'table' => 'pages',
                'icon' => '<i class="icon-files-empty2 position-left"></i>',
                'name' => translate('system.menu.packageItems.pages'),
                'url' => 'pages',
                'count' => Page::countWithLang()
            ],
            [
                'icon' => '<i class="icon-cog4 position-left"></i>',
                'name' => translate('system.menu.packageItems.system-settings.packageSubTitle'),
                'sub' => [
                    [
                        'icon' => '<i class="icon-magic-wand position-left"></i>',
                        'name' => translate('system.menu.packageItems.system-settings.packageItems.cache-clear'),
                        'url' => 'system/cache'
                    ],
                    [
                        'icon' => '<i class="icon-archive position-left"></i>',
                        'name' => translate('system.menu.packageItems.system-settings.packageItems.localization'),
                        'url' => 'system/localization'
                    ]
                ]
            ],
        ];
    }
}
