<?php

namespace Ariol\Admin\Controllers;

use Ariol\Classes\Localization;

/**
 * Основной класс админки.
 *
 * @package Ariol\Admin\Controllers
 */
class MainController extends Controller
{
    /**
     * Главная страница.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function index()
    {
        $all = disk_total_space('/');
        $free = disk_free_space('/');

        $allLanguages = count(Localization::getListLanguages());
        $useLanguages = count(Localization::getSelectedLanguages());

        return view('ariol::index')->with([
            'freeSpace' => 100 - round($free / $all * 100),
            'countLanguages' => round($useLanguages / $allLanguages * 100)
        ]);
    }
}
