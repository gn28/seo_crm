<?php

namespace Ariol\Admin\Controllers;

use Auth;
use Illuminate\Http\Request;

/**
 * Класс авторизации в админке.
 *
 * @package Ariol\Admin\Controllers
 */
class AuthController extends Controller
{
    /**
     * Форма авторизации.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getLogin()
    {
        return view('ariol::login');
    }

    /**
     * Авторизация пользователя.
     *
     * @param Request $request
     * @return array
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function postLogin(Request $request)
    {
        if (Auth::attempt([
            'email' => $request->input('email'),
            'password' => $request->input('password')
        ], $request->has('remember'))) {
            if (Auth::user()->role_id == 0) {
                $this->getLogout();

                return ['message' => translate('system.form.packageItems.no-permissions')];
            }

            return ['status' => true];
        }

        return ['message' => translate('system.form.packageItems.field-validate')];
    }

    /**
     * Выход из аккаунта.
     *
     * @return \Illuminate\Routing\Redirector
     */
    public function getLogout()
    {
        Auth::logout();

        return redirect(config('ariol.admin-path') . '/login');
    }
}
