<?php

namespace Ariol\Admin\Controllers;

use Schema;
use Session;
use Illuminate\Http\Request;
use Ariol\Admin\Forms\Verify;
use Ariol\Classes\Localization;

/**
 * Класс для работы с данными таблицы.
 *
 * @package Ariol\Admin\Controllers
 */
class GridController extends Controller
{
    /**
     * Переключатель отображения/активации элемента.
     *
     * @param Request $request
     */
    public function activity(Request $request)
    {
        $model = $request->get('model');

        $model::where('id', $request->get('id'))->update([
            $request->get('updating_field') => $request->get('state')
        ]);
    }

    /**
     * Удаление всех выбранных записей таблицы.
     *
     * @param Request $request
     * @return array
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function deleteItems(Request $request)
    {
        $selected = $request->input('selected');
        if (empty($selected)) {
            return ['error' => translate('system.grid.packageItems.no-selected')];
        }

        $model = $request->input('model');

        $verify = new Verify($model);

        $model = new $model;

        $idsToDelete = array_map(function ($item) use ($model, $verify) {
            return $verify->destroyExists($model, $item) == 0 ? $item : null;
        }, $request->input('selected'));

        $items = $model::whereIn('id', $idsToDelete);
        if (! $items) {
            return ['error' => translate('system.form.packageItems.no-isset')];
        }

        $items->delete();

        return ['success' => translate('system.form.packageItems.destroyed')];
    }

    /**
     * Обновление счётчика пункта меню.
     *
     * @param Request $request
     * @return int
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function updateCountMenuItem(Request $request)
    {
        $model = $request->input('model');
        $model = new $model;

        /* Наименование таблицы. */
        $tableName = with($model)->getTable();

        if ($model->languagable && Schema::hasColumn($tableName, $model->languageField)) {
            $model = $model->where($model->languageField, Localization::getLocale());
        }

        return $model->count();
    }

    /**
     * Сохранение данных прямо в таблице.
     *
     * @param Request $request
     * @return array
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function save(Request $request)
    {
        $id = $request->input('id');
        $value = $request->input('value');
        $field = $request->input('field');
        $model = $request->input('model');

        $model = new $model;

        $entry = $model::where('id', $id)->first();
        if (! $entry) {
            return [
                'type' => 'error',
                'message' => translate('system.form.packageItems.no-isset')
            ];
        }

        $verify = new Verify($model);

        if ($verify->getParam($model->form()[$field], 'unique')) {
            /* Наименование таблицы. */
            $tableName = with($model)->getTable();

            $query = $model::where('id', '!=', $id)->where($field, $value);

            if ($model->languagable && Schema::hasColumn($tableName, $model->languageField)) {
                $query = $query->where($model->languageField, Localization::getLocale());
            }

            if ($query->count() > 0) {
                return [
                    'type' => 'error',
                    'message' => translate('system.form.packageItems.field-validate')
                ];
            }
        }

        if ($verify->getParam($model->form()[$field], 'require') && empty($value)) {
            return [
                'type' => 'error',
                'message' => translate('system.grid.packageItems.no-empty')
            ];
        }

        if (! Schema::hasColumn($model->getTable(), $field)) {
            return [
                'type' => 'error',
                'message' => translate('system.grid.packageItems.no-isset')
            ];
        }

        $model::where('id', $id)->update([
            $field => $value
        ]);

        return [
            'type' => 'success',
            'message' => translate('system.grid.packageItems.success')
        ];
    }

    /**
     * Запоминание текущего номера страницы.
     *
     * @param Request $request
     * @return array
     */
    public function rememberPage(Request $request)
    {
        $page = $request->input('page');

        if (! empty($page)) {
            Session::put('grid_page', $page);
        }

        return ['success' => $page];
    }
}
