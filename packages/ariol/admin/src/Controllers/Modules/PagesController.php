<?php

namespace Ariol\Admin\Controllers\Modules;

use Ariol\Admin\Controllers\Forms\BaseController;

/**
 * Класс страниц
 *
 * @package Ariol\Admin\Controllers\Modules
 */
class PagesController extends BaseController
{
    /**
     * Используемая модель.
     *
     * @var string
     */
    protected $model = 'Ariol\Models\Page';
}
