<?php

namespace Ariol\Admin\Controllers\Modules;

use Ariol\Admin\Controllers\Forms\BaseController;

/**
 * Класс пользователей.
 *
 * @package Ariol\Admin\Controllers\Modules
 */
class UsersController extends BaseController
{
    /**
     * Используемая модель.
     *
     * @var string
     */
    protected $model = 'Ariol\Models\Users\User';
}
