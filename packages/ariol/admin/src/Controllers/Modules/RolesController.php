<?php

namespace Ariol\Admin\Controllers\Modules;

use DB;
use Schema;
use Illuminate\Http\Request;
use Ariol\Admin\Controllers\Forms\BaseController;

/**
 * Класс ролей пользователей.
 *
 * @package Ariol\Admin\Controllers\Modules
 */
class RolesController extends BaseController
{
    /**
     * Используемая модель.
     *
     * @var string
     */
    protected $model = 'Ariol\Models\Users\Role';

    /**
     * Подгрузка данных в multiselect.
     *
     * @param Request $request
     * @return mixed
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function select(Request $request)
    {
        $key = $request->input('key');
        $table = $request->input('table');
        $value = $request->input('value');
        $mainId = $request->input('mainId');
        $relationModel = $request->input('relationModel');

        $entries = [];

        if (Schema::hasColumn($table, $key)) {
            $fields = explode('|', $value);

            foreach ($fields as $field) {
                if (Schema::hasColumn($table, $field)) {
                    $value = $field;

                    break;
                }
            }

            $entries = DB::table($table)->select($key, $value)->orderBy($value)->get();
        }

        $relationData = '';

        if (! empty($relationModel) && !empty($mainId)) {
            $relationData = $relationModel::select('entries')
                ->where('role_id', $mainId)->where('table', $table)->first();
        }

        $content = '';
        $entriesIds = [];

        if (count($entries) == 0) {
            $content = '<option value="0">' . translate('system.form.packageItems.no-selected') . '</option>';
        }

        if (! empty($relationData)) {
            $entriesIds = !empty($relationData->entries) ? json_decode($relationData->entries, true) : [];
        }

        foreach ($entries as $entry) {
            $selected = !empty($entriesIds) && in_array($entry->{$key}, $entriesIds) ? 'selected' : null;

            $content .= '<option value="' . $entry->{$key} . '" ' . $selected . '>' . $entry->{$value} . '</option>';
        }

        return $content;
    }
}
