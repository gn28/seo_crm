<?php

namespace Ariol\Admin\Controllers\Modules;

use Ariol\Admin\Controllers\Forms\BaseController;

/**
 * Класс меню.
 *
 * @package Ariol\Admin\Controllers\Modules
 */
class MenuController extends BaseController
{
    /**
     * Используемая модель.
     *
     * @var string
     */
    protected $model = 'Ariol\Models\Menu';
}
