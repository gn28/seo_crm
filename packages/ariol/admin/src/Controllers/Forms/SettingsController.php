<?php

namespace Ariol\Admin\Controllers\Forms;

use Auth;
use Route;
use Schema;
use Ariol\Classes\Helper;
use Illuminate\Http\Request;
use Ariol\Admin\Forms\Verify;
use Ariol\Classes\Localization;
use Ariol\Admin\Forms\Construct;
use Ariol\Admin\Controllers\Controller;

/**
 * Класс настроек в админке.
 *
 * @package Ariol\Admin\Controllers\Forms
 */
class SettingsController extends Controller
{
    /**
     * Модель верификации данных.
     *
     * @var Verify
     */
    protected $verify;

    /**
     * BaseController constructor.
     *
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function __construct()
    {
        parent::__construct();

        if (! empty($this->model)) {
            $this->verify = new Verify($this->model);
        }
    }

    /**
     * Страница с вкладками.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Throwable
     */
    public function index()
    {
        if (empty($this->model)) {
            abort(503);
        }

        $model = new $this->model;

        if (Auth::user()->role_id != 1) {
            $tableName = with($model)->getTable();

            if (! Auth::user()->role->hasModule($tableName) || empty(Auth::user()->role)) {
                abort(404);
            } else {
                if ($model->editable) {
                    $model->editable = Auth::user()->role->haveAction($tableName, 'editable', $model->editable);
                }
            }
        }

        $form = new Construct;

        $data = [
            'form' => $form->settingsGenerate($model, [
                'mainPath' => '/' . Route::current()->uri()
            ])
        ];

        return view('ariol::components.settings.index', $data);
    }

    /**
     * Обновление данных.
     *
     * @param Request $request
     * @return array
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function update(Request $request)
    {
        if (empty($this->model)) {
            abort(503);
        }

        $model = new $this->model;

        $fieldsForSave = [];

        $group = $request->input('group_tab');
        $form = $model->form()[$group];

        $requestData =  $request->except('_token', 'group_tab', 'selectedFilesForUpload');

        $checkFields = $this->verify->checkRequireFields($form, $requestData);
        if (count($checkFields) > 0) {
            return [
                'checkFields' => $checkFields,
                'message' => translate('system.form.packageItems.field-require')
            ];
        }

        $checkFields = $this->verify->validateFields($form, $requestData);
        if (count($checkFields) > 0) {
            return [
                'checkFields' => $checkFields,
                'message' => translate('system.form.packageItems.field-validate')
            ];
        }

        /* Поля, которые задействованы в конструкторе. */
        $builderFields = [];

        foreach ($form as $nameField => $formFieldArr) {
            if ($formFieldArr['type'] == 'boolean') {
                if (! array_key_exists($nameField, $requestData)) {
                    $fieldsForSave[$nameField] = 0;
                }
            }
        }

        foreach ($requestData as $field => $value) {
            $realField = str_replace($group . '_', '', $field);

            if (! isset($form[$field])) {
                foreach ($form as $fieldName => $fields) {
                    if (isset($fields['fields'][$realField])) {
                        $form[$field] = $fields['fields'][$realField];

                        if (! $this->verify->getParam($fields['fields'][$realField], 'ignore')
                            && !$this->verify->getParam($fields['fields'][$realField], 'readonly')) {
                            //$builderFields[$fieldName][$realField] = $requestData[$group . '_' . $realField];
                            $builderFields[$fieldName][$realField] = [];

                            foreach ($requestData[$group . '_' . $realField] as $fieldItem) {
                                $builderFields[$fieldName][$realField][] = $this->verify->{$form[$field]['type']}(
                                    $form,
                                    $field,
                                    $fieldItem,
                                    $group
                                );
                            }
                        }
                    }
                }
            } else {
                if ($form[$field]['type'] != 'file' && $form[$field]['type'] != 'files') {
                    $validateField = $this->verify->{$form[$field]['type']}($form, $field, $value, $group);

                    if ((isset($validateField) || is_null($validateField)) && $validateField !== false) {
                        $fieldsForSave[$realField] = $validateField;
                    } elseif (isset($validateField['checkFields'])) {
                        return $validateField['checkFields'];
                    }
                }
            }
        }

        foreach ($builderFields as $fieldName => $fields) {
            $fieldsForSave[$fieldName] = json_encode($fields);
        }

        /* Наименование таблицы. */
        $tableName = with($model)->getTable();

        foreach ($fieldsForSave as $key => $value) {
            $query = $model::where('group', $group)->where('key', $key);

            if ($model->languagable && Schema::hasColumn($tableName, $model->languageField)) {
                $query = $query->where($model->languageField, Localization::getAdminLocale());
            }

            if ($query->count() > 0) {
                $query->update([
                    'value' => $value
                ]);
            } else {
                $data = [
                    'group' => $group,
                    'key' => $key,
                    'value' => $value
                ];

                if ($model->languagable && Schema::hasColumn($tableName, $model->languageField)) {
                    $data[$model->languageField] = Localization::getAdminLocale();
                }

                $model::create($data);
            }
        }

        $modelName = str_replace('app\http\models\\', '', mb_strtolower($this->model));
        $modelName = str_replace('ariol\models\\', '', $modelName);
        $modelName = str_replace('\\', '/', $modelName);

        foreach ($requestData as $field => $value) {
            $realField = str_replace($group . '_', '', $field);

            $query = $model::where('group', $group)->where('key', $realField);

            if ($model->languagable && Schema::hasColumn($tableName, $model->languageField)) {
                $query = $query->where($model->languageField, Localization::getAdminLocale());
            }

            if ($form[$field]['type'] == 'file') {
                if (! file_exists(public_path() . '/files/' . $modelName)) {
                    mkdir(public_path() . '/files/' . $modelName, 0777, true);
                }

                $modelField = $query->first();

                if (! $value) {
                    if (! empty($modelField->value) && file_exists(public_path() . $modelField->value)) {
                        unlink(public_path() . $modelField->value);
                    }

                    $query->update([
                        'value' => ''
                    ]);

                    continue;
                }

                if (is_array($value)) {
                    foreach ($value as $file) {
                        if (! file_exists(public_path() . urldecode($file))
                            || is_dir(public_path() . urldecode($file))) {
                            continue;
                        }

                        if (preg_match('%\/temp\/%', $file)) {
                            $fileParts = explode('/', urldecode($file));

                            $prefix = $group. '-';

                            if (config('ariol.multi-languages')) {
                                $prefix .= Localization::getAdminLocale() . '-';
                            }

                            $filename = $prefix . Helper::getWritableFileName(end($fileParts));

                            if ($modelField && '/files/' . $modelName . '/' . $filename != $modelField->value) {
                                if (! empty($modelField->value) && file_exists(public_path() . $modelField->value)) {
                                    unlink(public_path() . $modelField->value);
                                }
                            }

                            rename(
                                public_path() . urldecode($file),
                                public_path() . '/files/' . $modelName . '/' . $filename
                            );

                            $thumbnail = str_replace('/temp/', '/temp/thumbnail/', urldecode($file));
                            if (! empty($thumbnail) && file_exists(public_path() . $thumbnail)) {
                                unlink(public_path() . $thumbnail);
                            }

                            if ($query->count() > 0) {
                                $query->update([
                                    'value' => '/files/' . $modelName . '/' . $filename
                                ]);
                            } else {
                                $data = [
                                    'group' => $group,
                                    'key' => $realField,
                                    'value' => '/files/' . $modelName . '/' . $filename
                                ];

                                if ($model->languagable && Schema::hasColumn($tableName, $model->languageField)) {
                                    $data[$model->languageField] = Localization::getAdminLocale();
                                }

                                $model::create($data);
                            }
                        }
                    }
                } else {
                    if (! file_exists(public_path() . urldecode($value)) || is_dir(public_path() . urldecode($value))) {
                        continue;
                    }

                    if (preg_match('%\/temp\/%', $value)) {
                        $fileParts = explode('/', urldecode($value));

                        $prefix = $group. '-';

                        if (config('ariol.multi-languages')) {
                            $prefix .= Localization::getAdminLocale() . '-';
                        }

                        $filename = $prefix . Helper::getWritableFileName(end($fileParts));

                        if ($modelField && '/files/' . $modelName . '/' . $filename != $modelField->value) {
                            if (! empty($modelField->value) && file_exists(public_path() . $modelField->value)) {
                                unlink(public_path() . $modelField->value);
                            }
                        }

                        rename(
                            public_path() . urldecode($value),
                            public_path() . '/files/' . $modelName . '/' . $filename
                        );

                        $thumbnail = str_replace('/temp/', '/temp/thumbnail/', urldecode($value));
                        if (! empty($thumbnail) && file_exists(public_path() . $thumbnail)) {
                            unlink(public_path() . $thumbnail);
                        }

                        if ($query->count() > 0) {
                            $query->update([
                                'value' => '/files/' . $modelName . '/' . $filename
                            ]);
                        } else {
                            $data = [
                                'group' => $group,
                                'key' => $realField,
                                'value' => '/files/' . $modelName . '/' . $filename
                            ];

                            if ($model->languagable && Schema::hasColumn($tableName, $model->languageField)) {
                                $data[$model->languageField] = Localization::getAdminLocale();
                            }

                            $model::create($data);
                        }
                    }
                }
            }

            if ($form[$field]['type'] == 'files') {
                if (is_array($value)) {
                    foreach ($value as $valueFile) {
                        if (! file_exists(public_path() . '/files/' . $modelName)) {
                            mkdir(public_path() . '/files/' . $modelName, 0777, true);
                        }

                        $empty = true;
                        $files = explode(',', $valueFile);

                        foreach ($files as $file) {
                            $empty = $file ? false : true;
                        }

                        $modelField = $query->first();

                        if ($empty) {
                            if (! empty($modelField->value) && file_exists(public_path() . $modelField->value)) {
                                unlink(public_path() . $modelField->value);
                            }

                            $query->update([
                                'value' => ''
                            ]);

                            continue;
                        }

                        $rowValues = [];
                        $itemFiles = !empty($modelField->value) ? json_decode($modelField->value, true) : null;

                        if (is_array($itemFiles)) {
                            foreach ($itemFiles as $item) {
                                if (! in_array($item, $files)) {
                                    if (! empty($item) && file_exists(public_path() . $item)) {
                                        unlink(public_path() . $item);
                                    }
                                }
                            }
                        }

                        foreach ($files as $file) {
                            if (! file_exists(public_path() . urldecode($file))
                                || is_dir(public_path() . urldecode($file))) {
                                continue;
                            }

                            if (preg_match('%\/temp\/%', $file)) {
                                $fileParts = explode('/', urldecode($file));

                                $prefix = $group. '-';

                                if (config('ariol.multi-languages')) {
                                    $prefix .= Localization::getAdminLocale() . '-';
                                }

                                $filename = $prefix . Helper::getWritableFileName(end($fileParts));

                                rename(
                                    public_path() . urldecode($file),
                                    public_path() . '/files/' . $modelName . '/' . $filename
                                );

                                $thumbnail = str_replace('/temp/', '/temp/thumbnail/', urldecode($file));
                                if (! empty($thumbnail) && file_exists(public_path() . $thumbnail)) {
                                    unlink(public_path() . $thumbnail);
                                }

                                $rowValues[] = '/files/' . $modelName . '/' . $filename;
                            } elseif ($file) {
                                $rowValues[] = $file;
                            }
                        }

                        if ($rowValues) {
                            if ($query->count() > 0) {
                                $query->update([
                                    'value' => json_encode($rowValues)
                                ]);
                            } else {
                                $data = [
                                    'group' => $group,
                                    'key' => $realField,
                                    'value' => json_encode($rowValues)
                                ];

                                if ($model->languagable && Schema::hasColumn($tableName, $model->languageField)) {
                                    $data[$model->languageField] = Localization::getAdminLocale();
                                }

                                $model::create($data);
                            }
                        }
                    }
                } else {
                    if (! file_exists(public_path() . '/files/' . $modelName)) {
                        mkdir(public_path() . '/files/' . $modelName, 0777, true);
                    }

                    $empty = true;
                    $files = explode(',', (string) $value);

                    foreach ($files as $file) {
                        $empty = $file ? false : true;
                    }

                    $modelField = $query->first();

                    if ($empty) {
                        if (! empty($modelField->value) && file_exists(public_path() . $modelField->value)) {
                            unlink(public_path() . $modelField->value);
                        }

                        $query->update([
                            'value' => ''
                        ]);

                        continue;
                    }

                    $rowValues = [];
                    $itemFiles = !empty($modelField->value) ? json_decode($modelField->value, true) : null;

                    if (is_array($itemFiles)) {
                        foreach ($itemFiles as $item) {
                            if (! in_array($item, $files)) {
                                if (! empty($item) && file_exists(public_path() . $item)) {
                                    unlink(public_path() . $item);
                                }
                            }
                        }
                    }

                    foreach ($files as $file) {
                        if (! file_exists(public_path() . urldecode($file))
                            || is_dir(public_path() . urldecode($file))) {
                            continue;
                        }

                        if (preg_match('%\/temp\/%', $file)) {
                            $fileParts = explode('/', urldecode($file));

                            $prefix = $group. '-';

                            if (config('ariol.multi-languages')) {
                                $prefix .= Localization::getAdminLocale() . '-';
                            }

                            $filename = $prefix . Helper::getWritableFileName(end($fileParts));

                            rename(
                                public_path() . urldecode($file),
                                public_path() . '/files/' . $modelName . '/' . $filename
                            );

                            $thumbnail = str_replace('/temp/', '/temp/thumbnail/', urldecode($file));
                            if (! empty($thumbnail) && file_exists(public_path() . $thumbnail)) {
                                unlink(public_path() . $thumbnail);
                            }

                            $rowValues[] = '/files/' . $modelName . '/' . $filename;
                        } elseif ($file) {
                            $rowValues[] = $file;
                        }
                    }

                    if ($rowValues) {
                        if ($query->count() > 0) {
                            $query->update([
                                'value' => json_encode($rowValues)
                            ]);
                        } else {
                            $data = [
                                'group' => $group,
                                'key' => $realField,
                                'value' => json_encode($rowValues)
                            ];

                            if ($model->languagable && Schema::hasColumn($tableName, $model->languageField)) {
                                $data[$model->languageField] = Localization::getAdminLocale();
                            }

                            $model::create($data);
                        }
                    }
                }
            }
        }

        return ['message' => translate('system.form.packageItems.success')];
    }
}
