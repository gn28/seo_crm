<?php

namespace Ariol\Admin\Controllers\Forms;

use Auth;
use Form;
use Route;
use Schema;
use Session;
use ReflectionClass;
use Illuminate\Http\Request;
use Ariol\Admin\Forms\Verify;
use Ariol\Admin\Forms\Filters;
use Ariol\Classes\Localization;
use Ariol\Admin\Forms\Construct;
use Ariol\Admin\Controllers\Controller;

/**
 * Класс формы с вкладками в админке.
 *
 * @package Ariol\Admin\Controllers\Forms\Forms
 */
class BaseController extends Controller
{
    /**
     * Модель верификации данных.
     *
     * @var Verify
     */
    protected $verify;

    /**
     * Модель дополнительных фильтров для таблицы данных.
     *
     * @var Verify
     */
    protected $filters;

    /**
     * BaseController constructor.
     *
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function __construct()
    {
        parent::__construct();

        if (! empty($this->model)) {
            $this->verify = new Verify($this->model);
            $this->filters = new Filters();
        }
    }

    /**
     * Страница с данными в таблице.
     *
     * @param null $parentId
     * @param null $parentField
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function index($parentId = null, $parentField = null)
    {
        if (empty($this->model)) {
            abort(503);
        }

        $model = new $this->model;

        if (Auth::user()->role_id != 1) {
            $tableName = with($model)->getTable();

            if ((! empty(Auth::user()->role) && ! Auth::user()->role->hasModule($tableName)) || empty(Auth::user()->role)) {
                abort(404);
            } else {
                $actions = ['creatable', 'readonly', 'editable', 'destroyable'];

                foreach ($actions as $action) {
                    if ($model->{$action}) {
                        $model->{$action} = Auth::user()->role->haveAction($tableName, $action, $model->{$action});
                    } elseif (! $model->{$action} && $action == 'readonly') {
                        $model->{$action} = Auth::user()->role->haveAction($tableName, 'readonly', $model->readonly);
                    }
                }

                if ($model->editable) {
                    $model->readonly = false;
                }
            }
        }

        $labels = $model->labels();
        $columns = $model->columns();

        /* Преобразование стандартного указания лэйблов в формат для вкладок. */
        if (! multi_array_key_exists($labels, 'content', true)) {
            $labels = [
                'tab' => [
                    'title' => translate('system.form.packageItems.tab'),
                    'content' => $labels
                ]
            ];
        }

        $fields = [];

        foreach ($columns as $field => $value) {
            $field = is_numeric($field) ? $value : $field;
            $width = !empty($value['width']) ? $value['width'] : 0;
            $align = !empty($value['align']) ? $value['align'] : 'left';

            $firstKey = key($labels);

            if (! empty($labels[$firstKey]['content'][$field])) {
                $field = $labels[$firstKey]['content'][$field];
            }

            $field = !empty($value['title']) ? $value['title'] : $field;

            $fields[] = $this->getField($width, $align, $field, array_get($value, 'type'));
        }

        $getField = $this->getField(config('ariol.min-width-column-table'));

        if (empty($model->readonly)) {
            if (method_exists($model, 'children') && !empty($model->children())) {
                foreach ($model->children() as $child) {
                    $fields[] = $this->getField(
                        config('ariol.min-width-column-table'),
                        'center',
                        $labels[key($labels)]['content'][$child]
                    );
                }
            }

            if (! empty($model->editable)) {
                $fields[] = $getField;
            }
        } else {
            $fields[] = $getField;
        }

        if (! empty($model->destroyable)) {
            $fields[] = $getField;
        }

        $routeLink = '/' . Route::current()->uri();

        if (! empty($parentId) && !empty($parentField)) {
            if ($model->where('id', $parentId)->count() == 0) {
                abort(404);
            }

            $routeLink = str_replace(['{parentId?}', '{parentField?}'], [$parentId, $parentField], $routeLink);
        } else {
            $routeLink = str_replace(['/{parentId?}', '/{parentField?}'], '', $routeLink);
        }

        $breadcrumbs = method_exists($model, 'breadcrumbs') && !empty($model->breadcrumbs())
            ? $model->breadcrumbs()
            : [];

        $filtersContent = $label = '';

        if (method_exists($model, 'filters') && !empty($model->filters())) {
            foreach ($model->filters() as $field => $filter) {
                $firstKey = key($labels);

                if (! empty($labels[$firstKey]['content'][$field])) {
                    $label = $labels[$firstKey]['content'][$field];
                }

                $label = !empty($filter['title']) ? $filter['title'] : $label;

                $filters = new Filters($label, $field, $filter);
                $filtersContent .= $filters->{$filter['type']}();
            }
        }

        $reflection = new ReflectionClass($model);

        if ($reflection->name != Session::get('sessionModel')) {
            Session::put('grid_page', 1);
        }

        $gridPage = !empty(Session::get('grid_page')) ? Session::get('grid_page') : 1;
        $search = !empty(Session::get('search-' . $this->model)) ? Session::get('search-' . $this->model) : null;

        $data = [
            'fields' => $fields,
            'search' => $search,
            'model' => $this->model,
            'gridPage' => $gridPage,
            'breadcrumbs' => $breadcrumbs,
            'readonly' => $model->readonly,
            'editable' => $model->editable,
            'creatable' => $model->creatable,
            'dataUrl' => $routeLink . '/data',
            'filtersContent' => $filtersContent,
            'destroyable' => $model->destroyable,
            'createUrl' => $routeLink . '/create',
            'unSortableFields' => $this->getUnSortableFields($model)
        ];

        return view('ariol::components.grid.index', $data);
    }

    /**
     * Подгрузка данных в таблицу.
     *
     * @param null $parentId
     * @param null $parentField
     * @param Request $request
     * @return array
     * @throws \Throwable
     */
    public function data(Request $request, $parentId = null, $parentField = null)
    {
        if (empty($this->model)) {
            abort(503);
        }

        /* Запись в сессию текущей модели, чтобы на другой странице удалить сессию сохранённой страницы. */
        Session::put('sessionModel', $request->get('sessionModel'));

        $model = new $this->model;
        $columns = $model->columns();

        /* Получение данных своим способом или стандартно из модели. */
        $items = method_exists($model, 'data') && !empty($model->data()) ? $model->data() : $model;

        if (! empty($parentId) && !empty($parentField) && $parentId != 'create') {
            $items = $items->where($parentField, $parentId);
        }

        /* Обработка дополнительных фильтров. */
        if ($request->has('filters')) {
            foreach ($request->get('filters') as $field => $value) {
                if ($value != 'no-selected') {
                    $params = $model->filters()[$field];

                    $nameFilter = 'get' . ucfirst($params['type']);
                    $items = $this->filters->{$nameFilter}($items, $field, $value);
                }
            }
        }

        $length = $request->input('length');

        /* Наименование таблицы. */
        $tableName = with($model)->getTable();

        /* Данные из поля поиска. */
        $searchValue = $request->input('search')['value'];
        Session::put('search-' . $this->model, $searchValue);

        /* По умолчанию поиск работает с полями name и title, если они существуют в таблице. */
        $fieldName = Schema::hasColumn($tableName, 'name') ? 'name' : null;
        $fieldName = Schema::hasColumn($tableName, 'title') ? 'title' : $fieldName;

        /* Если в модели есть метод со списком полей, по которым осуществляется поиск, то ищем в них. */
        $searchableFields = method_exists($model, 'searchableFields') ? $model->searchableFields() : [$fieldName];
        if (! empty($searchableFields)) {
            if (! empty($searchValue)) {
                $items = $items->where(function ($query) use ($searchableFields, $searchValue, $tableName) {
                    foreach ($searchableFields as $field) {
                        if (Schema::hasColumn($tableName, $field)) {
                            $query->orWhere($field, 'LIKE', '%' . $searchValue . '%');
                        }
                    }
                });
            }
        } else {
            /* Если в таблице существует поле name или title, то совершаем поиск по ним. */
            if (! empty($fieldName)) {
                $items = $items->where($fieldName, $searchValue);
            }
        }

        if ($model->languagable) {
            $items = $items->where($tableName . '.' . $model->languageField, Localization::getAdminLocale());
        }

        if (Auth::user()->role_id != 1 && !empty(Auth::user()->role)) {
            if (Auth::user()->role->hasModule($tableName)) {
                $moduleIds = Auth::user()->role->getModuleIdsAccess($tableName);

                if (! empty($moduleIds) && !empty($moduleIds[0])) {
                    $items = $items->whereIn('id', $moduleIds);
                }
            }
        }

        /* Количество данных. */
        $count = $items->count();

        /* Если выбран вывод не всех данных, то выводим указанное количество. */
        if ($length != '-1') {
            $items = $items->take($length)->skip($request->input('start'));
        }

        $sortableFields = method_exists($model, 'sortableFields') ? $model->sortableFields() : [];

        /* Сортировка данных по колонкам. */
        $orderByColumnNumber = $request->input('order')[0]['column'];
        if ($orderByColumnNumber > 0) {
            $orderByColumnName = array_keys($columns)[$orderByColumnNumber - 1];
            $orderByColumnName = is_numeric($orderByColumnName) ? $columns[$orderByColumnName] : $orderByColumnName;

            $sortMethod = $request->input('order')[0]['dir'];

            if (isset($sortableFields[$orderByColumnName]) && is_array($sortableFields[$orderByColumnName])) {
                $searchModel = $sortableFields[$orderByColumnName]['model'];
                $searchField = $sortableFields[$orderByColumnName]['field'];

                $searchModel = new $searchModel;
                $searchTable = $searchModel->getTable();

                $items->join($searchTable, $searchTable . '.id', '=', $tableName . '.' . $orderByColumnName)
                    ->orderBy($searchTable . '.' . $searchField, $sortMethod)->select($tableName . '.*');
            } else {
                $items = $items->orderBy($orderByColumnName, $sortMethod);
            }
        } else {
            $sorting = method_exists($model, 'sorting') && !empty($model->sorting()) ? $model->sorting() : null;

            if (! empty($sorting) && is_array($sorting)) {
                $items = $items->orderBy($sorting[0], !empty($sorting[1]) ? $sorting[1] : 'asc');
            } else {
                $items = $items->orderBy('id', 'desc');
            }
        }

        $jsonData = [
            'aaData' => [],
            'iTotalDisplayRecords' => $count,
            'sEcho' => $request->input('draw'),
            'iTotalRecords' => count($items->get())
        ];

        foreach ($items->get() as $index => $item) {
            /* Проверка на возможность удаления записей. */
            $destroyIf = $this->verify->destroyExists($model, $item->id);

            if (Auth::user()->role_id != 1) {
                $tableName = with($model)->getTable();

                if (! Auth::user()->role->hasModule($tableName) || empty(Auth::user()->role)) {
                    abort(404);
                } else {
                    $actions = ['creatable', 'readonly', 'editable', 'destroyable'];

                    foreach ($actions as $action) {
                        if ($model->{$action}) {
                            $model->{$action} = Auth::user()->role->haveAction($tableName, $action, $model->{$action});
                        } elseif (! $model->{$action} && $action == 'readonly') {
                            $model->{$action} = Auth::user()->role->haveAction($tableName, 'readonly', $model->readonly);
                        }
                    }

                    if ($model->editable) {
                        $model->readonly = false;
                    }
                }
            }

            /* Выводим checkbox с выбором удаляемой записи только в том случае,
               если разрешено удаление записей. */
            if (! empty($model->destroyable)) {
                $jsonData['aaData'][$index][0] = view('ariol::components.grid.modifications.checkbox', [
                    'id' => $item->id,
                    'disabled' => $destroyIf == 0 ? false : true
                ])->render();
            }

            foreach (array_keys($columns) as $field) {
                $gridValue = '';

                /* Если просто перечислено поле, то ищем его по ключу в массиве колонок. */
                $fieldName = is_numeric($field) ? $columns[$field] : $field;

                /* Если выводимой ячейке присвоен тип, то обрабатываем его. */
                if (! empty($columns[$field]['type'])) {
                    /* Редактирование поля прямо в таблице данных. */
                    if ($columns[$field]['type'] == 'editable') {
                        if (! empty($columns[$field]['data']['variant'])
                            && $columns[$field]['data']['variant'] == 'select') {
                            $values = !empty($columns[$field]['data']['values'])
                                ? $columns[$field]['data']['values']
                                : [];

                            $gridValue = view('ariol::components.grid.modifications.select', [
                                'id' => $item->id,
                                'values' => $values,
                                'field' => $fieldName,
                                'selected' => $item->{$field}
                            ])->render();
                        } else {
                            $gridValue = view('ariol::components.grid.modifications.contenteditable', [
                                'id' => $item->id,
                                'field' => $fieldName,
                                'value' => $item->{$field}
                            ])->render();
                        }
                    }

                    /* Вывод кнопки с указанной ссылкой. */
                    if ($columns[$field]['type'] == 'link') {
                        $currentUri = Route::current()->uri();

                        $alias = !empty($columns[$field]['alias']) ? $columns[$field]['alias'] : null;
                        $url = '/' . str_replace('data', $columns[$field]['url'] . '/' . $item->id, $currentUri);

                        $gridValue = view('ariol::components.grid.buttons.custom', [
                            'url' => $url,
                            'id' => $item->id,
                            'alias' => $alias,
                            'icon' => $columns[$field]['icon'],
                            'color' => $columns[$field]['color'] ?? 'primary'
                        ])->render();
                    }

                    /* Переключатель состояния (true, false). */
                    if ($columns[$field]['type'] == 'activity') {
                        $checkboxHtml = Form::checkbox($field, $item->{$field}, $item->{$field}, [
                            'data-id' => $item->id,
                            'class' => 'list-activity',
                            'data-model' => $this->model,
                            'data-updating-field' => $fieldName,
                            'data-color' => $columns[$field]['color'] ?? 'primary'
                        ]);

                        $gridValue = $checkboxHtml;
                    }

                    /* Использование своего вывода данных в колонке. */
                    if ($columns[$field]['type'] == 'template') {
                        $gridValue = $item->{$columns[$field]['method']}();
                    }
                } else {
                    $gridValue = $item->{$fieldName};
                }

                $jsonData['aaData'][$index][] = (string) $gridValue;
            }

            $routeLink = Route::current()->uri();

            if (! empty($parentId) && !empty($parentField)) {
                $routeLink = str_replace(['{parentId?}', '{parentField?}'], [$parentId, $parentField], $routeLink);
            } else {
                $routeLink = str_replace('/{parentId?}/{parentField?}', '', $routeLink);
            }

            /* Если отключён режим только просмотра записей, то проверяем на возможность редактирования,
               в ином случае - выводим кнпоку на просмотр записи. */
            if (empty($model->readonly)) {
                /* Если разрешено редактирование данных и указано добавление записей на отдельной странице,
                   то выводим соответствующую кнопку. */
                if (method_exists($model, 'children') && !empty($model->children())) {
                    foreach ($model->children() as $child) {
                        $link = '/' . str_replace('data', $item->id . '/' . $child, Route::current()->uri());
                        $link = str_replace('/{parentId?}/{parentField?}', '', $link);

                        $parentField = empty($parentField) ? $model->children()[0] : $parentField;

                        $jsonData['aaData'][$index][] = view('ariol::components.grid.buttons.children', [
                            'link' => $link,
                            'count' => $model->where($parentField, $item->id)->count()
                        ])->render();
                    }
                }

                /* Если разрешено редактирование данных, то выводим соответствующую кнопку. */
                if (! empty($model->editable)) {
                    $link = '/' . str_replace('data', 'edit/' . $item->id, $routeLink);

                    $jsonData['aaData'][$index][] = view('ariol::components.grid.buttons.edit', [
                        'link' => $link
                    ])->render();
                }
            } else {
                $link = '/' . str_replace('data', 'edit/' . $item->id, $routeLink);

                $jsonData['aaData'][$index][] = view('ariol::components.grid.buttons.readonly', [
                    'link' => $link
                ])->render();
            }

            /* Если разрешено удаление данных, то выводим соответствующую кнопку. */
            if (! empty($model->destroyable)) {
                $link = '/' . str_replace('data', 'delete/' . $item->id, Route::current()->uri());
                $link = str_replace('/{parentId?}/{parentField?}', '', $link);

                $jsonData['aaData'][$index][] = view('ariol::components.grid.buttons.delete', [
                    'link' => $link,
                    'disabled' => $destroyIf == 0 ? false : true
                ])->render();
            }
        }

        return $jsonData;
    }

    /**
     * Форма создания.
     *
     * @param null $parentId
     * @param null $parentField
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @throws \Throwable
     */
    public function create($parentId = null, $parentField = null)
    {
        if (empty($this->model)) {
            abort(503);
        }

        $model = new $this->model;

        if (Auth::user()->role_id != 1 && !empty(Auth::user()->role)) {
            $tableName = with($model)->getTable();

            if (! Auth::user()->role->hasModule($tableName)
                || !Auth::user()->role->haveAction($tableName, 'creatable', $model->creatable)) {
                abort(404);
            }
        }

        $formPath = Route::current()->uri();
        $routeLink = '/' . str_replace('/create', '', Route::current()->uri());

        if (! empty($parentId) && !empty($parentField)) {
            $routeLink = str_replace(['{parentId?}', '{parentField?}'], [$parentId, $parentField], $routeLink);
            $formPath = str_replace(['{parentId?}', '{parentField?}'], [$parentId, $parentField], $formPath);
        }

        /* Если включён режим только просмотра или запрещено удаление, то перенаправляем в таблицу данных. */
        if (! empty($model->readonly) || empty($model->creatable)) {
            return redirect($routeLink);
        }

        $form = new Construct;
        $breadcrumbs = method_exists($model, 'breadcrumbs') && !empty($model->breadcrumbs())
            ? $model->breadcrumbs()
            : [];

        $data = [
            'form' => $form->tabsGenerate($model, [
                'mainPath' => $routeLink,
                'formPath' => '/' . $formPath
            ]),
            'title' => translate('system.form.packageItems.creating'),
            'breadcrumbs' => $breadcrumbs
        ];

        return view('ariol::components.tabs.index', $data);
    }

    /**
     * Форма редактирования.
     *
     * @param null $parentCurrentId
     * @param null $parentCurrentField
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @throws \Throwable
     */
    public function edit($id, $parentCurrentId = null, $parentCurrentField = null)
    {
        if (empty($this->model)) {
            abort(503);
        }

        $model = new $this->model;
        $tableName = with($model)->getTable();

        if (Auth::user()->role_id != 1 && !empty(Auth::user()->role)) {
            $editable = Auth::user()->role->haveAction($tableName, 'editable', $model->editable);
            $readonly = Auth::user()->role->haveAction($tableName, 'readonly', $model->readonly);

            if (! Auth::user()->role->hasModule($tableName) || (! $editable && !$readonly)) {
                abort(404);
            }

            if (Auth::user()->role->hasModule($tableName)) {
                $moduleIds = Auth::user()->role->getModuleIdsAccess($tableName);

                if (! in_array($id, $moduleIds)) {
                    abort(404);
                }
            }
        }

        if ($model->languagable && Schema::hasColumn($tableName, $model->languageField)) {
            $model = $model->where($model->languageField, Localization::getAdminLocale())->find($id);
        } else {
            $model = $model::find($id);
        }

        $parentId = $parentCurrentId;
        $parentField = $parentCurrentField;

        if (! empty($id) && !empty($parentCurrentId) && !empty($parentCurrentField)) {
            $parentId = $id;
            $parentField = $parentCurrentId;
            $id = $parentCurrentField;
        }

        if (! empty($parentId) && !empty($parentField)) {
            $model = $model->where($parentField, $parentId);

            if ($model->languagable && Schema::hasColumn($tableName, $model->languageField)) {
                $model = $model->where($model->languageField, Localization::getAdminLocale());
            }

            $model = $model->find($id);
        }

        if (empty($model->id)) {
            abort(404);
        }

        $routeLink = '/' . str_replace('{id}', $model->id, Route::current()->uri());
        $mainPath = '/' . str_replace('/edit/{id}', '', Route::current()->uri());

        if (! empty($parentId) && !empty($parentField)) {
            $query = $model->where('id', $parentId);

            if ($model->languagable && Schema::hasColumn($tableName, $model->languageField)) {
                $query = $query->where($model->languageField, Localization::getAdminLocale());
            }

            if ($query->count() == 0) {
                abort(404);
            }

            $routeLink = str_replace(['{parentId?}', '{parentField?}'], [$parentId, $parentField], $routeLink);
            $mainPath = str_replace(['{parentId?}', '{parentField?}'], [$parentId, $parentField], $mainPath);
        }

        /* Если запрещено редактирование, то перенаправляем в таблицу данных. */
        if (empty($model->editable)) {
            return redirect($routeLink);
        }

        $form = new Construct;
        $breadcrumbs = method_exists($model, 'breadcrumbs') && !empty($model->breadcrumbs())
            ? $model->breadcrumbs()
            : [];

        $data = [
            'form' => $form->tabsGenerate($model, [
                'formId' => 'form-base',
                'mainPath' => $mainPath,
                'formPath' => $routeLink
            ]),
            'title' => translate('system.form.packageItems.editing'),
            'breadcrumbs' => $breadcrumbs
        ];

        return view('ariol::components.tabs.index', $data);
    }

    /**
     * Обновление данных.
     *
     * @param Request $request
     * @param null $id
     * @param null $parentCurrentId
     * @param null $parentCurrentField
     * @return array|mixed
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function update(Request $request, $id = null, $parentCurrentId = null, $parentCurrentField = null)
    {
        if (empty($this->model)) {
            abort(503);
        }

        if (! empty($parentCurrentId) && empty($parentCurrentField)) {
            $parentId = $id;
            $parentField = $parentCurrentId;
            $id = null;
        } else {
            $parentId = $parentCurrentId;
            $parentField = $parentCurrentField;
        }

        if (! empty($id) && !empty($parentCurrentId) && !empty($parentCurrentField)) {
            $parentId = $id;
            $parentField = $parentCurrentId;
            $id = $parentCurrentField;
        }

        $saveButton = false;
        $model = new $this->model;

        $tableName = with($model)->getTable();

        if ($id) {
            if ($model->languagable && Schema::hasColumn($tableName, $model->languageField)) {
                $model = $model->where($model->languageField, Localization::getAdminLocale())->find($id);
            } else {
                $model = $model::find($id);
            }
        }

        if (! empty($parentId) && !empty($parentField) && $id) {
            $model = $model->where($parentField, $parentId);

            if ($model->languagable && Schema::hasColumn($tableName, $model->languageField)) {
                $model = $model->where($model->languageField, Localization::getAdminLocale());
            }

            $model = $model->find($id);
        }

        if (preg_match("/create/", Route::current()->uri())) {
            $saveButton = true;
        }

        $routeLink = str_replace('/create', '', Route::current()->uri());
        $routeLink = str_replace('/edit/{id}', '', $routeLink);

        if (! empty($parentId) && !empty($parentField)) {
            $routeLink = str_replace(['{parentId?}', '{parentField?}'], [$parentId, $parentField], $routeLink);
        }

        /* Если включён режим только просмотра или запрещено редактирование, то перенаправляем в таблицу данных. */
        if (! empty($model->readonly) || empty($model->editable)) {
            return redirect('/' . $routeLink);
        }

        $allForm = $model->form();
        $labels = $model->labels();

        /* Преобразование стандартного указания полей формы в формат для вкладок. */
        if (! multi_array_key_exists($labels, 'content', true)) {
            $allForm = ['tab' => $allForm];
        }

        $group = key($allForm);
        $form = $allForm[key($allForm)];

        $requestData = $request->except('_token', 'group_tab', 'selectedFilesForUpload');

        /* После обработки основной вкладки получаем её ID, чтобы связать со связующими таблицами. */
        $mainModelId = $this->validateForm(
            $group,
            $form,
            $requestData,
            $this->model,
            null,
            null,
            $id,
            $parentId,
            $parentField
        );

        if (! is_numeric($mainModelId)) {
            return $mainModelId;
        }

        unset($allForm[key($allForm)]);

        foreach ($allForm as $group => $form) {
            $this->validateForm(
                $group,
                $form,
                $requestData,
                $model->labels()[$group]['model']['name'],
                $model->labels()[$group]['model']['relationship'],
                $mainModelId,
                null,
                $parentId,
                $parentField
            );
        }

        if (is_numeric($mainModelId) && $saveButton) {
            return ['message' => $mainModelId];
        }

        return ['message' => translate('system.form.packageItems.success')];
    }

    /**
     * Валидация форм.
     *
     * @param $parentId
     * @param $parentField
     * @param $group
     * @param $form
     * @param $requestData
     * @param $model
     * @param bool $relationship
     * @param bool $mainModelId
     * @param bool $id
     * @return mixed
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function validateForm(
        $group,
        $form,
        $requestData,
        $model,
        $relationship = false,
        $mainModelId = false,
        $id = null,
        $parentId = null,
        $parentField = null
    ) {
        $model = new $model;

        if (empty($relationship)) {
            if (! empty($id)) {
                if (! empty($parentId) && !empty($parentField)) {
                    $model = $model->where($parentField, $parentId);
                }

                $model = $model->find($id);
            }
        } else {
            $getArticle = $model::where($relationship, $mainModelId)->first();

            if ($getArticle) {
                $model = $model::find($getArticle->id);
            }
        }

        $multipleField = '';
        $multipleFieldValue = '';

        /* Поля, которые задействованы в конструкторе. */
        $builderFields = [];

        foreach ($form as $key => $value) {
            $form[$group . '_' . $key] = $value;
            unset($form[$key]);
        }

        $request = [];
        $notEmpty = 0;

        foreach ($requestData as $key => $value) {
            if (preg_match('/' . $group . '_/i', $key)) {
                $request[$key] = $value;

                if (isset($value) || is_null($value)) {
                    $notEmpty = 1;
                }
            }
        }

        if ($notEmpty > 0) {
            $checkFields = $this->verify->checkRequireFields($form, $request, $group);
            if (count($checkFields) > 0) {
                return [
                    'checkFields' => $checkFields,
                    'message' => translate('system.form.packageItems.field-require')
                ];
            }

            $checkFields = $this->verify->checkUniqueFields($form, $request, $group);
            if (count($checkFields) > 0) {
                return [
                    'checkFields' => $checkFields,
                    'message' => translate('system.form.packageItems.field-unique')
                ];
            }

            $checkFields = $this->verify->validateFields($form, $request, $group);
            if (count($checkFields) > 0) {
                return [
                    'checkFields' => $checkFields,
                    'message' => translate('system.form.packageItems.field-validate')
                ];
            }

            $modifiedFields = ['builder', 'divider', 'html'];

            foreach ($form as $nameField => $formFieldArr) {
                $realFormField = str_replace($group . '_', '', $nameField);

                if ($formFieldArr['type'] == 'builder' && ! in_array('id', $formFieldArr['fields'])) {
                    $form[$nameField]['fields']['id'] = [
                        'type' => 'hidden'
                    ];
                }

                if ($formFieldArr['type'] == 'boolean') {
                    if (! array_key_exists($nameField, $request)) {
                        $model->$realFormField = 0;
                    }
                }

                if (! isset($request[$nameField]) && !in_array($formFieldArr['type'], $modifiedFields)
                    && !isset($formFieldArr['model']) && !isset($formFieldArr['relationship'])) {
                    $request[$nameField] = null;
                }
            }

            foreach ($request as $field => $value) {
                if (preg_match('/' . $group . '_/i', $field)) {
                    $realField = str_replace($group . '_', '', $field);

                    if (! isset($form[$field])) {
                        foreach ($form as $fieldName => $fields) {
                            if (isset($fields['fields'][$realField])) {
                                $form[$field] = $fields['fields'][$realField];

                                if (! $this->verify->getParam($fields['fields'][$realField], 'ignore')) {
                                    $builderFields[$fieldName][] = $realField;
                                }
                            }
                        }
                    }

                    if ($this->verify->getParam($form[$field], 'ignore')) {
                        unset($request[$field]);

                        continue;
                    }

                    $validateField = $this->verify->{$form[$field]['type']}($form, $field, $value, $group);

                    if ((isset($validateField) || is_null($validateField)) && $validateField !== false) {
                        $parentFieldName = preg_match("/" . $group . "_/", $parentField)
                            ? $parentField
                            : $group . '_' . $parentField;

                        if ($parentFieldName == $field
                            && (is_null($validateField) || $validateField == 0)
                            && !empty($parentId)
                            && !empty($parentField)
                        ) {
                            $model->{$parentField} = $parentId;
                        } else {
                            $model->{$realField} = $validateField;
                        }
                    } elseif (isset($validateField['checkFields'])) {
                        return $validateField['checkFields'];
                    }
                }

                unset($form[$field]);
            }

            /* Запись в текущую модель: связующее поле => ID главной модели. */
            if (! empty($relationship)) {
                $model->{$relationship} = $mainModelId;
            }

            /* Если используется тип builder. */
            if (! empty($builderFields)) {
                $this->verify->builder($model, $form, $builderFields, $relationship, $mainModelId, $group);

                /* Записывать выбранные данные (select) не в одно поле, а как разные записи. */
            } elseif (! empty($multipleField) && !empty($multipleFieldValue) && !empty($relationship)) {
                /* Удаляем все записи, связанные с основной моделью через ID. */
                $model::where($relationship, $mainModelId)->delete();

                /* Если выбранно несколько данных, то создаём записи через цикл, иначе - одну запись. */
                if (is_array($multipleFieldValue)) {
                    foreach ($multipleFieldValue as $val) {
                        $model::create([
                            $multipleField => $val,
                            $relationship => $mainModelId
                        ]);
                    }
                } else {
                    $model::create([
                        $multipleField => $multipleFieldValue,
                        $relationship => $mainModelId
                    ]);
                }
            } else {
                $modifiedFields = ['builder', 'divider', 'html'];

                if (! empty($form)) {
                    foreach ($form as $field => $type) {
                        $realField = str_replace($group . '_', '', $field);

                        if (! isset($type['model'])
                            && !isset($type['relationship'])
                            && !$this->verify->getParam($type, 'ignore')
                        ) {
                            if (! in_array($type['type'], $modifiedFields)) {
                                $model->{$realField} = null;
                            }

                            unset($form[$field]);
                        }
                    }
                }

                /* Наименование таблицы. */
                $tableName = with($model)->getTable();

                if ($model->languagable && Schema::hasColumn($tableName, $model->languageField)) {
                    $model->{$model->languageField} = Localization::getAdminLocale();
                }

                $model->save();
            }

            if (empty($relationship)) {
                return $model->id;
            }
        }

        return ['success' => translate('system.form.packageItems.success')];
    }

    /**
     * Удаление записи.
     *
     * @param $id
     * @param null $parentId
     * @param null $parentField
     * @return array
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function destroy($id, $parentId = null, $parentField = null)
    {
        if (empty($this->model)) {
            abort(503);
        }

        $model = new $this->model;
        $tableName = with($model)->getTable();

        if (Auth::user()->role_id != 1 && !empty(Auth::user()->role)) {
            if (! Auth::user()->role->hasModule($tableName)
                || !Auth::user()->role->haveAction($tableName, 'destroyable', $model->creatable)) {
                abort(404);
            } else {
                $moduleIds = Auth::user()->role->getModuleIdsAccess($tableName);

                if (! in_array($id, $moduleIds)) {
                    abort(404);
                }
            }
        }

        if ($model->languagable && Schema::hasColumn($tableName, $model->languageField)) {
            $item = $model->where($model->languageField, Localization::getAdminLocale())->find($id);
        } else {
            $item = $model::find($id);
        }

        if (! empty($parentId) && !empty($parentField)) {
            $item = $model->where($parentField, $parentId);

            if ($model->languagable && Schema::hasColumn($tableName, $model->languageField)) {
                $item = $item->where($model->languageField, Localization::getAdminLocale());
            }

            $item = $item->find($id);
        }

        if (! $item) {
            return ['error' => translate('system.form.packageItems.no-isset')];
        }

        /* Проверка на возможность удаления записей. */
        $destroyIf = $this->verify->destroyExists($model, $id);

        $routeLink = str_replace('/delete/{id}', '', Route::current()->uri());

        if (! empty($parentId) && !empty($parentField)) {
            $routeLink = str_replace(['{parentId?}', '{parentField?}'], [$parentId, $parentField], $routeLink);
        } else {
            $routeLink = str_replace(['/{parentId?}', '/{parentField?}'], '', $routeLink);
        }

        /* Если включён режим только просмотра или запрещено удаление, то перенаправляем в таблицу данных. */
        if (empty($item->destroyable) || $destroyIf != 0) {
            return redirect($routeLink);
        }

        foreach ($model->labels() as $relationshipModel) {
            if (! empty($relationshipModel['model']['name']) && !empty($relationshipModel['model']['relationship'])) {
                $newRelationshipModel = new $relationshipModel['model']['name'];
                $relationship = $relationshipModel['model']['relationship'];

                if ($newRelationshipModel::where($relationship, $id)->count() > 0) {
                    $newRelationshipModel::where($relationship, $id)->delete();
                }
            }
        }

        $form = $item->form();

        /* Если к записи прикреплены файлы, то вместе с ней удаляем и их. */
        foreach ($form as $name => $field) {
            if (! empty($field[key($field)]['model']['name']) && !empty($field[key($field)]['model']['relationship'])) {
                $field[key($field)]['model']['name']::where($field[key($field)]['model']['relationship'], $item->id)
                    ->delete();
            }

            if (! empty($field['type']) && $field['type'] == 'file') {
                if (! empty($item->{$name}) && file_exists(public_path() . $item->{$name})) {
                    unlink(public_path() . $item->{$name});
                }
            }

            if (! empty($field['type']) && $field['type'] == 'files') {
                $files = json_decode($item->{$name}, true);

                if (is_array($files)) {
                    foreach ($files as $file) {
                        if (! empty($file) && file_exists(public_path() . $file)) {
                            unlink(public_path() . $file);
                        }
                    }
                }
            }
        }

        if (! empty($model->recursiveDestroy)) {
            $this->recursiveDestroy($model, $item);
        }

        $item->delete();

        return ['success' => translate('system.form.packageItems.destroyed')];
    }

    /**
     * Рекурсивное удаление записей.
     *
     * @param $model
     * @param $item
     */
    private function recursiveDestroy($model, $item)
    {
        $field = $model->recursiveDestroy;

        $items = $model->where($field, $item->id)->get();
        if (count($items) > 0) {
            foreach ($items as $item) {
                $clone = clone $item;

                $item->delete();

                $this->recursiveDestroy($model, $clone);
            }
        }
    }

    /**
     * Получение полей без возможности сортировки.
     *
     * @param $model
     * @return array
     */
    public function getUnSortableFields($model)
    {
        if (empty($model)) {
            abort(503);
        }

        if (Auth::user()->role_id != 1) {
            $tableName = with($model)->getTable();

            if (! Auth::user()->role->hasModule($tableName) || empty(Auth::user()->role)) {
                abort(404);
            } else {
                $actions = ['creatable', 'readonly', 'editable', 'destroyable'];

                foreach ($actions as $action) {
                    if ($model->{$action}) {
                        $model->{$action} = Auth::user()->role->haveAction($tableName, $action, $model->{$action});
                    } elseif (! $model->{$action} && $action == 'readonly') {
                        $model->{$action} = Auth::user()->role->haveAction($tableName, 'readonly', $model->readonly);
                    }
                }

                if ($model->editable) {
                    $model->readonly = false;
                }
            }
        }

        $columns = $model->columns();

        if (empty($model->readonly) && empty($model->destroyable)) {
            $unSortableFields = [];
        } else {
            $unSortableFields = [0];
        }

        $sortableFields = method_exists($model, 'sortableFields') ? $model->sortableFields() : [];

        /* Убираем сортировку в колонках удаления, редактирования и просмотра записей. */
        if (empty($model->readonly)) {
            if (method_exists($model, 'children') && !empty($model->children())) {
                $columns += ['parentField' => []];
            }

            if (! empty($model->editable)) {
                $columns += ['edit' => []];
            }
        } else {
            $columns += ['readonly' => []];
        }

        if (! empty($model->destroyable)) {
            $columns += ['delete' => []];
        }

        foreach (array_keys($columns) as $index => $key) {
            $key = is_numeric($key) ? $columns[$key] : $key;
            $index = (! empty($model->readonly) && empty($model->destroyable) || empty($model->readonly) && empty($model->destroyable)) ? $index : $index + 1;

            if (Auth::user()->role_id != 1) {
                $tableName = with($model)->getTable();

                if (Auth::user()->role->hasModule($tableName)) {
                    $index = !empty($model->editable) && empty($model->destroyable) ? $index - 1 : $index;
                }
            }

            if (! in_array($key, $sortableFields) && !array_key_exists($key, $sortableFields)) {
                $unSortableFields[] = $index;
            }
        }

        return $unSortableFields;
    }

    /**
     * Настройка колонок.
     *
     * @param string $align
     * @param int $width
     * @param null $title
     * @param null $type
     * @return array
     */
    public function getField($width, $align = 'center', $title = null, $type = null)
    {
        return [
            'type' => $type,
            'title' => $title,
            'align' => $align,
            'width' => $width
        ];
    }

    /**
     * Сохранение выбранной вкладки.
     *
     * @param Request $request
     */
    public function saveTab(Request $request)
    {
        Session::put('active_tab', $request->input('tab'));
    }
}
