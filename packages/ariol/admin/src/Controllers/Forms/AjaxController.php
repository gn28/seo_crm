<?php

namespace Ariol\Admin\Controllers\Forms;

use Schema;
use Illuminate\Http\Request;
use Ariol\Classes\Localization;
use Ariol\Admin\Controllers\Controller;

/**
 * Класс ajax-полей формы.
 *
 * @package Ariol\Admin\Controllers\Forms
 */
class AjaxController extends Controller
{
    /**
     * Подгрузка данных в select или multiselect.
     *
     * @param Request $request
     * @return mixed
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function select(Request $request)
    {
        $key = $request->input('key');
        $value = $request->input('value');
        $model = $request->input('model');

        $model = new $model;
        $tableName = with($model)->getTable();

        $query = $model::select($key, $value);

        if ($model->languagable && Schema::hasColumn($tableName, $model->languageField)) {
            $query = $query->where($model->languageField, Localization::getAdminLocale());
        }

        $content = '';
        $entries = $query->orderBy($value)->get();

        foreach ($entries as $entry) {
            $content .= '<option value="' . $entry->{$key} . '">' . $entry->{$value} . '</option>';
        }

        return $content;
    }
}
