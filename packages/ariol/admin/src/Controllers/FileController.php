<?php

namespace Ariol\Admin\Controllers;

use Ariol\Classes\JQFile;
use Ariol\Classes\Helper;
use Illuminate\Http\Request;

/**
 * Класс загрузчика файлов в админке.
 *
 * @package Ariol\Admin\Controllers
 */
class FileController extends Controller
{
    /**
     * Плагин загрузки файлов.
     */
    public function upload()
    {
        new JQFile();
    }

    /**
     * Удаление файла.
     *
     * @param Request $request
     */
    public function delete(Request $request)
    {
        $filename = Helper::getReadableFileName($request->input('file'));

        $path[] = 'temp/' . $filename;
        $path[] = 'temp/thumbnail/' . $filename;

        foreach ($path as $p) {
            $p = public_path($p);

            if ($filename != null && file_exists($p)) {
                unlink($p);
            }
        }
    }
}
