<?php

namespace Ariol\Admin\Controllers;

use Illuminate\Http\Request;

/**
 * Класс автозаполнения полей.
 *
 * @package Ariol\Admin\Controllers
 */
class AutocompleteController extends Controller
{
    /**
     * Вывод предложений.
     *
     * @param Request $request
     * @return array
     */
    public function index(Request $request)
    {
        $result = [];

        $model = $request->get('model');
        $field = $request->get('field');

        $items = $model::where($field, 'like', '%' . $request->get('value') . '%')->get([$field]);

        foreach ($items as $item) {
            $result[] = $item->{$field};
        }

        return $result;
    }
}
