<?php

namespace Ariol\Admin\Controllers;

use View;
use Ariol\Classes\Localization;
use Illuminate\Routing\Controller as BaseController;

/**
 * Основной контроллер.
 *
 * @package App\Http\Controllers
 */
abstract class Controller extends BaseController
{
    /**
     * Controller constructor.
     *
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function __construct()
    {
        $data = [
            'currentLanguage' => Localization::getCurrentAdminLanguage(),
            'activeLanguages' => Localization::getSelectedLanguages(true)
        ];

        View::composer('*', function ($view) use ($data) {
            $view->with($data);
        });
    }
}
