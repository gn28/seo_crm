<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('active')->default('1');
            $table->string('name')->nullable();
            $table->string('url')->nullable();
            $table->tinyInteger('parent')->nullable()->default('0');
            $table->tinyInteger('position')->nullable();
            $table->char('language', 4)->nullable()->default('ru');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('menu');
    }
}
