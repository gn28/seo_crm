<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserRoleModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_role_modules', function (Blueprint $table) {
            $table->increments('id');
            $table->string('table')->nullable();
            $table->text('entries')->nullable();
            $table->text('actions')->nullable();
            $table->tinyInteger('role_id')->nullable()->default(0);
            $table->integer('position')->nullable()->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_role_modules');
    }
}
