<?php

namespace Ariol\Admin\Forms;

use View;

/**
 * Класс фильтров в таблице данных.
 *
 * @package Ariol\Admin\Form
 */
class Filters
{
    /**
     * Путь до шаблонов каждого типа.
     *
     * @var string
     */
    public $path = 'ariol::components.grid.filters.';

    /**
     * Наименование фильтра.
     *
     * @var
     */
    protected $label;

    /**
     * Связующее поле.
     *
     * @var
     */
    protected $field;

    /**
     * Фильтр.
     *
     * @var
     */
    protected $filter;

    /**
     * Filters constructor.
     * @param $label
     * @param $field
     * @param $filter
     */
    public function __construct($label = null, $field = null, $filter = null)
    {
        $this->filter = $filter;

        $column = [12, 0, 0];

        if (! empty($filter['column']) && is_array($filter['column'])) {
            $column = [
                0 => !$this->getValidateColumnValue($filter['column'][0]) ? 12 : $filter['column'][0],
                1 => (! empty($filter['column'][1]) && $this->getValidateColumnValue($filter['column'][1]))
                    ? $filter['column'][1]
                    : ($filter['column'][1] == 'right'
                        ? 0
                        : (! empty($filter['column'][2]) && $this->getValidateColumnValue($filter['column'][2])
                            ? $filter['column'][2]
                            : 0
                        )),
                2 => (! empty($filter['column'][2]) && $this->getValidateColumnValue($filter['column'][2]))
                    ? $filter['column'][2]
                    : 0
            ];
        } elseif (! empty($filter['column']) && !is_array($filter['column'])) {
            $column = [$filter['column'], 0, 0];
        }

        $data = [
            'name' => $field,
            'field' => $field,
            'label' => $label,
            'column' => $column
        ];

        View::composer('ariol::components.grid.filters.*', function ($view) use ($data) {
            $view->with($data);
        });
    }

    /**
     * Валидация данных, указанных в колонках.
     *
     * @param $value
     * @return bool
     */
    private function getValidateColumnValue($value)
    {
        $result = is_numeric($value) ? true : false;
        $result = $result ? (($value < 0 && $value > 12) ? false : true) : false;

        return $result;
    }

    /**
     * Checkbox.
     *
     * @return string
     * @throws \Throwable
     */
    public function checkbox()
    {
        return view($this->path . 'checkbox', [
            'checked' => !empty($this->filter['checked']) ? $this->filter['checked'] : false
        ])->render();
    }

    /**
     * Обработка checkbox'a.
     *
     * @param $items
     * @param $field
     * @param $value
     * @return mixed
     */
    public function getCheckbox($items, $field, $value)
    {
        return $items->where($field, $value);
    }

    /**
     * Выбор элемента из списка.
     *
     * @return string
     * @throws \Throwable
     */
    public function select()
    {
        return view($this->path . 'select', [
            'values' => !empty($this->filter['values']) ? $this->filter['values'] : [],
            'title' => !empty($this->filter['title']) ? $this->filter['title'] : 'Выберите элемент',
            'searchable' => !empty($this->filter['searchable']) ? $this->filter['searchable'] : false
        ])->render();
    }

    /**
     * Обработка select'a.
     *
     * @param $items
     * @param $field
     * @param $value
     * @return mixed
     */
    public function getSelect($items, $field, $value)
    {
        return $items->where($field, $value);
    }

    /**
     * Выбор нескольких элементов из списка.
     *
     * @return string
     * @throws \Throwable
     */
    public function multiselect()
    {
        return view($this->path . 'multiselect', [
            'values' => !empty($this->filter['values']) ? $this->filter['values'] : [],
            'searchable' => !empty($this->filter['searchable']) ? $this->filter['searchable'] : false
        ])->render();
    }

    /**
     * Обработка multiselect'a.
     *
     * @param $items
     * @param $field
     * @param $values
     * @return mixed
     */
    public function getMultiselect($items, $field, $values)
    {
        return $items->whereIn($field, $values);
    }
}
