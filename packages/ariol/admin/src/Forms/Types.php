<?php

namespace Ariol\Admin\Forms;

use View;

/**
 * Класс типов полей.
 *
 * @package Ariol\Admin\Form
 */
class Types
{
    /**
     * Используемая модель.
     *
     * @var
     */
    public $model;

    /**
     * Наименование поля.
     *
     * @var
     */
    public $name;

    /**
     * Параметры поля.
     *
     * @var
     */
    public $field;

    /**
     * Наименование группы.
     *
     * @var
     */
    public $group;

    /**
     * Значение поля.
     * @var
     */
    public $value;

    /**
     * Использование поля в конструкторе.
     * @var
     */
    public $builder;

    /**
     * Путь до шаблонов каждого типа.
     *
     * @var string
     */
    public $path = 'ariol::components.form.fields.';

    /**
     * Список типов изображений, доступных для превью-отображения.
     *
     * @var
     */
    public $allowedMimeTypes;

    /**
     * Types constructor.
     * @param $model
     * @param $name
     * @param $label
     * @param $field
     * @param $value
     * @param null $group
     * @param null $modelGroup
     * @param bool $builder
     */
    public function __construct(
        $model,
        $name,
        $label,
        $field,
        $value,
        $group = null,
        $modelGroup = null,
        $builder = false
    ) {
        $this->name = $name;
        $this->model = $model;
        $this->field = $field;
        $this->group = $group;
        $this->builder = $builder;

        if ($this->field['type'] != 'number') {
            $this->value = !empty($value) ? $value : (! empty($this->field['value']) ? $this->field['value'] : null);
        } else {
            $this->value = isset($value) ? $value : null;
        }

        $column = [12, 0, 0];

        if (! empty($field['column']) && is_array($field['column'])) {
            $column = [
                0 => !$this->getValidateColumnValue($field['column'][0]) ? 12 : $field['column'][0],
                1 => (! empty($field['column'][1]) && $this->getValidateColumnValue($field['column'][1]))
                    ? $field['column'][1]
                    : ($field['column'][1] == 'right'
                        ? 0
                        : (! empty($field['column'][2]) && $this->getValidateColumnValue($field['column'][2])
                            ? $field['column'][2]
                            : 0
                        )),
                2 => (! empty($field['column'][2]) && $this->getValidateColumnValue($field['column'][2]))
                    ? $field['column'][2]
                    : 0
            ];
        } elseif (! empty($field['column']) && !is_array($field['column'])) {
            $column = [$field['column'], 0, 0];
        }

        $verify = new Verify($model);

        $data = [
            'name' => $name,
            'label' => $label,
            'column' => $column,
            'groupTab' => $group,
            'modelTab' => $modelGroup,
            'require' => $verify->getParam($field, 'require'),
            'placeholder' => !empty($field['placeholder']) ? $field['placeholder'] : null,
            'description' => !empty($field['description']) ? $field['description'] : null,
            'readonly' => !empty($model->readonly) || $verify->getParam($field, 'readonly'),
            'maxLength' => !empty($field['params']['maxLength']) ? $field['params']['maxLength'] : null
        ];

        $this->allowedMimeTypes = [
            'image/jpeg', 'image/gif', 'image/png', 'image/tif',
            'image/bmp', 'image/svg+xml', 'image/x-icon', 'image/tiff',
        ];

        View::composer('ariol::components.form.fields.*', function ($view) use ($data) {
            $view->with($data);
        });
    }

    /**
     * Валидация данных, указанных в колонках.
     *
     * @param $value
     * @return bool
     */
    private function getValidateColumnValue($value)
    {
        $result = is_numeric($value) ? true : false;
        $result = $result ? (($value < 0 && $value > 12) ? false : true) : false;

        return $result;
    }

    /**
     * Автокомплит адреса от яндекса.
     *
     * @param $number
     * @return \Illuminate\Contracts\View\View
     * @throws \Throwable
     */
    public function address($number = null)
    {
        return view($this->path . 'address', [
            'value' => $this->value,
            'builder' => isset($number) ? '[]' : (! empty($this->builder) ? '[]' : null)
        ])->render();
    }

    /**
     * Массив.
     *
     * @param int $number
     * @return \Illuminate\Contracts\View\View
     * @throws \Throwable
     */
    public function arr($number = null)
    {
        return view($this->path . 'json.arr', [
            'field' => !empty($this->field['field']) ? $this->field['field'] : null,
            'value' => !empty($this->value) ? json_decode($this->value, true) : [0 => ''],
            'builder' => isset($number) ? '[' . $number . ']' : (! empty($this->builder) ? '[]' : null)
        ])->render();
    }

    /**
     * Автозаполнение поля или поиск в реальном времени с использованием данных из другой таблицы.
     *
     * @param int $number
     * @return \Illuminate\Contracts\View\View
     * @throws \Throwable
     */
    public function autocomplete($number = null)
    {
        $value = '';

        foreach (json_decode($this->value) as $val) {
            $item = $this->field['orm']['model']::where('id', $val)->first();

            if (isset($item)) {
                $value .= $item->{$this->field['orm']['field']} . ($val != end($values) ? ', ' : null);
            }
        }

        return view($this->path . 'orm.autocomplete', [
            'value' => $value,
            'model' => $this->field['orm']['model'],
            'field' => $this->field['orm']['field'],
            'builder' => isset($number) ? '[]' : (! empty($this->builder) ? '[]' : null)
        ])->render();
    }

    /**
     *  Поле выбора - checkbox.
     *
     * @param int $number
     * @return \Illuminate\Contracts\View\View
     * @throws \Throwable
     */
    public function boolean($number = null)
    {
        return view($this->path . 'boolean', [
            'value' => $this->value,
            'builder' => isset($number) ? '[]' : (! empty($this->builder) ? '[]' : null),
            'checked' => !empty($this->field['checked']) ? $this->field['checked'] : false
        ])->render();
    }

    /**
     * Конструктор полей.
     *
     * @param null $number
     * @return string
     * @throws \Throwable
     */
    public function builder($number = null)
    {
        $labels = $this->model->labels();
        $name = str_replace($this->group . '_', '', $this->name);

        /* Преобразование стандартного указания лэйблов в формат для вкладок. */
        if (! multi_array_key_exists($labels, 'content', true)) {
            $labels = [
                'tab' => [
                    'title' => translate('system.form.packageItems.tab'),
                    'content' => $labels
                ]
            ];
        }

        $fieldLabels = $labels[$this->group]['content'];

        $builderValues = [];
        $formContent = view('ariol::components.form.fields.builder.open')->render();

        $align = !empty($this->field['align']) ? $this->field['align'] : 'left';

        if (! empty($this->field['model']['name']) && !empty($this->field['model']['relationship'])) {
            $fieldModel = $this->field['model']['name'];

            $builderModel = new $fieldModel;
            $builderModelData = $builderModel::where($this->field['model']['relationship'], $this->model->id)
                ->orderBy('position')->get();

            if (isset($builderModelData)) {
                foreach ($builderModelData as $builderData) {
                    foreach ($this->field['fields'] as $builderName => $builderField) {
                        $builderValues[$builderData->id][$builderName] = !empty($builderData->{$builderName})
                            ? $builderData->{$builderName}
                            : null;
                    }
                }
            }

            if (count($builderModelData) > 0) {
                $i = 0;

                foreach ($builderModelData as $builderKeyData => $builderData) {
                    $builderFormContent = '';

                    if (! in_array('id', $this->field['fields'])) {
                        $this->field['fields']['id'] = [
                            'type' => 'hidden'
                        ];
                    }

                    foreach ($this->field['fields'] as $builderName => $builderField) {
                        $builderLabel = array_get($fieldLabels, $builderName, $builderName);
                        $builderGroupName = $this->group . '_' . $builderName;

                        $builderValue = isset($builderValues[$builderData->id][$builderName])
                            ? $builderValues[$builderData->id][$builderName]
                            : null;

                        $builderField['builder'] = true;

                        $builderTypes = new Types(
                            $this->model,
                            $builderGroupName,
                            $builderLabel,
                            $builderField,
                            $builderValue,
                            $this->group,
                            $this->group,
                            true
                        );

                        $builderFormContent .= $builderTypes->{$builderField['type']}($i);
                    }

                    $formContent .= view('ariol::components.form.fields.builder.fields', [
                        'name' => $name,
                        'align' => $align,
                        'mainId' => $this->model->id,
                        'fields' => $builderFormContent,
                        'model' => $this->field['model']['name'] ?? null,
                        'first_builder' => $builderKeyData == 0 ? true : false
                    ])->render();

                    $i++;
                }
            } else {
                $builderFormContent = '';

                foreach ($this->field['fields'] as $builderName => $builderField) {
                    $builderLabel = array_get($fieldLabels, $builderName, $builderName);
                    $builderGroupName = $this->group . '_' . $builderName;

                    $builderValue = isset($builderValues[$builderName]) ? $builderValues[$builderName] : null;

                    $builderField['builder'] = true;

                    $builderTypes = new Types(
                        $this->model,
                        $builderGroupName,
                        $builderLabel,
                        $builderField,
                        $builderValue,
                        $this->group,
                        $this->group,
                        true
                    );

                    $builderFormContent .= $builderTypes->{$builderField['type']}('0');
                }

                $formContent .= view('ariol::components.form.fields.builder.fields', [
                    'name' => $name,
                    'align' => $align,
                    'first_builder' => true,
                    'mainId' => $this->model->id,
                    'fields' => $builderFormContent,
                    'model' => $this->field['model']['name'] ?? null
                ])->render();
            }
        } else {
            $builderModelData = !empty($this->model->{$name}) ? json_decode($this->model->{$name}, true) : [];

            if (empty($builderModelData)) {
                $builderModelValue = $this->model->where('group', $this->group)->where('key', $name)->pluck('value');
                $builderModelData = !empty($builderModelValue[0]) ? json_decode($builderModelValue[0], true) : [];
            }

            if (isset($builderModelData)) {
                foreach ($builderModelData as $builderData) {
                    foreach ($this->field['fields'] as $builderName => $builderField) {
                        $builderValues[$builderName] = !empty($builderData) ? $builderData : null;
                    }
                }
            }

            $i = 0;
            $countFields = !empty($builderModelData) ? count($builderModelData[key($builderModelData)]) : 1;

            for ($c = 0; $c < $countFields; $c++) {
                $builderFormContent = '';

                foreach ($this->field['fields'] as $builderName => $builderField) {
                    $builderLabel = array_get($fieldLabels, $builderName, $builderName);
                    $builderGroupName = $this->group . '_' . $builderName;

                    $builderValues = !empty($this->model->{$name}) ? json_decode($this->model->{$name}, true) : [];
                    $builderValue = isset($builderValues[$builderName][$i]) ? $builderValues[$builderName][$i] : null;

                    if (empty($builderValues)) {
                        $getValue = $this->model->where('group', $this->group)->where('key', $name)->pluck('value');
                        $builderValues = !empty($getValue[0]) ? json_decode($getValue[0], true) : [];
                        $builderValue = isset($builderValues[$builderName][$i])
                            ? $builderValues[$builderName][$i]
                            : null;
                    }

                    $builderField['builder'] = true;

                    $builderTypes = new Types(
                        $this->model,
                        $builderGroupName,
                        $builderLabel,
                        $builderField,
                        $builderValue,
                        $this->group,
                        $this->group,
                        true
                    );

                    $builderFormContent .= $builderTypes->{$builderField['type']}($i);
                }

                $formContent .= view('ariol::components.form.fields.builder.fields', [
                    'name' => $name,
                    'align' => $align,
                    'first_builder' => true,
                    'mainId' => $this->model->id,
                    'fields' => $builderFormContent,
                    'model' => $this->field['model']['name'] ?? null,
                ])->render();

                $i++;
            }
        }

        $formContent .= view('ariol::components.form.fields.builder.close')->render();

        return $formContent;
    }

    /**
     * Выбор цвета.
     *
     * @param int $number
     * @return \Illuminate\Contracts\View\View
     * @throws \Throwable
     */
    public function color($number = null)
    {
        return view($this->path . 'pickers.color', [
            'value' => $this->value,
            'builder' => isset($number) ? '[]' : (! empty($this->builder) ? '[]' : null)
        ])->render();
    }

    /**
     * Выбор даты.
     *
     * @param int $number
     * @return \Illuminate\Contracts\View\View
     * @throws \Throwable
     */
    public function date($number = null)
    {
        return view($this->path . 'pickers.date', [
            'value' => $this->value,
            'builder' => isset($number) ? '[]' : (! empty($this->builder) ? '[]' : null)
        ])->render();
    }

    /**
     * Выбор даты и времени.
     *
     * @param int $number
     * @return \Illuminate\Contracts\View\View
     * @throws \Throwable
     */
    public function datetime($number = null)
    {
        return view($this->path . 'pickers.datetime', [
            'value' => $this->value,
            'builder' => isset($number) ? '[]' : (! empty($this->builder) ? '[]' : null)
        ])->render();
    }

    /**
     * Разделитель.
     *
     * @param int $number
     * @return \Illuminate\Contracts\View\View
     * @throws \Throwable
     */
    public function divider($number = null)
    {
        $type = (! empty($this->field['params']['type'])
            && ($this->field['params']['type'] == 'hr' || $this->field['params']['type'] == 'string'))
            ? $this->field['params']['type'] : 'string';

        $size = $type == 'hr' ? '1' : '16';
        $margin = $type == 'hr' ? '15' : '40';
        $align = $type == 'hr' ? 'left' : 'center';
        $color = $type == 'hr' ? '#ddd' : '#575656';

        return view($this->path . 'views.divider', [
            'type' => $type,
            'size' => !empty($this->field['params']['size']) ? $this->field['params']['size'] : $size,
            'color' => !empty($this->field['params']['color']) ? $this->field['params']['color'] : $color,
            'align' => !empty($this->field['params']['align']) ? $this->field['params']['align'] : $align,
            'margin' => !empty($this->field['params']['margin']) ? $this->field['params']['margin'] : $margin
        ])->render();
    }

    /**
     * Текстовый редактор.
     *
     * @param int $number
     * @return \Illuminate\Contracts\View\View
     * @throws \Throwable
     */
    public function editor($number = null)
    {
        $variants = ['tinymce', 'ckeditor'];

        return view($this->path . 'editor', [
            'value' => $this->value,
            'builder' => isset($number) ? '[]' : (! empty($this->builder) ? '[]' : null),
            'variant' => !empty($this->field['variant']) && in_array($this->field['variant'], $variants)
                ? $this->field['variant']
                : 'ckeditor'
        ])->render();
    }

    /**
     * Электронная почта.
     *
     * @param int $number
     * @return \Illuminate\Contracts\View\View
     * @throws \Throwable
     */
    public function email($number = null)
    {
        return view($this->path . 'email', [
            'value' => $this->value,
            'builder' => isset($number) ? '[]' : (! empty($this->builder) ? '[]' : null)
        ])->render();
    }

    /**
     * Выбор файла.
     *
     * @param int $number
     * @return \Illuminate\Contracts\View\View
     * @throws \Throwable
     */
    public function file($number = null)
    {
        $fileName = explode('/', $this->value);

        $creatable = isset($this->field['params']['creatable']) ? $this->field['params']['creatable'] : true;
        $destroyable = isset($this->field['params']['destroyable']) ? $this->field['params']['destroyable'] : true;

        return view($this->path . 'json.file', [
            'value' => $this->value,
            'creatable' => $creatable,
            'filename' => end($fileName),
            'destroyable' => $destroyable,
            'allowedMimeTypes' => $this->allowedMimeTypes,
            'builder' => !empty($this->builder) ? '[]' : null,
            'mime' => file_exists(public_path() . $this->value) ? mime_content_type(public_path() . $this->value) : null
        ])->render();
    }

    /**
     * Выбор файлов.
     *
     * @param int $number
     * @return \Illuminate\Contracts\View\View
     * @throws \Throwable
     */
    public function files($number = null)
    {
        return view($this->path . 'json.files', [
            'value' => json_decode($this->value, true),
            'allowedMimeTypes' => $this->allowedMimeTypes,
            'builder' => isset($number) ? '[' . $number . ']' : null,
            'creatable' => isset($this->field['params']['creatable']) ? $this->field['params']['creatable'] : true,
            'destroyable' => isset($this->field['params']['destroyable']) ? $this->field['params']['destroyable'] : true
        ])->render();
    }

    /**
     * Хеш.
     *
     * @param int $number
     * @return \Illuminate\Contracts\View\View
     * @throws \Throwable
     */
    public function hash($number = null)
    {
        return view($this->path . 'json.hash', [
            'builder' => isset($number) ? '[]' : (! empty($this->builder) ? '[]' : null),
            'value' => !empty($this->value) ? json_decode($this->value, true) : [
                0 => ''
            ]
        ])->render();
    }

    /**
     * Хеш из string и file.
     *
     * @param int $number
     * @return \Illuminate\Contracts\View\View
     * @throws \Throwable
     */
    public function buttons($number = null)
    {
        $mime = [];
        $fileName = [];
        $values = json_decode($this->value, true);

        if (count($values) > 0) {
            foreach ($values as $key => $value) {
                $name = !empty($value[1]) ? explode('/', $value[1]) : null;
                $fileName[$key] = !empty($name) ? end($name) : null;
                $mime[$key] = !empty($value[1]) && file_exists(public_path() . $value[1])
                    ? mime_content_type(public_path() . $value[1])
                    : null;
            }
        }

        return view($this->path . 'json.buttons', [
            'builder' => isset($number) ? '[' . $number . ']' : (! empty($this->builder) ? '[]' : null),
            'value' => !empty($this->value) ? $values : [
                0 => ''
            ],
            'allowedMimeTypes' => $this->allowedMimeTypes,
            'fileName' => $fileName,
            'mime' => $mime
        ])->render();
    }

    /**
     * Скрытое поле.
     *
     * @param int $number
     * @return \Illuminate\Contracts\View\View
     * @throws \Throwable
     */
    public function hidden($number = null)
    {
        return view($this->path . 'hidden', [
            'value' => $this->value,
            'builder' => isset($number) ? '[]' : (! empty($this->builder) ? '[]' : null)
        ])->render();
    }

    /**
     * Просто вывод html-данных.
     *
     * @param null $number
     * @return \Illuminate\Contracts\View\View
     * @throws \Throwable
     */
    public function html($number = null)
    {
        return view($this->path . 'views.html', [
            'value' => $this->value,
        ])->render();
    }

    /**
     * Ввод/выбор адреса и получение его координат.
     *
     * @param int $number
     * @return \Illuminate\Contracts\View\View
     * @throws \Throwable
     */
    public function locale($number = null)
    {
        return view($this->path . 'pickers.locale', [
            'value' => json_decode($this->value),
            'builder' => isset($number) ? '[]' : (! empty($this->builder) ? '[]' : null)
        ])->render();
    }

    /**
     * Выбор нескольких элементов из списка.
     *
     * @param int $number
     * @return \Illuminate\Contracts\View\View
     * @throws \Throwable
     */
    public function multiselect($number = null)
    {
        return view($this->path . 'selects.multiselect', [
            'builder' => isset($number) ? '[' . $number . ']' : null,
            'values' => !empty($this->field['values']) ? $this->field['values'] : [],
            'nullable' => isset($this->field['nullable']) ? $this->field['nullable'] : true,
            'standart' => isset($this->field['standart']) ? $this->field['standart'] : false,
            'selected' => is_array($this->value) ? $this->value : json_decode($this->value, true)
        ])->render();
    }

    /**
     * Автодополнение в селекте с выбором нескольких элементов.
     *
     * @param int $number
     * @return \Illuminate\Contracts\View\View
     * @throws \Throwable
     */
    public function multiselect_autocomplete($number = null)
    {
        $values = [];
        $value = !empty($this->value) ? json_decode($this->value, true) : null;

        if (! empty($value) && is_array($value)) {
            foreach ($value as $val) {
                $item = $this->field['orm']['model']::where('id', $val)->first();

                if (isset($item)) {
                    $values += [
                        $val => $item->{$this->field['orm']['field']}
                    ];
                }
            }
        } elseif (! empty($value) && is_numeric($value)) {
            $item = $this->field['orm']['model']::where('id', $value)->first();

            if (isset($item)) {
                $values += [
                    $value => $item->{$this->field['orm']['field']}
                ];
            }
        }

        return view($this->path . 'selects.multiselect_autocomplete', [
            'values' => $values,
            'url' => $this->field['url'],
            'model' => $this->field['orm']['model'],
            'field' => $this->field['orm']['field'],
            'builder' => isset($number) ? '[' . $number . ']' : null
        ])->render();
    }

    /**
     * Выбор нескольких элементов из списка с группами.
     *
     * @param int $number
     * @return \Illuminate\Contracts\View\View
     * @throws \Throwable
     */
    public function multiselect_group($number = null)
    {
        return view($this->path . 'selects.multiselect_group', [
            'builder' => isset($number) ? '[' . $number . ']' : null,
            'values' => !empty($this->field['values']) ? $this->field['values'] : [],
            'nullable' => isset($this->field['nullable']) ? $this->field['nullable'] : true,
            'standart' => isset($this->field['standart']) ? $this->field['standart'] : false,
            'selected' => is_array($this->value) ? $this->value : json_decode($this->value, true)
        ])->render();
    }

    /**
     * Строкое поле, позволяющее вводить только цифры.
     *
     * @param int $number
     * @return \Illuminate\Contracts\View\View
     * @throws \Throwable
     */
    public function number($number = null)
    {
        if (! empty($this->field['nullable'])) {
            $value = $this->value;
        } else {
            $value = !empty($this->value) ? $this->value : null;
        }

        return view($this->path . 'number', [
            'builder' => !empty($this->builder) ? '[]' : null,
            'clearable' => !empty($this->field['clearable']),
            'value' => $value
        ])->render();
    }

    /**
     * Пароль.
     *
     * @param int $number
     * @return \Illuminate\Contracts\View\View
     * @throws \Throwable
     */
    public function password($number = null)
    {
        return view($this->path . 'password', [
            'value' => '',
            'builder' => isset($number) ? '[]' : (! empty($this->builder) ? '[]' : null)
        ])->render();
    }

    /**
     * Телефон.
     *
     * @param int $number
     * @return \Illuminate\Contracts\View\View
     * @throws \Throwable
     */
    public function phone($number = null)
    {
        return view($this->path . 'phone', [
            'value' => $this->value,
            'builder' => isset($number) ? '[]' : (! empty($this->builder) ? '[]' : null)
        ])->render();
    }

    /**
     * Выбор элемента из списка.
     *
     * @param int $number
     * @return \Illuminate\Contracts\View\View
     * @throws \Throwable
     */
    public function select($number = null)
    {
        return view($this->path . 'selects.select', [
            'selected' => $this->value,
            'duplicate' => !empty($this->field['duplicate']),
            'values' => !empty($this->field['values']) ? $this->field['values'] : [],
            'builder' => isset($number) ? '[]' : (! empty($this->builder) ? '[]' : null),
            'nullable' => isset($this->field['nullable']) ? $this->field['nullable'] : true,
            'model' => !empty($this->field['ajax']['model']) ? $this->field['ajax']['model'] : null,
            'relation' => !empty($this->field['ajax']['relation']) ? $this->field['ajax']['relation'] : null,
            'key' => !empty($this->field['ajax']['fields']['key']) ? $this->field['ajax']['fields']['key'] : null,
            'field' => !empty($this->field['ajax']['fields']['value']) ? $this->field['ajax']['fields']['value'] : null,
            'language' => !empty($this->field['ajax']['fields']['language']) ? $this->field['ajax']['fields']['language'] : null
        ])->render();
    }

    /**
     * Автодополнение в селекте с выбором одного элемента.
     *
     * @param int $number
     * @return \Illuminate\Contracts\View\View
     * @throws \Throwable
     */
    public function select_autocomplete($number = null)
    {
        $value = !empty($this->value) ? json_decode($this->value) : null;

        if (! empty($value)) {
            $item = $this->field['orm']['model']::where('id', $value)->first();

            if (! empty($item)) {
                $value = [
                    'id' => $value,
                    'name' => $item->{$this->field['orm']['field']}
                ];
            }
        }

        return view($this->path . 'selects.select_autocomplete', [
            'value' => $value,
            'url' => $this->field['url'],
            'model' => $this->field['orm']['model'],
            'field' => $this->field['orm']['field'],
            'duplicate' => !empty($this->field['duplicate']),
            'builder' => isset($number) ? '[]' : (! empty($this->builder) ? '[]' : null)
        ])->render();
    }

    /**
     * Выбор элемента из списка с группами.
     *
     * @param int $number
     * @return \Illuminate\Contracts\View\View
     * @throws \Throwable
     */
    public function select_group($number = null)
    {
        return view($this->path . 'selects.select_group', [
            'selected' => $this->value,
            'duplicate' => !empty($this->field['duplicate']),
            'values' => !empty($this->field['values']) ? $this->field['values'] : [],
            'builder' => isset($number) ? '[]' : (! empty($this->builder) ? '[]' : null),
            'nullable' => isset($this->field['nullable']) ? $this->field['nullable'] : true
        ])->render();
    }

    /**
     * Строковое поле.
     *
     * @param $number
     * @return \Illuminate\Contracts\View\View
     * @throws \Throwable
     */
    public function string($number = null)
    {
        return view($this->path . 'string', [
            'value' => $this->value,
            'builder' => isset($number) ? '[]' : (! empty($this->builder) ? '[]' : null),
            'slug' => !empty($this->field['params']['slug']) ? $this->field['params']['slug'] : null
        ])->render();
    }

    /**
     * Текстовое поле.
     *
     * @param int $number
     * @return \Illuminate\Contracts\View\View
     * @throws \Throwable
     */
    public function text($number = null)
    {
        return view($this->path . 'text', [
            'value' => $this->value,
            'builder' => isset($number) ? '[]' : (! empty($this->builder) ? '[]' : null)
        ])->render();
    }

    /**
     * Выбор времени.
     *
     * @param int $number
     * @return \Illuminate\Contracts\View\View
     * @throws \Throwable
     */
    public function time($number = null)
    {
        return view($this->path . 'pickers.time', [
            'number' => $number,
            'value' => $this->value,
            'builder' => isset($number) ? '[]' : (! empty($this->builder) ? '[]' : null)
        ])->render();
    }

    /**
     * Ссылка.
     *
     * @param int $number
     * @return \Illuminate\Contracts\View\View
     * @throws \Throwable
     */
    public function url($number = null)
    {
        return view($this->path . 'url', [
            'value' => $this->value,
            'builder' => isset($number) ? '[]' : (! empty($this->builder) ? '[]' : null)
        ])->render();
    }
}
