<?php

namespace Ariol\Admin\Forms;

use Auth;
use Route;
use Schema;
use Session;
use Ariol\Classes\Localization;

/**
 * Класс генерации форм.
 *
 * @package Ariol\Admin\Form
 */
class Construct
{
    /**
     * Генерация форм с табами.
     *
     * @param $model
     * @param $data
     * @return string
     * @throws \Throwable
     */
    public function settingsGenerate($model, $data)
    {
        $formContent = '';
        $form = $model->form();
        $labels = $model->labels();

        $session = !empty($model->activeTab) ? 'tab_' . $model->activeTab : Session::get('active_tab');
        $sessionArr = !empty($session) ? explode('tab_', $session) : null;

        if (count($labels) > 1) {
            $formContent .= '<ul class="nav nav-tabs bg-slate nav-tabs-component">';

            foreach ($labels as $group => $tabData) {
                $tabActive = ((empty($session) && key($labels) == $group)
                    || (! empty($session) && !array_key_exists($sessionArr[1], $labels) && key($labels) == $group)
                    || ($session == 'tab_' . $group)) ? 'active' : null;

                $formContent .= view('ariol::components.settings.title', [
                    'tabGroup' => $group,
                    'tabTitle' => $tabData['title'],
                    'tabActive' => $tabActive
                ])->render();
            }

            $formContent .= "</ul>";
        }

        $formContent .= '<div class="tab-content">';

        $fieldsData = [];

        /* Наименование таблицы. */
        $tableName = with($model)->getTable();

        if ($model->languagable && Schema::hasColumn($tableName, $model->languageField)) {
            $modelData = $model->getWithLang();
        } else {
            $modelData = $model::get();
        }

        foreach ($modelData as $fieldData) {
            if (array_key_exists($fieldData['group'], $fieldsData)) {
                $fieldsData[$fieldData['group']] += [
                    $fieldData['key'] => $fieldData['value']
                ];
            } else {
                $fieldsData += [
                    $fieldData['group'] => [
                        $fieldData['key'] => $fieldData['value']
                    ]
                ];
            }
        }

        foreach ($form as $group => $fields) {
            $tabActive = ((empty($session) && key($labels) == $group)
                || (! empty($session) && !array_key_exists($sessionArr[1], $labels) && key($labels) == $group)
                || ($session == 'tab_' . $group)) ? 'active' : null;

            $formContent .= '<div class="tab-pane ' . $tabActive . '" id="tab_' . $group . '">';
            $formContent .= '<div class="panel panel-flat">';
            $formContent .= '<div class="panel-body">';
            $formContent .= '<div class="row">';

            $formContent .= view('ariol::components.form.open', [
                'id' => 'tab-' . $group,
                'mainPath' => $data['mainPath'],
                'groupTab' => $group
            ])->render();

            $fields += [
                'group_tab' => [
                    'type' => 'hidden',
                    'value' => $group
                ]
            ];

            foreach ($fields as $name => $field) {
                $fieldLabels = $labels[$group]['content'];
                $label = array_get($fieldLabels, $name, $name);

                if ($field['type'] != 'number') {
                    $value = !empty($fieldsData[$group][$name]) ? $fieldsData[$group][$name] : null;
                } else {
                    $value = isset($fieldsData[$group][$name]) ? $fieldsData[$group][$name] : null;
                }

                $types = new Types($model, $name, $label, $field, $value, $group);
                $formContent .= $types->{$field['type']}();
            }

            $formContent .= view('ariol::components.form.close', [
                'mainPath' => $data['mainPath'],
                'groupTab' => $group,
                'settings' => true
            ])->render();

            $formContent .= '</div></div></div></div>';
        }

        $formContent .= '</div>';

        return $formContent;
    }

    /**
     * Генерация форм с вкладками.
     *
     * @param $model
     * @param $data
     * @return string
     * @throws \Throwable
     */
    public function tabsGenerate($model, $data)
    {
        $formContent = '';
        $form = $model->form();
        $labels = $model->labels();
        $tableName = with($model)->getTable();

        if (Auth::user()->role_id != 1 && !empty(Auth::user()->role)) {
            $editable = Auth::user()->role->haveAction($tableName, 'editable', $model->editable);
            $readonly = Auth::user()->role->haveAction($tableName, 'readonly', $model->readonly);

            if (! $model->readonly) {
                $model->readonly = !$editable && $readonly ? true : false;
            }

            if (! $editable && !$readonly) {
                abort(404);
            }
        }

        /* Преобразование стандартного указания лэйблов в формат для вкладок. */
        if (! multi_array_key_exists($labels, 'content', true)) {
            $labels = [
                'tab' => [
                    'title' => translate('system.form.packageItems.tab'),
                    'content' => $labels
                ]
            ];

            $form = ['tab' => $form];
        }

        $session = !empty($model->activeTab) ? 'tab_' . $model->activeTab : Session::get('active_tab');
        $sessionArr = !empty($session) ? explode('tab_', $session) : null;

        $formContent .= view('ariol::components.form.open', [
            'id' => 'form-base',
            'mainPath' => !empty($data['formPath']) ? $data['formPath'] : $data['mainPath']
        ])->render();

        if (count($labels) > 1) {
            $formContent .= '<ul class="nav nav-tabs bg-slate nav-tabs-component">';

            foreach ($labels as $group => $tabData) {
                $tabActive = ((empty($session) && key($labels) == $group)
                    || (! empty($session) && !array_key_exists($sessionArr[1], $labels) && key($labels) == $group)
                    || ($session == 'tab_' . $group)) ? 'active' : null;

                $formContent .= view('ariol::components.tabs.title', [
                    'tabGroup' => $group,
                    'tabActive' => $tabActive,
                    'tabTitle' => $tabData['title']
                ])->render();
            }

            $formContent .= "</ul>";
        }

        $formContent .= '<div class="tab-content tab-content-full-width">';

        foreach ($form as $group => $fields) {
            $tabActive = ((empty($session) && key($labels) == $group)
                || (!empty($session) && !array_key_exists($sessionArr[1], $labels) && key($labels) == $group)
                || ($session == 'tab_' . $group)) ? 'active' : null;

            $formContent .= '<div class="tab-pane ' . $tabActive . '" id="tab_' . $group . '">';
            $formContent .= '<div class="panel panel-flat">';
            $formContent .= '<div class="panel-body">';
            $formContent .= '<div class="row">';

            foreach ($fields as $name => $field) {
                $fieldLabels = $labels[$group]['content'];
                $label = array_get($fieldLabels, $name, $name);

                $groupName = $group . '_' . $name;

                if ($group != key($form)) {
                    $modelName = $labels[$group]['model']['name'];
                    $modelRelationship = $labels[$group]['model']['relationship'];

                    $groupModel = new $modelName;

                    /* Наименование таблицы. */
                    $tableName = with($groupModel)->getTable();

                    $groupModelData = $groupModel->where($modelRelationship, $model->id);
                    $groupDataSelectCount = $groupModel->where($modelRelationship, $model->id);

                    if ($groupModel->languagable && Schema::hasColumn($tableName, $groupModel->languageField)) {
                        $groupModelData = $groupModelData->where($field, Localization::getLocale());
                        $groupDataSelectCount = $groupDataSelectCount->where($field, Localization::getLocale());
                    }

                    $groupDataSelectCount = $groupDataSelectCount->count();

                    $groupDataSelect = $groupDataSelectCount > 1 ? $groupModelData->get() : $groupModelData->first();

                    $value = null;

                    if (! empty($groupDataSelect)) {
                        if ($groupDataSelectCount > 1) {
                            $values = [];

                            if (Schema::hasColumn($tableName, $name)) {
                                foreach ($groupDataSelect as $dataSelect) {
                                    $values[] = $dataSelect->{$name};
                                }

                                $value = json_encode($values);
                            }
                        } else {
                            $value = $groupDataSelect->{$name};
                        }
                    }
                } else {
                    if (! empty($field['model']) && is_array($field['model'])) {
                        if (! empty($field['model']['name']) && !empty($field['model']['relationship'])) {
                            $otherModel = new $field['model']['name'];

                            $value = $otherModel::where($field['model']['relationship'], $model->id);

                            /* Наименование таблицы. */
                            $tableName = with($otherModel)->getTable();

                            if ($otherModel->languagable && Schema::hasColumn($tableName, $otherModel->languageField)) {
                                $value = $value->where($field, Localization::getLocale());
                            }

                            $model->{$name} = json_encode($value->pluck($name)->toArray());
                        }
                    }

                    if (! empty($field['relationship']) && is_array($field['relationship'])) {
                        if (! empty($field['relationship']['model']) && !empty($field['relationship']['field'])) {
                            $relationshipModel = new $field['relationship']['model'];

                            $value = $relationshipModel::where('id', $model->{$field['relationship']['field']});

                            /* Наименование таблицы. */
                            $tableName = with($relationshipModel)->getTable();

                            if ($relationshipModel->languagable && Schema::hasColumn(
                                $tableName,
                                $relationshipModel->languageField
                            )) {
                                $value = $value->where($field, Localization::getLocale());
                            }

                            $model->{$name} = json_encode($value->first()->{$name});
                        }
                    }

                    if ($field['type'] != 'number') {
                        $value = !empty($model->{$name}) ? $model->{$name} : null;
                    } else {
                        $value = isset($model->{$name}) ? $model->{$name} : null;
                    }
                }

                $parameters = Route::current()->parameters();

                if (! empty($parameters['parentId']) && !empty($parameters['parentField'])) {
                    $parentField = $parameters['parentField'];

                    $parentFieldName = preg_match("/" . $group . "_/", $parentField)
                        ? $parentField
                        : $group . '_' . $parentField;

                    if ($groupName == $parentFieldName) {
                        $value = $parameters['parentId'];
                    }
                }

                $types = new Types($model, $groupName, $label, $field, $value, $group, $group);
                $formContent .= $types->{$field['type']}();
            }

            $formContent .= '</div></div></div></div>';
        }

        $formContent .= '</div>';

        $formContent .= view('ariol::components.form.close', [
            'readonly' => $model->readonly,
            'mainPath' => $data['mainPath'],
            'buttons' => method_exists($model, 'customButtons') ? $model->customButtons() : null
        ])->render();

        return $formContent;
    }
}
