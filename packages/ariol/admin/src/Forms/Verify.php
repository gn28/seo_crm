<?php

namespace Ariol\Admin\Forms;

use DB;
use Schema;
use Ariol\Classes\Helper;
use Ariol\Classes\Localization;

/**
 * Класс верификации данных.
 *
 * @package Ariol\Admin\Forms
 */
class Verify
{
    /**
     * ID записи.
     *
     * @var
     */
    protected $modelId;

    /**
     * Модель, данные которой будут проходить через валидацию.
     *
     * @var
     */
    protected $model;

    /**
     * Наименование модели.
     *
     * @var
     */
    protected $modelName;

    /**
     * Путь модели.
     *
     * @var
     */
    protected $modelPath;

    /**
     * Путь хранения загруженных файлов.
     *
     * @var
     */
    protected $filesPath;

    /**
     * Назначаем модель, с которой собираемся работать.
     *
     * @param $model
     */
    public function __construct($model)
    {
        $article = new $model;
        $id = request()->route('id');

        $this->model = !empty($id) ? $article->where('id', $id)->first() : $article;

        $this->modelPath = $model;

        $this->modelId = $this->getId();
        $this->modelName = $this->getModelName();

        $this->filesPath = config('ariol.files-path') . $this->modelName . '/';
    }

    /**
     * Проверка на обязательное заполнение полей.
     *
     * @param $form
     * @param $data
     * @param $group
     * @return array
     */
    public function checkRequireFields($form, $data, $group = null)
    {
        $fields = [];

        foreach ($form as $nameField => $field) {
            if (! empty($field['fields'])) {
                foreach ($field['fields'] as $currentField => $fieldData) {
                    $currentField = empty($group) ? $currentField : $group . '_' . $currentField;

                    if ($this->getParam($fieldData, 'require')) {
                        if ($fieldData['type'] == 'arr' || $fieldData['type'] == 'hash') {
                            foreach ($data[$currentField] as $item) {
                                if (! $item) {
                                    $fields[] = $currentField;
                                }
                            }
                        }

                        if (! empty($data[$currentField]) && is_array($data[$currentField])) {
                            foreach ($data[$currentField] as $value) {
                                if (empty($value)) {
                                    $fields[] = $currentField;
                                }
                            }
                        } elseif (! $data[$currentField]) {
                            $fields[] = empty($group) ? $currentField : $group . '_' . $currentField;
                        }
                    }
                }
            } else {
                if ($this->getParam($field, 'require')) {
                    if ($field['type'] == 'arr' || $field['type'] == 'hash') {
                        foreach ($data[$nameField] as $item) {
                            if (! $item) {
                                $fields[] = empty($group) ? $nameField : $group . '_' . $nameField;
                            }
                        }
                    }

                    if (! isset($data[$nameField])) {
                        $fields[] = empty($group) ? $nameField : $group . '_' . $nameField;
                    }
                }
            }
        }

        return $fields;
    }

    /**
     * Проверка на уникальность поля.
     *
     * @param $form
     * @param $data
     * @param null $group
     * @return array
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function checkUniqueFields($form, $data, $group = null)
    {
        $fields = [];

        /* Наименование таблицы. */
        $tableName = with($this->model)->getTable();

        foreach ($form as $nameField => $field) {
            if (! empty($field['fields'])) {
                foreach ($field['fields'] as $currentField => $fieldData) {
                    if ($this->getParam($fieldData, 'unique')) {
                        $realNameField = empty($group) ? $currentField : str_replace($group . '_', '', $currentField);

                        $query = $this->model::where($realNameField, $data[$currentField])
                            ->where('id', '!=', $this->modelId);

                        if ($this->model->languagable && Schema::hasColumn($tableName, $this->model->languageField)) {
                            $query = $query->where($this->model->languageField, Localization::getAdminLocale());
                        }

                        if ($query->count() > 0) {
                            $fields[] = (! empty($group) ? $group . '_' : null) . $currentField;
                        }
                    }
                }
            } else {
                if ($this->getParam($field, 'unique')) {
                    $realNameField = empty($group) ? $nameField : str_replace($group . '_', '', $nameField);

                    $query = $this->model::where($realNameField, $data[$nameField])
                        ->where('id', '!=', $this->modelId);

                    if ($this->model->languagable && Schema::hasColumn($tableName, $this->model->languageField)) {
                        $query = $query->where($this->model->languageField, Localization::getAdminLocale());
                    }

                    if ($query->count() > 0) {
                        $fields[] = (! empty($group) ? $group . '_' : null) . $nameField;
                    }
                }
            }
        }

        return $fields;
    }

    /**
     * Валидация полей.
     *
     * @param $form
     * @param $data
     * @param $group
     * @return array
     */
    public function validateFields($form, $data, $group = null)
    {
        $fields = [];
        $types = ['email', 'number', 'url'];

        foreach ($form as $nameField => $field) {
            if ($field['type'] != 'divider' || $field['type'] != 'html') {
                $realNameField = empty($group) ? $nameField : str_replace($group . '_', '', $nameField);

                if (! empty($field['params'])) {
                    foreach ($field['params'] as $type => $rule) {
                        if ($rule != 'ignore') {
                            $type = in_array($field['type'], $types) ? $field['type'] : $type;
                            $type = is_numeric($type) ? $field['params'][$type] : $type;

                            if (is_array($data[$nameField])) {
                                foreach ($data[$nameField] as $dataValue) {
                                    $result = $this->listValidateType($type, $dataValue, $rule);

                                    if ($result) {
                                        $fields[] = $realNameField;
                                    }
                                }
                            } else {
                                $result = $this->listValidateType($type, $data[$nameField], $rule);

                                if ($result) {
                                    $fields[] = $realNameField;
                                }
                            }
                        }
                    }
                } else {
                    if (in_array($field['type'], $types)) {
                        $result = $this->listValidateType($field['type'], $data[$nameField]);

                        if ($result) {
                            $fields[] = $nameField;
                        }
                    };
                }
            }
        }

        return $fields;
    }

    /**
     * Валидация полей.
     *
     * @param $type
     * @param $val
     * @param $rule
     * @return int|null
     */
    public function listValidateType($type, $val, $rule = null)
    {
        $error = null;

        switch ($type) {
            case 'email':
                if (! filter_var($val, FILTER_VALIDATE_EMAIL) && !empty($val)) {
                    $error++;
                }

                break;
            case 'url':
                if (! filter_var($val, FILTER_VALIDATE_URL) && !empty($val)) {
                    $error++;
                }

                break;
            case 'number':
                $val = str_replace(',', '.', $val);

                if (! is_numeric($val) && !empty($val)) {
                    $error++;
                }

                break;
            case 'regexp':
                if (! preg_match($rule, $val) && !empty($val)) {
                    $error++;
                }

                break;
        }

        return $error;
    }

    /**
     * Проверка на то, возможно ли удаление записи с заданными условиями.
     *
     * @param $model
     * @param $id
     * @return int
     */
    public function destroyExists($model, $id)
    {
        $destroy = 0;
        $destroyIfItem = $model->where('id', $id);

        /* Дополнительные условия для удаления данных. */
        if (! empty($model->destroyIf)) {
            foreach ($model->destroyIf as $selection) {
                if (! empty($selection[1])) {
                    if (($selection[1] == '!=' || $selection[1] == '<>') && empty($selection[2])) {
                        $destroyIfItem = $destroyIfItem->whereNotNull($selection[0]);
                    } elseif ($selection[1] == '=' && empty($selection[2])) {
                        $destroyIfItem = $destroyIfItem->whereNull($selection[0]);
                    } else {
                        $destroyIfItem = $destroyIfItem->where($selection[0], $selection[1], $selection[2]);
                    }
                }
            }

            $destroy = $destroyIfItem->exists() ? 0 : 1;
        }

        return $destroy;
    }

    /**
     * Получение ID записи.
     *
     * @return int
     */
    public function getId()
    {
        if (! empty(request()->route('id'))) {
            $id = request()->route('id');
        } else {
            $autoIncrement = DB::select("show table status like '" . $this->model->getTable() . "'");
            $id = $autoIncrement[0]->Auto_increment;
        }

        return $id;
    }

    /**
     * Путь и наименование модели.
     *
     * @return string
     * @throws \ReflectionException
     */
    protected function getModelName()
    {
        $class = new \ReflectionClass($this->modelPath);

        $modelName = str_replace(config('ariol.models-path') . '\\', '', $class->getName());
        $modelName = str_replace('ariol\models\\', '', $modelName);

        return str_replace('\\', '/', strtolower($modelName));
    }

    /**
     * Автокомплит адреса от яндекса.
     *
     * @param $form
     * @param $field
     * @param $value
     * @param $group
     * @return string
     */
    public function address($form, $field, $value, $group = null)
    {
        return $value;
    }

    /**
     * Массив.
     *
     * @param $form
     * @param $field
     * @param $value
     * @param $group
     * @return string
     */
    public function arr($form, $field, $value, $group = null)
    {
        $values = [];

        if (isset($value[0]) && is_array($value[0])) {
            foreach ($value as $key => $val) {
                $values[$key] = json_encode($val);
            }
        } else {
            $values = json_encode($value);
        }

        return $values;
    }

    /**
     * Поле с автодополнением вводимого значения.
     *
     * @param $form
     * @param $field
     * @param $value
     * @param $group
     * @return string
     */
    public function autocomplete($form, $field, $value, $group = null)
    {
        $completeModel = $form[$field]['orm']['model'];
        $completeField = $form[$field]['orm']['field'];

        $completeData = [];
        $completeValues = explode(',', $value);

        foreach ($completeValues as $completeValue) {
            $getItem = $completeModel::where($completeField, trim($completeValue))->first();

            if (isset($getItem)) {
                $completeData[] = $getItem->id;
            }
        }

        return json_encode($completeData);
    }

    /**
     * Checkbox.
     *
     * @param $form
     * @param $field
     * @param $value
     * @param $group
     * @return int
     */
    public function boolean($form, $field, $value, $group = null)
    {
        return !empty($value) ? $value : 0;
    }

    /**
     * Конструктор полей.
     *
     * @param $model
     * @param $form
     * @param $builderFields
     * @param $relationship
     * @param $mainModelId
     * @param $group
     */
    public function builder($model, $form, $builderFields, $relationship, $mainModelId, $group)
    {
        /* Значения полей, которые задействованы в конструкторе. */
        $allBuilderFieldsValues = [];

        /* Формируем массив: название поля => [значения полей]. */
        foreach ($builderFields as $fieldName => $builderFieldValues) {
            foreach ($builderFieldValues as $builderFieldValue) {
                if (isset($model->getAttributes()[$builderFieldValue])) {
                    $modelValue = $model->getAttributes()[$builderFieldValue];

                    if (is_array($modelValue)) {
                        foreach ($modelValue as $key => $modelField) {
                            if (isset($model->getCasts()[$builderFieldValue])
                                && $model->getCasts()[$builderFieldValue] == 'array') {
                                if (! empty($model->getAttributes()['id'][$key])) {
                                    $modelField = is_array($modelField) ? json_encode($modelField) : $modelField;
                                }
                            } else {
                                $modelField = is_array($modelField) ? json_encode($modelField) : $modelField;
                            }

                            $allBuilderFieldsValues[$fieldName][$builderFieldValue][] = $modelField;
                        }
                    } else {
                        $allBuilderFieldsValues[$fieldName][$builderFieldValue][] = $modelValue;
                    }
                } else {
                    $allBuilderFieldsValues[$fieldName][$builderFieldValue][] = null;
                }

                unset($model->{$builderFieldValue});
            }
        }

        /* ID всех текущих записей. */
        $builderId = [];

        /* Массив для сбора данных конструктора. */
        $builderRequest = [];

        if (! empty($form[key($allBuilderFieldsValues)]['model'])) {
            $firstKey = key($allBuilderFieldsValues);
            $secondKey = key($allBuilderFieldsValues[$firstKey]);

            /* Кол-во полей, задействованных в конструкторе. */
            $countBuilderFields = count($allBuilderFieldsValues[$firstKey]);

            /* Кол-во значений, заданных в полях конструктора. */
            $countBuilderValues = count($allBuilderFieldsValues[$firstKey][$secondKey]);
        } else {
            /* Кол-во полей, задействованных в конструкторе. */
            $countBuilderFields = count($allBuilderFieldsValues);

            /* Кол-во значений, заданных в полях конструктора. */
            $countBuilderValues = count($allBuilderFieldsValues[key($allBuilderFieldsValues)]);
        }

        /* Сколько значений, столько и записей будет создано в соответствующей таблице. */
        for ($i = 0; $i < $countBuilderValues; $i++) {
            /* Формируем массив на основе значений каждого поля. */
            for ($k = 0; $k < $countBuilderFields; $k++) {
                /* Определение поля конструктора. */
                $builderField = array_slice($allBuilderFieldsValues[key($allBuilderFieldsValues)], $k, ($k + 1));

                /* Получение значения поля. */
                $builderValue = isset($builderField[key($builderField)][$i])
                    ? $builderField[key($builderField)][$i]
                    : null;

                /* Формирование массива: название поля => значение. */
                $builderRequest[key($builderField)] = $builderValue;

                /* К сформированному массиву данных добавляем связующее поле и значение. */
                $builderRequest[$relationship] = $mainModelId;
            }

            if (! empty($form[key($allBuilderFieldsValues)]['model'])) {
                if (isset($builderRequest['id']) && $builderRequest['id'] != 0) {
                    $builderId[] = $builderRequest['id'];
                }

                $builderRequest['position'] = $i + 1;

                if (isset($builderRequest['id']) && $model::where('id', $builderRequest['id'])->count() > 0) {
                    $model = $model->where('id', $builderRequest['id'])->first();

                    unset($builderRequest['id']);

                    foreach ($builderRequest as $field => $value) {
                        $model->{$field} = $value;
                    }

                    $model->save();
                } else {
                    $new = $model::create($builderRequest);

                    $builderId[] = $new->id;
                }
            } else {
                foreach ($allBuilderFieldsValues as $fieldName => $builderFieldValues) {
                    foreach ($builderFieldValues as $builderFieldName => $builderFieldValue) {
                        unset($builderRequest['id']);

                        $realField = str_replace($group . '_', '', $fieldName);

                        $model->{$realField} = json_encode($builderFieldValues);
                    }

                    $model->save();
                }
            }
        }

        if (! empty($builderId)) {
            $model::where($relationship, $mainModelId)->whereNotIn('id', $builderId)->delete();
        }
    }

    /**
     * Выбор цвета.
     *
     * @param $form
     * @param $field
     * @param $value
     * @param $group
     * @return string
     */
    public function color($form, $field, $value, $group = null)
    {
        return $value;
    }

    /**
     * Выбор даты.
     *
     * @param $form
     * @param $field
     * @param $value
     * @param $group
     * @return string
     */
    public function date($form, $field, $value, $group = null)
    {
        return $value;
    }

    /**
     * Выбор даты и времени.
     *
     * @param $form
     * @param $field
     * @param $value
     * @param $group
     * @return string
     */
    public function datetime($form, $field, $value, $group = null)
    {
        return $value;
    }

    /**
     * Разделитель.
     *
     * @param $form
     * @param $field
     * @param $value
     * @param $group
     * @return bool
     */
    public function divider($form, $field, $value, $group = null)
    {
        return false;
    }

    /**
     * Текстовый редактор.
     *
     * @param $form
     * @param $field
     * @param $value
     * @param $group
     * @return string
     */
    public function editor($form, $field, $value, $group = null)
    {
        return $value;
    }

    /**
     * Электронная почта.
     *
     * @param $form
     * @param $field
     * @param $value
     * @param $group
     * @return string
     */
    public function email($form, $field, $value, $group = null)
    {
        return $value;
    }

    /**
     * Загрузка одного файла.
     *
     * @param $form
     * @param $field
     * @param $value
     * @param null $group
     * @return array|mixed|string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function file($form, $field, $value, $group = null)
    {
        if (! file_exists(public_path() . $this->filesPath)) {
            mkdir(public_path() . $this->filesPath, 0777, true);
        }

        if (! $value) {
            if (! empty($this->model->{$field}) && file_exists(public_path() . $this->model->{$field})) {
                unlink(public_path() . $this->model->{$field});
            }

            return '';
        }

        if (is_array($value)) {
            $files = [];

            foreach ($value as $val) {
                $result = $this->validateFile($field, $val);

                $files[] = $result;
            }

            return $files;
        } else {
            return $this->validateFile($field, $value);
        }
    }

    /**
     * Обработка загруженного файла.
     *
     * @param $field
     * @param $value
     * @return mixed
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function validateFile($field, $value)
    {
        if (preg_match('%\/temp\/%', $value)) {
            $fileParts = explode('/', urldecode($value));

            $prefix = !empty($this->modelId) ? $this->modelId . '-' : null;

            if (config('ariol.multi-languages')) {
                $prefix .= Localization::getAdminLocale() . '-';
            }

            $checkFile = public_path() . $this->filesPath . Helper::getWritableFileName(end($fileParts));
            if (file_exists($checkFile)) {
                $prefix .= time() . '-';
            }

            $filename = $prefix . Helper::getWritableFileName(end($fileParts));

            if ($this->model->{$field} && $this->filesPath . $filename != $this->model->{$field}) {
                if (! empty($this->model->{$field}) && file_exists(public_path() . $this->model->{$field})) {
                    unlink(public_path() . $this->model->{$field});
                }
            }

            if (file_exists(public_path() . urldecode($value))) {
                rename(public_path() . urldecode($value), public_path() . $this->filesPath . $filename);
            }

            $thumbnail = str_replace('/temp/', '/temp/thumbnail/', urldecode($value));
            if (! empty($thumbnail) && file_exists(public_path() . $thumbnail)) {
                unlink(public_path() . $thumbnail);
            }

            return str_replace('//', '/', $this->filesPath . $filename);
        } else {
            return $value;
        }
    }

    /**
     * Загрузка нескольких файлов.
     *
     * @param $form
     * @param $field
     * @param $value
     * @param null $group
     * @return array|string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function files($form, $field, $value, $group = null)
    {
        if (! file_exists(public_path() . $this->filesPath)) {
            mkdir(public_path() . $this->filesPath, 0777, true);
        }

        if (is_array($value)) {
            $allFiles = [];

            foreach ($value as $key => $val) {
                $result = $this->validateFiles($field, $val, $key);
                if ($result) {
                    $allFiles[] = $result;
                }
            }

            return $allFiles;
        } else {
            return $this->validateFiles($field, $value);
        }
    }

    /**
     * Обработка загруженных файлов.
     *
     * @param $field
     * @param $value
     * @param null $key
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function validateFiles($field, $value, $key = null)
    {
        $files = explode(',', $value);

        $empty = 0;
        foreach ($files as $file) {
            if (! empty($file)) {
                $empty++;
            }
        }

        if ($empty == 0) {
            if (! empty($this->model->{$field}) && file_exists(public_path() . $this->model->{$field})) {
                unlink(public_path() . $this->model->{$field});
            }

            return '';
        }

        $rowValues = [];
        $itemFiles = json_decode($this->model->{$field}, true);

        if (! empty($key)) {
            $itemFiles = json_decode($this->model->getAttributes()[$field][$key], true);
        }

        if (is_array($itemFiles)) {
            foreach ($itemFiles as $item) {
                if (! in_array($item, $files)) {
                    if (file_exists(public_path() . $item)) {
                        unlink(public_path() . $item);
                    }
                }
            }
        }

        foreach ($files as $file) {
            if (! file_exists(public_path() . urldecode($file)) || is_dir(public_path() . urldecode($file))) {
                continue;
            }

            $nextId = $this->modelId;

            if (! empty($key)) {
                if (isset($this->model->getAttributes()['id'][$key])
                    && $this->model->getAttributes()['id'][$key] != 0) {
                    $nextId = $this->model->getAttributes()['id'][$key];
                } else {
                    $autoIncrement = DB::select("show table status like '" . $this->model->getTable() . "'");
                    $nextId = $autoIncrement[0]->Auto_increment;
                }
            }

            if (preg_match('%\/temp\/%', $file)) {
                $fileParts = explode('/', urldecode($file));

                $prefix = !empty($nextId) ? $nextId . '-' : null;

                if (config('ariol.multi-languages')) {
                    $prefix .= Localization::getAdminLocale() . '-';
                }

                $checkFile = public_path() . $this->filesPath . Helper::getWritableFileName(end($fileParts));
                if (file_exists($checkFile)) {
                    $prefix .= time() . '-';
                }

                $filename = $prefix . Helper::getWritableFileName(end($fileParts));

                if (file_exists(public_path() . urldecode($file))) {
                    rename(
                        public_path() . urldecode($file),
                        public_path() . str_replace('//', '/', $this->filesPath . $filename)
                    );
                }

                $rowValues[] = $this->filesPath . $filename;

                $thumbnail = str_replace('/temp/', '/temp/thumbnail/', urldecode($file));
                if (! empty($thumbnail) && file_exists(public_path() . $thumbnail)) {
                    unlink(public_path() . $thumbnail);
                }
            } elseif ($file) {
                $rowValues[] = $file;
            }
        }

        if (! empty($rowValues)) {
            return json_encode($rowValues);
        } else {
            return $value;
        }
    }

    /**
     * 2 поля для ввода ключа и значения.
     *
     * @param $form
     * @param $field
     * @param $value
     * @param $group
     * @return string
     */
    public function hash($form, $field, $value, $group = null)
    {
        $key = 0;
        $data = [];

        foreach ($value as $i => $val) {
            $data[$key][] = $val;

            if ($i % 2 != 0) {
                $key++;
            }
        }

        return json_encode($data);
    }

    /**
     * Скрытое поле.
     *
     * @param $form
     * @param $field
     * @param $value
     * @param $group
     * @return array|string
     */
    public function hidden($form, $field, $value, $group = null)
    {
        return $this->slugField($form, $field, $value, $group);
    }

    /**
     * Html-вывод информации.
     *
     * @param $form
     * @param $field
     * @param $value
     * @param $group
     * @return bool
     */
    public function html($form, $field, $value, $group = null)
    {
        return false;
    }

    /**
     * Выбор локации или ввод вручную.
     *
     * @param $form
     * @param $field
     * @param $value
     * @param $group
     * @return string
     */
    public function locale($form, $field, $value, $group = null)
    {
        return json_encode($value);
    }

    /**
     * Выбор нескольких значений.
     *
     * @param $form
     * @param $field
     * @param $value
     * @param $group
     * @return array|string}bool
     */
    public function multiselect($form, $field, $value, $group = null)
    {
        return $this->jsonSelectField($form, $field, $value, $group);
    }

    /**
     * Выбор нескольких значений, отсортированных по группам.
     *
     * @param $form
     * @param $field
     * @param $value
     * @param $group
     * @return array|string}bool
     */
    public function multiselect_autocomplete($form, $field, $value, $group = null)
    {
        return $this->jsonSelectField($form, $field, $value, $group);
    }

    /**
     * Выбор нескольких значений, отсортированных по группам.
     *
     * @param $form
     * @param $field
     * @param $value
     * @param $group
     * @return array|string}bool
     */
    public function multiselect_group($form, $field, $value, $group = null)
    {
        return $this->jsonSelectField($form, $field, $value, $group);
    }

    /**
     * Ввод числового значения.
     *
     * @param $form
     * @param $field
     * @param $value
     * @param $group
     * @return string
     */
    public function number($form, $field, $value, $group = null)
    {
        $nullable = !empty($form[$field]['nullable']) ? true : false;

        if ($nullable) {
            $value = isset($value) ? $value : null;
        } else {
            $value = !empty($value) ? $value : 0;
        }

        return str_replace(',', '.', $value);
    }

    /**
     * Пароль и его подтверждение.
     *
     * @param $form
     * @param $field
     * @param $value
     * @param $group
     * @return array|bool|string
     */
    public function password($form, $field, $value, $group = null)
    {
        if (isset($value[0])) {
            $fields[] = $field;

            if (strlen((string) $value[0]) < 6) {
                return [
                    'checkFields' => $fields,
                    'message' => translate('system.form.packageItems.length-password')
                ];
            }

            if (isset($value[1]) && $value[0] != $value[1]) {
                return [
                    'checkFields' => $fields,
                    'message' => translate('system.form.packageItems.no-confirmed-passwords')
                ];
            } else {
                return bcrypt($value[0]);
            }
        }

        return false;
    }

    /**
     * Ввод номера телефона, используя маску.
     *
     * @param $form
     * @param $field
     * @param $value
     * @param $group
     * @return string
     */
    public function phone($form, $field, $value, $group = null)
    {
        return $value;
    }

    /**
     * Выбор одного значения из списка.
     *
     * @param $form
     * @param $field
     * @param $value
     * @param $group
     * @return array|string}bool
     */
    public function select($form, $field, $value, $group = null)
    {
        return $this->jsonSelectField($form, $field, $value, $group);
    }

    /**
     * Выбор одного значения из списка данных, взятых из другой таблицы, с автодополнение.
     *
     * @param $form
     * @param $field
     * @param $value
     * @param $group
     * @return array|string}bool
     */
    public function select_autocomplete($form, $field, $value, $group = null)
    {
        return $this->jsonSelectField($form, $field, $value, $group);
    }

    /**
     * Выбор одного значения из списка, который отсортирован по группам.
     *
     * @param $form
     * @param $field
     * @param $value
     * @param $group
     * @return array|string}bool
     */
    public function select_group($form, $field, $value, $group = null)
    {
        return $this->jsonSelectField($form, $field, $value, $group);
    }

    /**
     * Строковое поле.
     *
     * @param $form
     * @param $field
     * @param $value
     * @param $group
     * @return array|string
     */
    public function string($form, $field, $value, $group = null)
    {
        return $this->slugField($form, $field, $value, $group);
    }

    /**
     * Текстовое поле.
     *
     * @param $form
     * @param $field
     * @param $value
     * @param $group
     * @return array|string
     */
    public function text($form, $field, $value, $group = null)
    {
        return $this->slugField($form, $field, $value, $group);
    }

    /**
     * Выбор времени.
     *
     * @param $form
     * @param $field
     * @param $value
     * @param $group
     * @return string
     */
    public function time($form, $field, $value, $group = null)
    {
        return $value;
    }

    /**
     * Поле для ввода ссылок.
     *
     * @param $form
     * @param $field
     * @param $value
     * @param $group
     * @return array|string
     */
    public function url($form, $field, $value, $group = null)
    {
        return $this->slugField($form, $field, $value, $group);
    }

    /**
     * Все типы с использованием select для занесения данных в разные таблицы.
     *
     * @param $form
     * @param $field
     * @param $value
     * @param $group
     * @return bool
     */
    public function jsonSelectField($form, $field, $value, $group)
    {
        $otherModel = '';
        $otherModelFieldName = '';
        $otherModelFieldValue = [];
        $otherModelRelationshipField = '';

        $otherRelationship = '';
        $otherRelationshipFieldName = '';
        $otherRelationshipFieldValue = '';
        $otherRelationshipFieldModel = '';

        /* Использование поля в форме из другой таблицы. */
        if (! empty($form[$field]['model']) && is_array($form[$field]['model'])) {
            if (! empty($form[$field]['model']['name']) && !empty($form[$field]['model']['relationship'])) {
                $otherModel = new $form[$field]['model']['name'];

                /* Название поля в другой таблице. */
                $otherModelFieldName = str_replace($group . '_', '', $field);

                /* Значение поля в другой таблице. */
                $otherModelFieldValue = $value;

                /* Связующее поле. */
                $otherModelRelationshipField = $form[$field]['model']['relationship'];
            }
        } elseif (! empty($form[$field]['relationship']) && is_array($form[$field]['relationship'])) {
            /* Использование полей из другой таблицы через связующую таблицу. */
            if (! empty($form[$field]['relationship']['model']) && !empty($form[$field]['relationship']['field'])) {
                $otherRelationship = new $form[$field]['relationship']['model'];

                /* Название поля в другой таблице. */
                $otherRelationshipFieldName = str_replace($group . '_', '', $field);

                /* Значение поля в другой таблице. */
                $otherRelationshipFieldValue = $value;

                /* Связующее поле. */
                $otherRelationshipFieldModel = $form[$field]['relationship']['field'];
            }
        } else {
            if (in_array($form[$field]['type'], [
                'multiselect', 'multiselect_group', 'multiselect_autocomplete'
            ])) {
                $values = [];

                if (isset($value[0]) && is_array($value[0])) {
                    foreach ($value as $key => $val) {
                        $values[$key] = json_encode($val);
                    }
                } else {
                    $values = json_encode($value);
                }

                return $values;
            } else {
                return $value;
            }
        }

        if (! empty($otherModel)) {
            /* Удаляем все записи, связанные с основной моделью через ID. */
            $otherModel::where($otherModelRelationshipField, $this->modelId)->delete();

            if (! empty($otherModelFieldValue)) {
                foreach ($otherModelFieldValue as $val) {
                    if (! empty($form)) {
                        foreach ($form as $field => $type) {
                            if ($field == $otherModelRelationshipField && isset($type['model'])) {
                                $val = null;

                                unset($form[$field]);
                            }
                        }
                    }

                    /* Создаём записи на основе выбранных данных и связующего поля. */
                    $otherModel::create([
                        $otherModelRelationshipField => $this->modelId,
                        $otherModelFieldName => $val
                    ]);
                }
            }
        }

        if (! empty($otherRelationship)) {
            if (! empty($form)) {
                foreach ($form as $field => $type) {
                    if ($field == $otherRelationshipFieldName && isset($type['relationship'])) {
                        $otherRelationshipFieldValue = null;

                        unset($form[$field]);
                    }
                }
            }

            $otherRelationship::where('id', $this->model->{$otherRelationshipFieldModel})->update([
                $otherRelationshipFieldName => $otherRelationshipFieldValue
            ]);
        }

        return false;
    }

    /**
     * Генерация алиаса выбранного поля для занесения значения в текущее.
     *
     * @param $form
     * @param $field
     * @param $value
     * @param $group
     * @return array|string
     */
    public function slugField($form, $field, $value, $group = null)
    {
        $realField = !empty($group) ? str_replace($group . '_', '', $field) : $field;

        if (! empty($form[$field]['params']['slug']) && empty($value)) {
            $slugValue = $this->model->{$form[$field]['params']['slug']};
            $value = str_slug($slugValue);

            if ($this->getParam($form[$field], 'unique')) {
                if ($this->model::where($realField, $value)->where('id', '!=', $this->modelId)->count() > 0) {
                    return [
                        'checkFields' => [$field],
                        'message' => translate('system.form.packageItems.isset-value')
                    ];
                }
            }
        }

        $this->model->{$realField} = $value;

        return $value;
    }

    /**
     * Проверка на наличие параметра.
     *
     * @param $field
     * @param $type
     * @return bool
     */
    public function getParam($field, $type)
    {
        if (! empty($field['params']) && in_array($type, $field['params'])) {
            return true;
        }

        return false;
    }
}
