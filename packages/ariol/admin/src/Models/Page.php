<?php

namespace Ariol\Models;

/**
 * Модель страниц в админке.
 *
 * @package Ariol\Models
 */
class Page extends Model
{
    /**
     * Используемая таблица.
     *
     * @var string
     */
    protected $table = 'pages';

    /**
     * Разрешаем все поля для автозаполнения.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * В таблице не используются timestamp'ы.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Используется ли мультиязычность.
     *
     * @var bool
     */
    public $languagable = true;

    /**
     * Данные полей, выводимых на странице.
     *
     * @return array
     */
    public function columns()
    {
        return [
            'title' => [
                'type' => 'editable'
            ],
            'url' => [
                'type' => 'editable'
            ]
        ];
    }

    /**
     * Наименование полей.
     *
     * @return array
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function labels()
    {
        return [
            'title' => translate('admin.modules.packageItems.users.packageItems.name'),
            'url' => translate('admin.common.packageItems.url'),
            'content' => translate('admin.common.packageItems.content'),
            'seo' => translate('admin.common.packageItems.seo'),
            'seo_title' => translate('admin.common.packageItems.title'),
            'seo_description' => translate('admin.common.packageItems.description'),
            'seo_keywords' => translate('admin.common.packageItems.keywords')
        ];
    }

    /**
     * Описание типов полей.
     *
     * @return array
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function form()
    {
        return [
            'title' => [
                'column' => 6,
                'type' => 'string',
                'params' => ['require']
            ],
            'url' => [
                'column' => 6,
                'type' => 'string',
                'params' => ['slug' => 'title', 'unique'],
                'placeholder' => translate('admin.common.packageItems.alias-description')
            ],
            'content' => [
                'type' => 'editor',
                'params' => ['require']
            ],
            'seo' => [
                'type' => 'divider'
            ],
            'seo_title' => [
                'column' => 4,
                'type' => 'text'
            ],
            'seo_description' => [
                'column' => 4,
                'type' => 'text'
            ],
            'seo_keywords' => [
                'column' => 4,
                'type' => 'text'
            ]
        ];
    }

    /**
     * Поля, по которым можно сортировать данные в таблице.
     *
     * @return array
     */
    public function sortableFields()
    {
        return ['title'];
    }

    /**
     * Поля, по которым будет осуществляться поиск в таблице.
     *
     * @return array
     */
    public function searchableFields()
    {
        return ['title', 'url'];
    }
}
