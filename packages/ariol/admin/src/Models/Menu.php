<?php

namespace Ariol\Models;

/**
 * Модель пунктов меню.
 *
 * @package Ariol\Models
 */
class Menu extends Model
{
    /**
     * Используемая таблица.
     *
     * @var string
     */
    protected $table = 'menu';

    /**
     * Разрешаем все поля для автозаполнения.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Связующее поле, по которому будет происходить рекурсивное удаление.
     *
     * @var string
     */
    public $recursiveDestroy = 'parent';

    /**
     * Используется ли мультиязычность.
     *
     * @var bool
     */
    public $languagable = true;

    /**
     * Дополнительные фильтры.
     *
     * @return array
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function filters()
    {
        return [
            'active' => [
                'column' => 2,
                'type' => 'checkbox',
                'title' => translate('admin.modules.packageItems.menu.packageItems.filter')
            ]
        ];
    }

    /**
     * Данные полей, выводимых на странице.
     *
     * @return array
     */
    public function columns()
    {
        return [
            'position' => [
                'width' => 100,
                'type' => 'editable',
                'align' => 'center'
            ],
            'name' => [
                'type' => 'editable'
            ],
            'active' => [
                'width' => 150,
                'type' => 'activity',
                'align' => 'center'
            ]
        ];
    }

    /**
     * Наименование полей.
     *
     * @return array
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function labels()
    {
        return [
            'url' => translate('admin.common.packageItems.url'),
            'name' => translate('admin.common.packageItems.name'),
            'position' => translate('admin.common.packageItems.position'),
            'parent' => translate('admin.modules.packageItems.menu.packageItems.parent'),
            'active' => translate('admin.modules.packageItems.menu.packageItems.active')
        ];
    }

    /**
     * Описание типов полей.
     *
     * @return array
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function form()
    {
        return [
            'name' => [
                'column' => 6,
                'type' => 'string',
                'params' => ['require']
            ],
            'url' => [
                'column' => 6,
                'type' => 'string',
                'params' => ['slug' => 'name'],
                'placeholder' => translate('admin.common.packageItems.alias-description')
            ],
            'parent' => [
                'column' => 6,
                'type' => 'select',
                'values' => $this->outputCategories()
            ],
            'position' => [
                'column' => 6,
                'type' => 'number'
            ]
        ];
    }

    /**
     * Поля, по которым можно сортировать данные в таблице.
     *
     * @return array
     */
    public function sortableFields()
    {
        return ['position', 'name'];
    }

    /**
     * Поля, по которым будет осуществляться поиск в таблице.
     *
     * @return array
     */
    public function searchableFields()
    {
        return ['name', 'url'];
    }

    /**
     * Родительская категория.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function category()
    {
        return $this->hasOne('Ariol\Models\Menu', 'id', 'parent');
    }

    /**
     * Получение данных активной категории.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function cat()
    {
        return $this->hasOne('Ariol\Models\Menu', 'id', 'parent')->where('active', 1);
    }

    /**
     * Вывод категорий.
     *
     * @return array
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function outputCategories()
    {
        $data = $this::with('cat')->getWithLang();

        $items = [];
        $items[0] = translate('admin.modules.packageItems.menu.packageItems.main_category');

        foreach ($data as $item) {
            if ((isset($this->id) && $this->id != $item->id && $this->id != $item->parent) || !isset($this->id)) {
                $cat = $item->cat ? $item->cat->name. ' / ' : null;
                $cats = $cat ? ($item->cat->cat ? $item->cat->cat->name . ' / ' : '') : null;

                $items[$item->id] = $cats.$cat.$item->name;
            }
        }

        return $items;
    }

    /**
     * Наименование категории.
     *
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function catName()
    {
        return $this->cat ? $this->cat->name : translate('admin.modules.packageItems.menu.packageItems.main_category');
    }
}
