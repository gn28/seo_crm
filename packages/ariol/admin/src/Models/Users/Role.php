<?php

namespace Ariol\Models\Users;

use Ariol\Models\Model;

/**
 * Модель ролей пользователей.
 *
 * @package Ariol\Models\Users
 */
class Role extends Model
{
    /**
     * Используемая таблица.
     *
     * @var string
     */
    protected $table = 'roles';

    /**
     * Разрешаем все поля для автозаполнения.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Удалять только те записи, которые попадают под заданные условия.
     *
     * @var array
     */
    public $destroyIf = [
        ['id', '!=', '1']
    ];

    /**
     * Данные полей, выводимых на странице.
     *
     * @return array
     */
    public function columns()
    {
        return ['alias', 'name'];
    }

    /**
     * Наименование полей.
     *
     * @return array
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function labels()
    {
        return [
            'role' => [
                'title' => translate('admin.modules.packageItems.users.packageItems.role_id'),
                'content' => [
                    'name' => translate('admin.common.packageItems.name'),
                    'alias' => translate('admin.common.packageItems.alias')
                ]
            ],
            'permissions' => [
                'title' => translate('admin.modules.packageItems.users.packageItems.permissions'),
                'model' => [
                    'name' => 'Ariol\Models\Users\Module',
                    'relationship' => 'role_id'
                ],
                'content' => [
                    'table' => translate('admin.modules.packageItems.users.packageItems.module'),
                    'entries' => translate('admin.modules.packageItems.users.packageItems.entries'),
                    'actions' => translate('admin.modules.packageItems.users.packageItems.actions')
                ]
            ]
        ];
    }

    /**
     * Описание типов полей.
     *
     * @return array
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function form()
    {
        return [
            'role' => [
                'alias' => [
                    'column' => 6,
                    'type' => 'string',
                    'params' => ['unique', 'require']
                ],
                'name' => [
                    'column' => 6,
                    'type' => 'string',
                    'params' => ['require']
                ]
            ],
            'permissions' => [
                'modules' => [
                    'type' => 'builder',
                    'model' => [
                        'name' => 'Ariol\Models\Users\Module',
                        'relationship' => 'role_id'
                    ],
                    'fields' => [
                        'id' => [
                            'type' => 'hidden'
                        ],
                        'table' => [
                            'column' => 4,
                            'type' => 'select',
                            'values' => $this->getModules(),
                            'ajax' => [
                                'relation' => 'entries',
                                'model' => 'App\Models\Catalog\Direction',
                                'fields' => [
                                    'key' => 'id',
                                    'value' => 'name|title',
                                    'language' => $this->languageField
                                ]
                            ]
                        ],
                        'entries' => [
                            'column' => 4,
                            'type' => 'multiselect'
                        ],
                        'actions' => [
                            'column' => 4,
                            'type' => 'multiselect',
                            'values' => $this->getActions()
                        ]
                    ]
                ]
            ]
        ];
    }

    /**
     * Поля, по которым можно сортировать данные в таблице.
     *
     * @return array
     */
    public function sortableFields()
    {
        return ['alias', 'name'];
    }

    /**
     * Поля, по которым будет осуществляться поиск в таблице.
     *
     * @return array
     */
    public function searchableFields()
    {
        return ['alias', 'name'];
    }

    /**
     * Список модулей.
     *
     * @return array
     */
    public function getModules()
    {
        return $this->parseMenu(\config\menu::getMenu(), []);
    }

    /**
     * Получает указанную таблицу каждого пункта меню.
     *
     * @param $items
     * @param $modules
     * @return array
     */
    public function parseMenu($items, $modules)
    {
        if (! empty($items)) {
            foreach ($items as $item) {
                if (! empty($item['table'])) {
                    $modules[$item['table']] = $item['name'];
                }

                if (! empty($item) && !empty($item['sub'])) {
                    $modules = $this->parseMenu($item['sub'], $modules);
                }
            }
        }

        return $modules;
    }

    /**
     * Список действий.
     *
     * @return array
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function getActions()
    {
        return [
            'readonly' => translate('admin.modules.packageItems.users.packageItems.readonly'),
            'creatable' => translate('admin.modules.packageItems.users.packageItems.creatable'),
            'editable' => translate('admin.modules.packageItems.users.packageItems.editable'),
            'destroyable' => translate('admin.modules.packageItems.users.packageItems.destroyable')
        ];
    }

    /**
     * Запрос на получение списка модулей.
     *
     * @return object
     */
    public function modules()
    {
        return Module::where('role_id', $this->id);
    }

    /**
     * Получение данных роли по таблице модуля.
     *
     * @param $table
     * @return object
     */
    public function getModuleByTable($table)
    {
        return $this->modules()->where('table', $table)->first();
    }

    /**
     * Проверка на допуск по таблице модуля.
     *
     * @param $table
     * @return bool
     */
    public function hasModule($table)
    {
        return in_array($table, $this->modules()->get()->pluck('table')->toArray());
    }

    /**
     * Проверка на какие-либо действия в модуле.
     *
     * @param $table
     * @param $action
     * @param $default
     * @return bool
     */
    public function haveAction($table, $action, $default)
    {
        if ($this->hasModule($table)) {
            $module = $this->getModuleByTable($table);

            if (! empty($module)) {
                $actions = !empty($module->actions) ? json_decode($module->actions, true) : [];

                return in_array($action, $actions);
            }
        }

        return $default;
    }

    /**
     * Получение записей, к которым разрешён доступ.
     *
     * @param $table
     * @return array|mixed
     */
    public function getModuleIdsAccess($table)
    {
        if ($this->hasModule($table)) {
            $module = $this->getModuleByTable($table);

            if (! empty($module)) {
                $ids = !empty($module->entries) ? json_decode($module->entries, true) : [];

                return $ids;
            }
        }

        return [];
    }
}
