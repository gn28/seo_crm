<?php

namespace Ariol\Models\Users;

use Ariol\Models\Model;

/**
 * Модель, которая связывает модули и действия в них с ролями.
 *
 * @package Ariol\Models\Users
 */
class Module extends Model
{
    /**
     * Используемая таблица.
     *
     * @var string
     */
    protected $table = 'user_role_modules';

    /**
     * Разрешаем все поля для автозаполнения.
     *
     * @var array
     */
    protected $guarded = [];
}
