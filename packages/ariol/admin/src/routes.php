<?php

$controllersPath = '\Ariol\Admin\Controllers\\';

Route::group(['middleware' => 'web'], function () use ($controllersPath) {
    Route::group(['prefix' => config('ariol.admin-path')], function () use ($controllersPath) {
        /* Форма авторизации в админке. */
        Route::get('/login', [
            'as' => 'login',
            'uses' => $controllersPath . 'AuthController@getLogin'
        ]);

        /* Авторизация в админке. */
        Route::post('/login', $controllersPath . 'AuthController@postLogin');
    });
});

Route::group(['middleware' => ['web', 'auth']], function () use ($controllersPath) {
    Route::group(['prefix' => config('ariol.admin-path')], function () use ($controllersPath) {
        /* Выход из админки. */
        Route::get('/logout', $controllersPath . 'AuthController@getLogout');

        /* Переадресация в админку, если есть права. */
        Route::get('/', $controllersPath . 'MainController@index');

        $modulesPath = $controllersPath . 'Modules\\';
        $routes = ['Users', 'Pages', 'Menu', 'Roles', 'Permissions'];

        foreach ($routes as $route) {
            Route::group(['prefix' => lcfirst($route)], function () use ($route, $modulesPath) {
                Route::get('/', $modulesPath . $route . 'Controller@index');
                Route::post('/data', $modulesPath . $route . 'Controller@data');
                Route::get('/create', $modulesPath . $route . 'Controller@create');
                Route::post('/create', $modulesPath . $route . 'Controller@update');
                Route::get('/edit/{id}', $modulesPath . $route . 'Controller@edit');
                Route::post('/edit/{id}', $modulesPath . $route . 'Controller@update');
                Route::get('/delete/{id}', $modulesPath . $route . 'Controller@destroy');

                if ($route == 'Roles') {
                    Route::post('/select', $modulesPath . $route . 'Controller@select');
                }
            });
        }

        /* Системные настройки сайта. */
        Route::group(['prefix' => 'system'], function () use ($modulesPath) {
            Route::group(['prefix' => 'cache'], function () use ($modulesPath) {
                Route::get('/', $modulesPath . 'CacheController@index');
                Route::post('/clear', $modulesPath . 'CacheController@clear');
            });

            Route::group(['prefix' => 'localization'], function () use ($modulesPath) {
                Route::get('/', $modulesPath . 'LocalizationController@index');
                Route::post('/load-package', $modulesPath . 'LocalizationController@loadPackage');
                Route::post('/add-language', $modulesPath . 'LocalizationController@addLanguage');
                Route::post('/toggle-active', $modulesPath . 'LocalizationController@toggleActive');
                Route::post('/toggle-default', $modulesPath . 'LocalizationController@toggleDefault');
                Route::post('/save-translate', $modulesPath . 'LocalizationController@saveTranslate');
                Route::post('/remove-language', $modulesPath . 'LocalizationController@removeLanguage');
                Route::post('/change-admin-language', $modulesPath . 'LocalizationController@changeAdminLanguage');
                Route::post('/update-list-languages', $modulesPath . 'LocalizationController@updateListLanguages');
                Route::post('/update-admin-languages', $modulesPath . 'LocalizationController@updateAdminLanguages');
                Route::post('/update-available-languages', $modulesPath . 'LocalizationController@updateAvailableLanguages');
                Route::post('/change-current-admin-language', $modulesPath . 'LocalizationController@changeCurrentAdminLanguage');
                Route::post('/change-language-for-translation', $modulesPath . 'LocalizationController@changeLanguageForTranslation');
            });
        });

        /* Таблица данных. */
        Route::group(['prefix' => 'grid'], function () use ($controllersPath) {
            /* Сохранение данных прямо в таблице. */
            Route::post('/save', $controllersPath . 'GridController@save');

            /* Переключатель в самой таблице (on/off). */
            Route::post('/activity', $controllersPath . 'GridController@activity');

            /* Удаление всех выбранных записей в таблице. */
            Route::post('/delete-items', $controllersPath . 'GridController@deleteItems');

            /* Запоминание текущего номера страницы. */
            Route::post('/remember-page', $controllersPath . 'GridController@rememberPage');

            /* Обновление счётчика в пункте меню. */
            Route::post('/update-count-menu-item', $controllersPath . 'GridController@updateCountMenuItem');
        });

        /* Обработка ajax-полей. */
        Route::group(['prefix' => 'ajax'], function () use ($controllersPath) {
            /* Сохранение данных прямо в таблице. */
            Route::post('/select', $controllersPath . 'Forms\AjaxController@select');
        });

        /* Загрузка файлов. */
        Route::post('/file-uploader', $controllersPath . 'FileController@upload');

        /* Сохранение активной вкладки в сессию. */
        Route::post('/save-tab', $controllersPath . 'Forms\BaseController@saveTab');

        /* Удаление файлов. */
        Route::post('/delete-thumbnail', $controllersPath . 'FileController@delete');

        /* Подгрузка данных для автозаполнения. */
        Route::get('/autoComplete', $controllersPath . 'AutocompleteController@index');
    });
});
