// Chart setup
function progressIcon(element, radius, border, backgroundColor, foregroundColor, end, iconClass) {
    // Basic setup
    // ------------------------------

    // Main variables
    var d3Container = d3.select(element),
        startPercent = 0,
        iconSize = 32,
        endPercent = end,
        twoPi = Math.PI * 2,
        formatPercent = d3.format('.0%'),
        boxSize = radius * 2;

    // Values count
    var count = Math.abs((endPercent - startPercent) / 0.01);

    // Values step
    var step = endPercent < startPercent ? -0.01 : 0.01;

    // Create chart
    // ------------------------------

    // Add SVG element
    var container = d3Container.append('svg');

    // Add SVG group
    var svg = container
        .attr('width', boxSize)
        .attr('height', boxSize)
        .append('g')
        .attr('transform', 'translate(' + (boxSize / 2) + ',' + (boxSize / 2) + ')');


    // Construct chart layout
    // ------------------------------

    // Arc
    var arc = d3.svg.arc()
        .startAngle(0)
        .innerRadius(radius)
        .outerRadius(radius - border)
        .cornerRadius(20);

    //
    // Append chart elements
    //

    // Paths
    // ------------------------------

    // Background path
    svg.append('path')
        .attr('class', 'd3-progress-background')
        .attr('d', arc.endAngle(twoPi))
        .style('fill', backgroundColor);

    // Foreground path
    var foreground = svg.append('path')
        .attr('class', 'd3-progress-foreground')
        .attr('filter', 'url(#blur)')
        .style({
            'fill': foregroundColor,
            'stroke': foregroundColor
        });

    // Front path
    var front = svg.append('path')
        .attr('class', 'd3-progress-front')
        .style({
            'fill': foregroundColor,
            'fill-opacity': 1
        });

    // Text
    // ------------------------------

    // Percentage text value
    var numberText = d3.select('.progress-percentage')
        .attr('class', 'mt-15 mb-5');

    // Icon
    d3.select(element)
        .append("i")
        .attr("class", iconClass + " counter-icon")
        .style({
            'color': foregroundColor,
            'top': ((boxSize - iconSize) / 2) + 'px'
        });

    // Animation
    // ------------------------------

    // Animate path
    function updateProgress(progress) {
        foreground.attr('d', arc.endAngle(twoPi * progress));
        front.attr('d', arc.endAngle(twoPi * progress));
        numberText.text(formatPercent(progress));
    }

    // Animate text
    var progress = startPercent;
    (function loops() {
        updateProgress(progress);
        if (count > 0) {
            count--;
            progress += step;
            setTimeout(loops, 10);
        }
    })();
}